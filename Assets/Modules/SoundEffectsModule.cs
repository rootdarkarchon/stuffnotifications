﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Asyncoroutine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchLib.Api.V5.Models.Subscriptions;

namespace Assets.Modules
{
    [Serializable]
    public class SoundEffectsModule : ChatModule
    {
        [Setting("NAME", "🔊 SoundEffects")]
        public string S_Name;

        public List<SoundEffect> SoundEffects;
        public List<string> SoundEffectsList;
        public override string ModuleName => S_Name;

        [Command("[SpecificSoundEffect]", ActualCommandListName = nameof(SoundEffectsList))]
        public void Command_PlaySoundEffect([C_UserSelf] TwitchDBUser userName, [C_SoundEffect] string soundEffectCommand)
        {
            SoundEffect soundEffect = SoundEffects.Single(s => s.Command.ToLower() == soundEffectCommand.ToLower());

            if (soundEffect.IsOnGlobalCooldown() || soundEffect.IsOnUserCooldown(userName))
            {
                Log("SoundEffect " + soundEffect.Name + " is on cooldown");
                return;
            }

            long currentUserCurrency1 = userName.Currency1;

            if (soundEffect.CostCurrency1 > currentUserCurrency1)
            {
                TwitchManager.SendChannelMessage($"{userName} you cannot afford {soundEffect.CostCurrency1.AsCur1()}, you only have {currentUserCurrency1}");
                return;
            }

            _ = Task.Run(async () =>
            {
                await new WaitForMainThread();
                soundEffect.LoadSoundClip(Globals);

                SoundManager.EnqueueSoundEffect(soundEffect);
            });

            TwitchManager.SendChannelMessage(soundEffect.Message);

            userName.Currency1 -= soundEffect.CostCurrency1;

            Task.Run(() => soundEffect.SetCooldownForUser(userName));
            Task.Run(() => soundEffect.SetCooldownForGlobal());
        }

        [Command("soundeffects", Description = "Displays all available sound effect triggers")]
        public void Command_SoundEffects([C_UserSelf] TwitchDBUser userName)
        {
            var sb = new StringBuilder();

            sb.Append($"{userName} available sound effects: ");

            sb.Append(string.Join(", ", SoundEffects
                .Where(s => TwitchManager.Subscribers.Any(sub => sub.DisplayName.ToLower() == s.UserName.ToLower()))
                .Select(c => Globals.S_CommandPrefix + c.Command)));

            TwitchManager.SendChannelMessage(sb.ToString());
        }

        public override void ExecuteSubscriberEvent(TwitchUser user, Subscription subscription)
        {
            SoundEffect soundEffect = SoundEffects.FirstOrDefault(f => f.UserName.ToLower() == user.DisplayName.ToLower());
            if (soundEffect != null && !SoundEffectsList.Any(c => c.ToLower() != soundEffect.Command.ToLower()))
            {
                EnableSubSoundEffect(soundEffect);
            }
        }

        public override void InitializeModule()
        {
            SoundEffects = Database.GetSoundEffects();

            PopulateSoundEffectCommands();
        }

        private void EnableSubSoundEffect(SoundEffect soundEffect)
        {
            if (!SoundEffectsList.Contains(soundEffect.Command))
            {
                SoundEffectsList.Add(soundEffect.Command);
            }
        }

        private void PopulateSoundEffectCommands()
        {
            foreach (SoundEffect soundEffect in SoundEffects.OrderBy(s => s.Command))
            {
                //if (TwitchManager.Subscribers.Any(sub => sub.DisplayName.ToLower() == soundEffect.UserName.ToLower()))
                //{
                    EnableSubSoundEffect(soundEffect);
                //}
            }
        }
    }
}