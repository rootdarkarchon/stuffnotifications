﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using TwitchLib.Client.Models;

namespace Assets.Modules
{
    public class EmoteChainModule : ChatModule
    {
        public Emote EmoteChainEmote;

        public int EmoteChainLength;

        public List<TwitchDBUser> EmoteChainParticipants;

        public TwitchDBUser LastEmoteChainParticipant;

        [Setting("EMOTE_CHAIN_EMOTE_VALUE", 10)]
        public int S_EmoteChainEmoteValue;

        [Setting("EMOTE_CHAIN_MIN_LENGTH", 5)]
        public int S_EmoteChainMinLength;

        [Setting("EMOTE_CHAIN_SUBSCRIBER_EMOTE_PREFIX", "rootda")]
        public string S_EmoteChainSubPrefix;

        [Setting("EMOTE_CHAIN_SUBSCRIBER_EMOTE_MULTIPLIER", 3)]
        public int S_EmoteChainSubscriberEmoteMultiplier;

        [Setting("NAME", "😂 EmoteChain")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("combobreaker", Description = "Breaks a current running emote chain and awards all [Globals.CURRENCY_1_NAME] to the combo breaking user.")]
        public void Command_Combobreaker([C_UserSelf] TwitchDBUser username)
        {
            if (EmoteChainLength >= S_EmoteChainMinLength)
            {
                TwitchManager.SendChannelMessage($"C-C-C-C-C-COMBOBREAKER! {username} gets it all now rootdaSmug");
                HandlePayoutForParticipants(true, username);
            }
        }

        [Command("emotechain", Description = "Explanation of how emote chains work")]
        public void Command_EmoteChain()
        {
            TwitchManager.SendChannelMessage($"Different users posting the same emote in consecutive messages will award {S_EmoteChainEmoteValue.AsCur1()} per chain length. Using subscriber emotes will multiply this by 3. Emote chains are counted from the 5th consecutive emote. !combobreaker will award only the person who does it.");
        }

        public override void ExecuteChatEvent(TwitchUser user, ChatMessage chatMessage)
        {
            if (chatMessage.DisplayName.ToLower() == TwitchManager.S_ClientUsername ||
                chatMessage.DisplayName.ToLower() == TwitchManager.S_BotUsername ||
                chatMessage.DisplayName.ToLower() == "streamelements" ||
                chatMessage.Message.StartsWith($"{Globals.S_CommandPrefix}combobreaker"))
            {
                return;
            }

            HandleEmotesInChain(user.TwitchDBUser, chatMessage.EmoteSet.Emotes);
        }

        public override void InitializeModule()
        {
            EmoteChainEmote = null;
            EmoteChainParticipants = new List<TwitchDBUser>();
            EmoteChainLength = 0;
            LastEmoteChainParticipant = null;
        }

        private void ClearEmoteChain()
        {
            EmoteChainLength = 0;
            EmoteChainEmote = null;
            EmoteChainParticipants.Clear();
            LastEmoteChainParticipant = null;
        }

        private int EmoteChainMultiplier()
        {
            if (EmoteChainEmote.Name.StartsWith(S_EmoteChainSubPrefix))
            {
                return S_EmoteChainSubscriberEmoteMultiplier;
            }
            else
            {
                return 1;
            }
        }

        private void HandleEmoteChainBreak(TwitchDBUser username)
        {
            if (EmoteChainLength >= S_EmoteChainMinLength)
            {
                try
                {
                    TwitchManager.SendChannelMessage($"{username} has broken the {EmoteChainLength} x {EmoteChainEmote.Name} chain!");
                    HandlePayoutForParticipants();
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }
            else
            {
                ClearEmoteChain();
            }
        }

        private void HandleEmoteChainMessage(TwitchDBUser username, List<Emote> emotes)
        {
            if (emotes.Any(e => e.Name == EmoteChainEmote.Name))
            {
                if (LastEmoteChainParticipant != username)
                {
                    EmoteChainLength++;
                    HandleEmoteChainParticipant(username);
                }
            }
            else
            {
                HandleEmoteChainBreak(username);
            }
        }

        private void HandleEmoteChainParticipant(TwitchDBUser username)
        {
            LastEmoteChainParticipant = username;
            if (!EmoteChainParticipants.Contains(username))
            {
                EmoteChainParticipants.Add(username);
            }
        }

        private void HandleEmotesInChain(TwitchDBUser username, List<Emote> emotes)
        {
            if (EmoteChainLength == 0 && emotes.Count > 0)
            {
                InitializeEmoteChain(username, emotes);
            }
            else
            {
                HandleEmoteChainMessage(username, emotes);
            }
        }

        private void HandlePayoutForParticipants(bool combobreaker = false, TwitchDBUser username = null)
        {
            string participants = string.Join(", ", EmoteChainParticipants);
            long amount = EmoteChainLength * EmoteChainMultiplier() * S_EmoteChainEmoteValue;

            if (combobreaker)
            {
                TwitchManager.SendChannelMessage($"{amount.AsCur1()} have been awarded to {username} only rootdaSmug");
                username.Currency1 += amount;
            }
            else
            {
                TwitchManager.SendChannelMessage($"{amount.AsCur1()} have been awarded to {participants}");
                foreach (TwitchDBUser user in EmoteChainParticipants)
                {
                    user.Currency1 += amount;
                }
            }

            ClearEmoteChain();
        }

        private void InitializeEmoteChain(TwitchDBUser username, List<Emote> emotes)
        {
            EmoteChainEmote = emotes.First();
            EmoteChainLength++;
            HandleEmoteChainParticipant(username);
        }
    }
}