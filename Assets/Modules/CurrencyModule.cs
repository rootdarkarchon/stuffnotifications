﻿using Assets.Base;
using Assets.Helpers;
using Assets.Managers;
using Assets.Models;
using Assets.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Client.Models;

namespace Assets.Modules
{
    internal class CurrencyModule : ChatModule
    {
        public List<TwitchDBUser> BoostedUsers;

        [NonSerialized]
        public TwitchDBUser CapitalismInitiator;

        private double CapitalismPercentage;
        public bool CapitalismWasRun;

        [NonSerialized]
        public TwitchDBUser CommunismInitiator;

        public bool CommunismWasRun;

        public string LastCheckedForChatters;

        [Setting("BOOST_MULTIPLIER", 3)]
        public int S_BoostMultiplier;

        [Setting("BOOST_TIME_MINUTES", 15)]
        public int S_BoostTimeMinutes;

        [Setting("BOOST_TIMEOUT_HOURS", 2)]
        public int S_BoostTimeoutHours;

        [Setting("CAPITALISM_TIMEOUT_HOURS", 5)]
        public int S_CapitalismTimeoutHours;

        [Setting("COMMUNISM_TIMEOUT_HOURS", 5)]
        public int S_CommunismTimeoutHours;

        [Setting("CURRENCY_2_COST", 10)]
        public int S_Currency2Cost;

        [Setting("CURRENCY_2_SALE", 7)]
        public int S_Currency2Sale;

        [Setting("IDLE_CURRENCY_1_SUBSCRIBER_MULTIPLIER", 3)]
        public int S_IdleCurrency1SubscriberMultiplier;

        [Setting("LURKER_BASE_CURRENCY", 100)]
        public int S_LurkerBaseCurrency1;

        [Setting("LURKER_TIMESPAN_MINUTES", 10)]
        public int S_LurkerTimeSpanMinutes;

        [Setting("NAME", "💰 Currency")]
        public string S_Name;

        [Setting("CAPITALISM_FAILURE_CHANCE", 30)]
        public int S_CapitalismFailureChance;

        [Setting("COMMUNISM_FAILURE_CHANCE", 30)]
        public int S_CommunismFailureChance;

        [Setting("TRADING_CURRENCY_2_TIMEOUT_MINUTES", 30)]
        public int S_TradingCurrency2TimeoutMinutes;

        [Setting("CHATTING_CURRENCY_1", 3)]
        public int S_ChattingCurrency1;

        public List<TwitchDBUser> TimedoutBoosters;
        public List<TwitchDBUser> TimedOutUsers;
        private Task lurkerCheckTask;
        private TimeSpan TradingTimeoutTimeSpan;
        public override string ModuleName => S_Name;

        [Command("boost", Description = "Boosts you for [CurrencyModule.BOOST_TIME_MINUTES] minutes and you will get [CurrencyModule.BOOST_MULTIPLIER]x [Globals.CURRENCY_1_NAME] for chatting. [CurrencyModule.BOOST_TIMEOUT_HOURS] hours timeout.")]
        [ValidateAttributeInTwitchDbList(nameof(BoostedUsers), typeof(C_UserSelfAttribute), false)]
        [ValidateAttributeInTwitchDbList(nameof(TimedoutBoosters), typeof(C_UserSelfAttribute), false)]
        public void Command_Boost([C_UserSelf] TwitchDBUser userName)
        {
            TwitchManager.SendChannelMessage($"{userName} you are now boosted for the next {S_BoostTimeMinutes} minutes and get {S_BoostMultiplier}x points for chatting!");

            Task.Run(() => BoostUser(userName));
        }

        [Command("buy", Description = "Allows you to buy [Globals.CURRENCY_2_NAME]. [CurrencyModule.CURRENCY_2_COST] [Globals.CURRENCY_1_NAME] = 1 [Globals.CURRENCY_2_NAME]. Used for leaderboards and interest. [CurrencyModule.TRADING_CURRENCY_2_TIMEOUT_MINUTES] minutes timeout for buying and selling.")]
        [ValidateAttributeInTwitchDbList(nameof(TimedOutUsers), typeof(C_UserSelfAttribute), false)]
        [ValidatePositiveAmount]
        [ValidateUserBuyAmount]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false, ErrorMessage = "You can't buy while a major event is in play")]
        public void Command_BuyCurrency2([C_UserSelf] TwitchDBUser userName, [C_Amount(currencyType: CurrencyType.Currency1)] long amount)
        {
            userName.Currency1 -= amount * S_Currency2Cost;
            userName.Currency2 += amount;

            TwitchManager.SendChannelMessage($"{userName} bought {amount.AsCur2()} and now has {userName.Currency1.AsCur1()} and {userName.Currency2.AsCur2()}. Time out for buying/selling is in effect until {DateTime.Now.Add(TradingTimeoutTimeSpan).ToString("T")}.");

            Task.Run(() => TimeoutUserForBuyingAndSelling(userName));
        }

        [Command("capitalism", Description = "CAPITALISM! Gives you [Globals.CURRENCY_1_NAME] of all users.", Cost = 1000, Currency = CurrencyType.Currency2)]
        [ValidateValueEquality(nameof(CapitalismWasRun), false, ErrorMessage = "Capitalism was already in effect")]
        [ValidateValueEquality(nameof(CapitalismInitiator), default(TwitchDBUser), ErrorMessage = "")]
        [ValidateValueEquality(nameof(CommunismInitiator), default(TwitchDBUser), ErrorMessage = "")]
        public void Command_Capitalism([C_UserSelf] TwitchDBUser userName)
        {
            GetAmountOfAllPeopleInChat(out List<TwitchDBUser> userList, out long totalCurrency1, userName);

            double randomPercentage = (long)(new Random(DateTime.Now.Millisecond).NextDouble() * (90 - 70)) + 70;

            UsersBlockedFromTransactions.AddRange(userList);

            TwitchManager.SendChannelMessage($"{userName} are you sure you want get {randomPercentage}% {Globals.S_Currency1Name} of {userList.Count} chatters? There is a {S_CapitalismFailureChance}% chance of failure and losing all of your {Globals.S_Currency1Name} and double of the cost in {Globals.S_Currency2Name}. Type !fuckyeah to confirm. It will cost 1000 {Globals.S_Currency2Name}. You have 60s to reply.");

            Task.Run(() => InitiateCapitalism(userName, userList, randomPercentage));
        }

        [Command("communism", Description = "COMMNUISM Товарищ! Distributes all [Globals.CURRENCY_1_NAME] between all users equally.", Cost = 100, Currency = CurrencyType.Currency2, DisplayInHelp = false)]
        [ValidateValueEquality(nameof(CommunismWasRun), false, ErrorMessage = "Товарищ, Communism is on cooldown")]
        [ValidateValueEquality(nameof(CommunismInitiator), default(TwitchDBUser), ErrorMessage = "")]
        [ValidateValueEquality(nameof(CapitalismInitiator), default(TwitchDBUser), ErrorMessage = "")]
        public void Command_Communism([C_UserSelf] TwitchDBUser userName)
        {
            GetAmountOfAllPeopleInChat(out List<TwitchDBUser> userList, out long totalCurrency1);

            UsersBlockedFromTransactions.AddRange(userList);

            TwitchManager.SendChannelMessage($"Товарищ {userName} are you sure you want to distribute all {Globals.S_Currency1Name} between {userList.Count} chatters? Type !davai to confirm. It will cost 100 {Globals.S_Currency2Name}. You have 60s to reply.");

            Task.Run(() => InitiateCommunism(userName, userList));
        }

        [Command("davai", Description = "Execute the Communism! Distribute the wealth equally! Plebeians rise!", Cost = 100, Currency = CurrencyType.Currency2, DisplayInHelp = false)]
        [ValidateValueEquality(nameof(CommunismInitiator), typeof(C_UserSelfAttribute), ErrorMessage = "")]
        [ValidateUserAmount(typeof(C_UserSelfAttribute), ErrorMessage = "Товарищ, Communism costs [C_Amount] [Globals.CURRENCY_2_NAME]. You do not have [C_Amount] [Globals.CURRENCY_2_NAME].")]
        public void Command_Davai([C_UserSelf] TwitchDBUser userName)
        {
            GetAmountOfAllPeopleInChat(out List<TwitchDBUser> chatterUsers, out long totalCurrency1);

            int randNr = new Random(DateTime.Now.Millisecond).Next(100);

            bool failedCommunism = randNr <= S_CommunismFailureChance;

            userName.Currency2 -= 100;

            if (!failedCommunism)
            {
                long currencyPerUser = totalCurrency1 / chatterUsers.Count;

                TwitchManager.SendChannelMessage($"Товарищ {userName} you are the hero of the пролетариат. Everyone now has {currencyPerUser.AsCur1()}! rootdaToot");

                foreach (TwitchDBUser user in chatterUsers)
                {
                    user.Currency1 = currencyPerUser;
                }
            }
            else
            {
                var userMaxCurrency = chatterUsers.OrderByDescending(f => f.Currency1).First();

                userMaxCurrency.Currency1 += userName.Currency1;
                userName.Currency1 -= userName.Currency1;

                TwitchManager.SendChannelMessage($"Товарищ {userName} your attempt at communism FAILED. All of your {Globals.S_Currency1Name} have been awarded to капиталист {userMaxCurrency}! rootdaStonk");
            }

            CommunismWasRun = true;
            CommunismInitiator = null;
            UsersBlockedFromTransactions.Clear();
            Task.Run(() => Cooldown_Communism());
        }

        [Command("fuckyeah", Description = "Execute the Capitalism! I want all the money! America fuck yeah!", Cost = 1000, Currency = CurrencyType.Currency2, DisplayInHelp = false)]
        [ValidateValueEquality(nameof(CapitalismInitiator), typeof(C_UserSelfAttribute), ErrorMessage = "")]
        [ValidateUserAmount(typeof(C_UserSelfAttribute), ErrorMessage = "Capitalism costs [C_Amount] [Globals.CURRENCY_2_NAME]. Shit isn't cheap ok.")]
        public void Command_Fuckyeah([C_UserSelf] TwitchDBUser userName)
        {
            GetAmountOfAllPeopleInChat(out List<TwitchDBUser> chatters, out long totalCurrency1, userName);

            long currency2Cost = 1000;

            totalCurrency1 = (long)(totalCurrency1 * (CapitalismPercentage / 100));

            int randNr = new Random(DateTime.Now.Millisecond).Next(100);

            bool failedCapitalism = randNr <= S_CapitalismFailureChance;

            userName.Currency2 -= failedCapitalism ? 2 * currency2Cost : currency2Cost;
            long capitalismUserCurrency = userName.Currency1;
            userName.Currency1 += failedCapitalism ? -capitalismUserCurrency : totalCurrency1;

            if (!failedCapitalism)
            {
                TwitchManager.SendChannelMessage($"Congratulations {userName} you now get {totalCurrency1.AsCur1()}. All users reduced their {Globals.S_Currency1Name} by {CapitalismPercentage}%. rootdaSmug");

                foreach (TwitchDBUser user in chatters)
                {
                    user.Currency1 -= (long)(user.Currency1 * (CapitalismPercentage / 100));
                }
            }
            else
            {
                TwitchManager.SendChannelMessage($"{userName} your attempt at capitalism FAILED. {capitalismUserCurrency.AsCur1()} and {currency2Cost.AsCur2()} will be distributed among {chatters.Count} chatters.");

                long addToEachUserCurr1 = capitalismUserCurrency / chatters.Count;
                long addToEachUserCurr2 = currency2Cost / chatters.Count;

                foreach (TwitchDBUser user in chatters)
                {
                    user.Currency1 += addToEachUserCurr1;
                    user.Currency2 += addToEachUserCurr2;
                }
            }

            CapitalismWasRun = true;
            CapitalismInitiator = null;
            UsersBlockedFromTransactions.Clear();
            Task.Run(() => Cooldown_Capitalism());
        }

        [Command("give", Description = "Allows you to give [Globals.CURRENCY_1_NAME] to other users.")]
        [ValidatePositiveAmount]
        [ValidateOtherUserExists]
        [ValidateUserAmount(typeof(C_UserSelfAttribute))]
        public void Command_Give([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser otherUser, [C_Amount] long amount)
        {
            userName.Currency1 -= amount;
            otherUser.Currency1 += amount;

            TwitchManager.SendChannelMessage($"{userName} gave {amount.AsCur1()} to {otherUser} and now has {userName.Currency1.AsCur1()} - {otherUser} now has {otherUser.Currency1.AsCur1()}");
        }

        [Command("sell", Description = "Allows you to sell [Globals.CURRENCY_2_NAME]. 1 [Globals.CURRENCY_2_NAME] = [CurrencyModule.CURRENCY_2_SALE] [Globals.CURRENCY_1_NAME]. [CurrencyModule.TRADING_CURRENCY_2_TIMEOUT_MINUTES] minutes timeout for buying and selling.")]
        [ValidateAttributeInTwitchDbList(nameof(TimedOutUsers), typeof(C_UserSelfAttribute), false)]
        [ValidatePositiveAmount]
        [ValidateUserAmount(typeof(C_UserSelfAttribute))]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false, ErrorMessage = "You can't sell while a major event is in play")]
        public void Command_SellCurrency2([C_UserSelf] TwitchDBUser userName, [C_Amount(currencyType: CurrencyType.Currency2)] long amount)
        {
            userName.Currency1 += amount * S_Currency2Sale;
            userName.Currency2 -= amount;

            TwitchManager.SendChannelMessage($"{userName} sold {amount.AsCur2()} and now has {userName.Currency1.AsCur1()} and {userName.Currency2.AsCur2()}. Time out for buying/selling is in effect until {DateTime.Now.Add(TradingTimeoutTimeSpan).ToString("T")}.");

            Task.Run(() => TimeoutUserForBuyingAndSelling(userName));
        }

        public override void ExecuteChatEvent(TwitchUser user, ChatMessage message) => AwardCurrencyForChatting(user, message);

        public override void InitializeModule()
        {
            TradingTimeoutTimeSpan = TimeSpan.FromMinutes(S_TradingCurrency2TimeoutMinutes);
            lurkerCheckTask = Task.Run(Background_AwardLurkingPoints);
            TimedOutUsers = new List<TwitchDBUser>();
            TimedoutBoosters = new List<TwitchDBUser>();
            BoostedUsers = new List<TwitchDBUser>();
            CommunismInitiator = null;
            CapitalismInitiator = null;
        }

        [Command("addpoints", ModeratorLevel = 1, Description = "Removes specified [Globals.CURRENCY_1_NAME] to specified user. Moderator action.")]
        [ValidateModerator]
        [ValidatePositiveAmount]
        [ValidateValueEquality(typeof(C_UserSelfAttribute), typeof(C_UserOtherAttribute), false)]
        [ValidateOtherUserExists]
        public void ModeratorCommand_AddCurrency1([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser otherUser, [C_Amount(allowAll: false)] long amount)
        {
            otherUser.Currency1 += amount;

            TwitchManager.SendChannelMessage($"{userName} added {amount.AsCur1()} to {otherUser}, they now have {otherUser.Currency1.AsCur1()}");
        }

        [Command("removepoints", ModeratorLevel = 1, Description = "Removes specified [Globals.CURRENCY_1_NAME] to specified user. Moderator action.")]
        [ValidateModerator]
        [ValidatePositiveAmount]
        [ValidateValueEquality(typeof(C_UserSelfAttribute), typeof(C_UserOtherAttribute), false)]
        [ValidateOtherUserExists]
        public void ModeratorCommand_RemovePoints([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser otherUser, [C_Amount(allowAll: false)] long amount)
        {
            otherUser.Currency2 -= amount;

            TwitchManager.SendChannelMessage($"{userName} subtracted {amount.AsCur1()} from {otherUser}, they now have {otherUser.Currency1.AsCur1()}");
        }

        public async override Task UpdateWithSettings()
        {
            if (lurkerCheckTask == null)
            {
                Log("Lurker Check Task was null, restarting");
                lurkerCheckTask = Task.Run(Background_AwardLurkingPoints);
            }

            TradingTimeoutTimeSpan = TimeSpan.FromMinutes(S_TradingCurrency2TimeoutMinutes);
        }

        private void AwardCurrencyForChatting(TwitchUser user, ChatMessage chatMessage)
        {
            TwitchDBUser tdbUser = user.TwitchDBUser;
            string message = chatMessage.Message;

            foreach (Emote emote in chatMessage.EmoteSet.Emotes)
            {
                message = message.Replace(emote.Name, "");
            }

            message = message.Replace(" ", "");

            int amount = message.Count() * S_ChattingCurrency1;

            if (BoostedUsers.Contains(tdbUser))
            {
                amount *= S_BoostMultiplier;
            }

            tdbUser.Currency1 += amount;
        }

        private async Task Background_AwardLurkingPoints()
        {
            if (string.IsNullOrEmpty(LastCheckedForChatters))
            {
                LastCheckedForChatters = DateTime.Now.Subtract(TimeSpan.FromMinutes(1)).ToString("o");
            }
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(60));

                try
                {
                    if (Math.Abs((DateTime.Parse(LastCheckedForChatters) - DateTime.Now).TotalMinutes) <= S_LurkerTimeSpanMinutes)
                    {
                        continue;
                    }

                    LastCheckedForChatters = DateTime.Now.ToString("o");

                    List<TwitchDBUser> chatters = TwitchManager.GetActiveChatters();

                    foreach (TwitchDBUser user in chatters)
                    {
                        long addedRubles = S_LurkerBaseCurrency1;
                        if (TwitchManager.Subscribers.Any(f => f.DisplayName.ToLower() == user.Username.ToLower()))
                        {
                            addedRubles *= S_IdleCurrency1SubscriberMultiplier;
                        }

                        user.Currency1 += addedRubles;
                    }
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }
        }

        private async Task BoostUser(TwitchDBUser username)
        {
            BoostedUsers = BoostedUsers ?? new List<TwitchDBUser>();
            BoostedUsers.Add(username);
            await Task.Delay(TimeSpan.FromMinutes(S_BoostTimeMinutes));
            TwitchManager.SendChannelMessage($"{username} your boost is over. Next boost in {S_BoostTimeoutHours}h ({DateTime.Now.Add(TimeSpan.FromHours(S_BoostTimeoutHours)).ToString("t")})");
            BoostedUsers.Remove(username);
            TimedoutBoosters.Add(username);
            await Task.Delay(TimeSpan.FromHours(S_BoostTimeoutHours));
            TimedoutBoosters.Remove(username);
        }

        private async Task Cooldown_Capitalism()
        {
            await Task.Delay(TimeSpan.FromHours(S_CapitalismTimeoutHours));
            CapitalismWasRun = false;
        }

        private async Task Cooldown_Communism()
        {
            await Task.Delay(TimeSpan.FromHours(S_CommunismTimeoutHours));
            CommunismWasRun = false;
        }

        private void GetAmountOfAllPeopleInChat(out List<TwitchDBUser> resultList, out long totalCurrency, TwitchDBUser ignoredUser = null)
        {
            var chatterUsers = TwitchManager.GetActiveChatters();
            resultList = new List<TwitchDBUser>(chatterUsers).Where(c => c != ignoredUser).ToList();
            totalCurrency = 0;
            foreach (TwitchDBUser chatter in resultList)
            {
                totalCurrency += chatter.Currency1;
            }
        }

        private async Task InitiateCapitalism(TwitchDBUser userName, List<TwitchDBUser> chatters, double capitalismPercentage)
        {
            CapitalismInitiator = userName;
            CapitalismPercentage = capitalismPercentage;
            await Task.Delay(TimeSpan.FromSeconds(60));
            if (CapitalismInitiator != null)
            {
                CapitalismInitiator = null;
                CapitalismPercentage = 0.0;
                TwitchManager.SendChannelMessage("Capitalism has been aborted.");
                UsersBlockedFromTransactions.RemoveAll(u => chatters.Contains(u));
            }
        }

        private async Task InitiateCommunism(TwitchDBUser userName, List<TwitchDBUser> chatters)
        {
            CommunismInitiator = userName;
            await Task.Delay(TimeSpan.FromSeconds(60));
            if (CommunismInitiator != null)
            {
                CommunismInitiator = null;
                TwitchManager.SendChannelMessage("Communism has been aborted.");
                UsersBlockedFromTransactions.RemoveAll(u => chatters.Contains(u));
            }
        }

        private async Task TimeoutUserForBuyingAndSelling(TwitchDBUser username)
        {
            TimedOutUsers = TimedOutUsers ?? new List<TwitchDBUser>();

            TimedOutUsers.Add(username);
            await Task.Delay(TradingTimeoutTimeSpan);
            TimedOutUsers.Remove(username);

            TwitchManager.SendPrivateMessage(username.Username, "hello, you can buy again");
        }
    }
}