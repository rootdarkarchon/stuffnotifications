﻿using Assets.Base;
using Assets.Models;
using System.Linq;
using System.Reflection;

namespace Assets.Modules
{
    public class HelpModule : ChatModule
    {
        [Setting("NAME", "Help")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("help", Description = "Shows usage and description of any command. Without command parameter displays all available commands.")]
        public void Command_Help([C_UserSelf] TwitchDBUser username, [C_Command] string commandName = "")
        {
            if (!string.IsNullOrEmpty(commandName))
            {
                ChatModule module = MainManager.RegisteredModules.SingleOrDefault(m => m.GetType().GetMethods().Any(z => z.GetCustomAttribute<CommandAttribute>()?.Command.ToLower() == commandName.ToLower()));
                MethodInfo method = module?.GetType().GetMethods().SingleOrDefault(m => m.GetCustomAttribute<CommandAttribute>()?.Command.ToLower() == commandName.ToLower());

                if (method == null)
                {
                    TwitchManager.SendChannelMessage($"{commandName} does not exist");
                    return;
                }

                CommandAttribute command = method.GetCustomAttribute<CommandAttribute>();

                string description = Globals.ReplaceTextWithSettings(command.Description);
                long cost = command.Cost;
                string costString = "";
                if (command.Cost > 0)
                {
                    costString = $"Cost: {command.Cost} {Globals.GetCurrencyName(command.Currency)} ⏸️";
                }
                string usage = GetCommandUsage(method);

                TwitchManager.SendChannelMessage($"{usage} ⏸️ {costString} {description}");
                return;
            }

            TwitchManager.SendChannelMessage(string.Join(" | ", MainManager.RegisteredModules
                .Where(m => m.ModuleName != ModuleName)
                .Where(m => m.GetType().GetMethods().Where(f => f.GetCustomAttribute<CommandAttribute>()?.Command != null && f.GetCustomAttribute<CommandAttribute>().ModeratorLevel == 0 && f.GetCustomAttribute<CommandAttribute>().DisplayInHelp).Count() > 0)
                .Select(m => m.ModuleName + ": " +
                    string.Join(", ", m.GetType().GetMethods().Where(f => f.GetCustomAttribute<CommandAttribute>()?.Command != null && f.GetCustomAttribute<CommandAttribute>()?.ModeratorLevel == 0 && f.GetCustomAttribute<CommandAttribute>().DisplayInHelp)
                    .Select(c => Globals.S_CommandPrefix + c.GetCustomAttribute<CommandAttribute>().Command)))));

            if (username.ModeratorLevel == 0)
            {
                return;
            }

            TwitchManager.SendChannelMessage(string.Join(" | ", MainManager.RegisteredModules
                .Where(m => m.ModuleName != ModuleName)
                .Where(m => m.GetType().GetMethods().Where(f => f.GetCustomAttribute<CommandAttribute>()?.Command != null && f.GetCustomAttribute<CommandAttribute>().ModeratorLevel > 0 && f.GetCustomAttribute<CommandAttribute>().DisplayInHelp).Count() > 0)
                .Select(m => m.ModuleName + ": " +
                    string.Join(", ", m.GetType().GetMethods().Where(f => f.GetCustomAttribute<CommandAttribute>()?.Command != null
                    && f.GetCustomAttribute<CommandAttribute>()?.ModeratorLevel > 0
                    && f.GetCustomAttribute<CommandAttribute>()?.ModeratorLevel <= username.ModeratorLevel
                    && f.GetCustomAttribute<CommandAttribute>().DisplayInHelp)
                    .Select(c => Globals.S_CommandPrefix + c.GetCustomAttribute<CommandAttribute>().Command)))));
        }
    }
}