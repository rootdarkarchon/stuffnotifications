﻿using Assets.Base;
using Assets.Helpers.AI;
using Assets.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Linq;
using Assets.Validators;
using System.Text;
using Asyncoroutine;
using System;
using System.Threading;
using Assets.Helpers;

public class StreamBaseDefenseModule : ChatModule
{
    public PlayerAIControl PlayerControl;

    public long DefenseCurrencyCost;

    public bool DefenseInitiated;

    public Task defenseStartCoroutine;

    public CancellationTokenSource DefenseStartToken;

    public bool DefenseStarted;

    public int PlayerCount;

    public List<TwitchDBUser> Players;

    public List<TwitchDBUser> TimedoutWavePlayers;

    public Camera DefendingCamera;

    public bool CameraIsRotating;

    [Setting("NAME", "🛡️ Potassium Defense Force")]
    public string S_Name;

    [Setting("DEFENSE_START_TIME", 60)]
    public int S_DefenseStart;

    public DefenseGameLogic DefenseLogic;
    public override string ModuleName => S_Name;

    [Command("abortdefense", ModeratorLevel = 1, Description = "Aborts a base defense game before it starts.")]
    [ValidateModerator]
    [ValidateValueEquality(nameof(DefenseInitiated), true, ErrorMessage = "Defense is not initiated!")]
    [ValidateValueEquality(nameof(DefenseStarted), false, ErrorMessage = "Defense is already started!")]
    public void Command_AbortDefense([C_UserSelf]TwitchDBUser user)
    {
        DefenseInitiated = false;
        DefenseStartToken.Cancel();
        _ = Task.Run(() => ReinitializeGame());
    }

    [Command("defend", ModeratorLevel = 1, Description = "Starts a base defense game for a set amount of currency.")]
    [ValidateValueEquality(nameof(DefenseInitiated), false)]
    [ValidateModerator]
    [ValidateAttributeInRange(typeof(C_AmountAttribute), 1000, 3000)]
    public void Command_Defend([C_UserSelf]TwitchDBUser user, [C_Amount(CurrencyType = CurrencyType.Currency2)] long amount)
    {
        DefenseCurrencyCost = amount;

        DefenseLogic.RemainingCurrencyToDefend = 0;

        PlayerCount = 0;

        TwitchManager.SendChannelMessage($"A defense game has started! If you want to join the defense game type {Globals.S_CommandPrefix}jd");
        TwitchManager.SendChannelMessage($"Use {Globals.S_CommandPrefix}[n/e/s/w] for moving in the cardinal directions. Shooting is automated.");

        DefenseStartToken?.Dispose();
        DefenseStartToken = new CancellationTokenSource();
        defenseStartCoroutine = Task.Run(async () =>
        {
            await ReinitializeGame();
            DefenseInitiated = true;
            await StartDefense(DefenseStartToken);
        });
    }

    [Command("go", DisplayInHelp = false)]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), true)]
    public void Command_Go([C_UserSelf]TwitchDBUser user, [C_Direction] DefenseDirection dir)
    {
        if (user.PlayerAI == null) return;
        Task.Run(async () =>
        {
            await new WaitForMainThread();
            await PlayerControl.MovePlayerDestination(user.PlayerAI, dir);
        });
    }

    [Command("e", DisplayInHelp = false)]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), true)]
    public void Command_GoEast([C_UserSelf]TwitchDBUser user)
    {
        Command_Go(user, DefenseDirection.East);
    }

    [Command("n", DisplayInHelp = false)]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), true)]
    public void Command_GoNorth([C_UserSelf]TwitchDBUser user)
    {
        Command_Go(user, DefenseDirection.North);
    }

    [Command("s", DisplayInHelp = false)]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), true)]
    public void Command_GoSouth([C_UserSelf]TwitchDBUser user)
    {
        Command_Go(user, DefenseDirection.South);
    }

    [Command("w", DisplayInHelp = false)]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), true)]
    public void Command_GoWest([C_UserSelf]TwitchDBUser user)
    {
        Command_Go(user, DefenseDirection.West);
    }

    [Command("jd", DisplayInHelp = false)]
    [ValidateValueEquality(nameof(PlayerCount), 4, false, ErrorMessage = "All slots are full!")]
    [ValidateValueEquality(nameof(DefenseStarted), false, ErrorMessage = "Defense already started!")]
    [ValidateValueEquality(nameof(DefenseInitiated), true, ErrorMessage = "No defense is currently running!")]
    [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false, ErrorMessage = "Cannot join defense. You are currently blocked from transactions.")]
    [ValidateAttributeInTwitchDbList(nameof(Players), typeof(C_UserSelfAttribute), false, ErrorMessage = "Cannot join twice!")]
    public void Command_JoinDefense([C_UserSelf]TwitchDBUser user)
    {
        long currentUserCurrency1 = user.Currency2;
        if (DefenseCurrencyCost > currentUserCurrency1)
        {
            TwitchManager.SendChannelMessage($"{user} you cannot afford to join the defense. You need to have {DefenseCurrencyCost} {Globals.S_Currency2Name}.");
            return;
        }

        Task.Run(async () =>
        {
            await new WaitForMainThread();
            DefenseLogic.RemainingCurrencyToDefend += DefenseCurrencyCost;
            await SpawnPlayer(user);
            UsersBlockedFromTransactions.Add(user);
            TwitchManager.SendChannelMessage($"{user.Username} joined the defense! Your color is {user.PlayerAI.SpokenColor.ToUpper()}. {PlayerCount}/4 players are in the game now.");
        });
    }

    [Command("spin", Cost = 250, Currency = CurrencyType.Currency2)]
    [ValidateValueEquality(nameof(CameraIsRotating), false, ErrorMessage = "[C_UserSelf] the spin is still going.")]
    [ValidateValueEquality(nameof(DefenseStarted), true, ErrorMessage = "No defense is currently running!")]
    [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false, ErrorMessage = "")]
    public void Command_Spin([C_UserSelf]TwitchDBUser user)
    {
        CameraIsRotating = true;

        user.Currency2 -= 250;

        TwitchManager.SendChannelMessage($"{user} span the wheel of fortune, you better learn to navigate! (250 {Globals.S_Currency2Name} subtracted from {user})");

        Task.Run(async () =>
        {
            await DefenseLogic.SpinCamera();
            CameraIsRotating = false;
        });
    }

    [Command("wave", Currency = CurrencyType.Currency2)]
    [ValidateAttributeInRange(typeof(C_AmountAttribute), 50, 350)]
    [ValidateUserAmount(typeof(C_UserSelfAttribute), ErrorMessage = "You cannot afford spawning this wave!")]
    [ValidateValueEquality(nameof(DefenseStarted), true, ErrorMessage = "No defense is currently running!")]
    [ValidateAttributeInTwitchDbList(nameof(TimedoutWavePlayers), typeof(C_UserSelfAttribute), false, ErrorMessage = "")]
    //[ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false, ErrorMessage = "")]
    public void Command_Wave([C_UserSelf]TwitchDBUser user, [C_Amount] long amount, [C_Direction] DefenseDirection attackVector)
    {
        var origAmount = amount;
        if (PlayerCount == 1 && amount >= 75) amount = 75;
        if (PlayerCount == 2 && amount >= 150) amount = 150;
        if (PlayerCount == 3 && amount >= 225) amount = 225;

        if(amount != origAmount)
        {
            TwitchManager.SendChannelMessage($"{user} due to the amount of players ({PlayerCount}) your wave has been clamped to {amount} enemies.");
        }

        TwitchManager.SendChannelMessage($"RED ALERT: An imminent wave of {amount} has been sighted from {attackVector.ToString()}");

        Task.Run(() => TimeoutWaveSpawner(user));

        user.Currency2 -= amount;

        Task.Run(() => DefenseLogic.SpawnWave(amount, attackVector, user));
    }

    public async Task TimeoutWaveSpawner(TwitchDBUser user)
    {
        TimedoutWavePlayers.Add(user);
        await Task.Delay(TimeSpan.FromSeconds(120));
        TimedoutWavePlayers.Remove(user);
    }

    public async Task AwardUserForWave(AttackWave wave)
    {
        while (!wave.WaveFullyCompleted)
        {
            await Task.Delay(1000);
        }

        var award = wave.StolenBananas * 2;

        TwitchManager.SendChannelMessage($"{wave.User} your wave of {wave.TotalEnemies} enemies awarded you {award.AsCur2()} rootdaSmug");
        wave.User.Currency2 += award;
    }

    public async Task DefenseEnd(bool won)
    {
        await new WaitForMainThread();

        DefenseLogic.DefenseStarted = false;
        DefenseInitiated = false;
        DefenseStarted = false;

        if (won)
        {
            TwitchDBUser mvp = Players.OrderByDescending(p => p.PlayerAI.Stat_BananaSaves * 2 + p.PlayerAI.Stat_Kills).First();
            TwitchDBUser agile = Players.OrderByDescending(p => p.PlayerAI.Stat_TotalMoves).First();
            TwitchDBUser destroyer = Players.OrderByDescending(p => p.PlayerAI.Stat_Kills).First();
            TwitchDBUser savior = Players.OrderByDescending(p => p.PlayerAI.Stat_BananaSaves).First();

            var doubleCurrency = DefenseLogic.RemainingCurrencyToDefend;
            for (int i = 0; i < doubleCurrency; i++)
            {
                DefenseLogic.RemainingCurrencyToDefend++;
                if (i % 5 == 0) await Task.Delay(3);
            }

            int playerNumber = 0;
            while (DefenseLogic.RemainingCurrencyToDefend > 0)
            {
                Players[playerNumber].PlayerAI.Stat_Kills++;
                if (++playerNumber >= Players.Count) playerNumber = 0;
                await Task.Delay(5);
                DefenseLogic.RemainingCurrencyToDefend--;
            }

            foreach (var player in Players)
            {
                player.Currency2 += player.PlayerAI.Stat_Kills;
                TwitchManager.SendChannelMessage($"{player} received {player.PlayerAI.Stat_Kills.AsCur2()} for a successful defense!");
            }

            if (Players.Count > 1)
            {
                int additionalRewards = (int)(DefenseCurrencyCost / 4);

                string stats = $"Defense Stats: MVP: {mvp} // Destroyer: {destroyer} // Runner: {agile} // Savior: {savior} // each award gives {additionalRewards.AsCur2()} in addition!";
                mvp.Currency2 += additionalRewards;
                agile.Currency2 += additionalRewards;
                destroyer.Currency2 += additionalRewards;
                savior.Currency2 += additionalRewards;
                TwitchManager.SendChannelMessage(stats);
            }
        }

        await Task.Delay(TimeSpan.FromSeconds(5));

        await ReinitializeGame();
    }

    public async override void InitializeModule()
    {
        Players = new List<TwitchDBUser>();
        TimedoutWavePlayers = new List<TwitchDBUser>();
        DefendingCamera.enabled = false;
        await PlayerControl.WaitForInit();
    }

    private async Task ReinitializeGame()
    {
        await new WaitForMainThread();

        foreach (var kvp in DefenseLogic.WaveHelpers)
        {
            foreach (var value in new List<EnemyAI>(kvp.SpawnedEnemies))
            {
                Destroy(value.gameObject);
            }
        }

        foreach (var kvp in Players)
        {
            Destroy(kvp.PlayerAI.gameObject);
            kvp.PlayerAI.Stat_Kills = 0;
            kvp.PlayerAI.UI_KillsText.text = "";
            kvp.PlayerAI.UI_NameText.text = "";
            kvp.PlayerAI = null;
        }

        foreach (var trans in PlayerControl.Positions)
        {
            trans.Occupied = false;
        }

        DefenseLogic.gameObject.GetComponentInChildren<IntroUITextHelper>().ResetTextAndCamera(true);

        DefendingCamera.enabled = false;

        DefenseLogic.DefenseStarted = false;
        DefenseInitiated = false;
        DefenseStarted = false;
        PlayerCount = 0;

        foreach (var user in Players)
        {
            UsersBlockedFromTransactions.Remove(user);
        }

        Players = new List<TwitchDBUser>();
    }

    private async Task SpawnPlayer(TwitchDBUser user)
    {
        PlayerCount++;
        var playerAI = PlayerControl.SpawnPlayer(user, PlayerCount);
        Players.Add(user);
        user.PlayerAI = playerAI;
    }

    private async Task StartDefense(CancellationTokenSource ct)
    {
        await new WaitForMainThread();

        DefendingCamera.enabled = true;

        DefenseLogic.TimeRemaining = S_DefenseStart;

        await DefenseLogic.StartCountdown(S_DefenseStart, ct, false);

        if (ct.IsCancellationRequested)
        {
            _ = Task.Run(() => ReinitializeGame());
            return;
        }

        if (Players.Count == 0)
        {
            TwitchManager.SendChannelMessage("Nobody has joined the defense game, game aborted!");
            _ = Task.Run(() => ReinitializeGame());
            return;
        }

        foreach (var player in Players)
        {
            player.Currency2 -= DefenseCurrencyCost;
        }

        TwitchManager.SendChannelMessage($"{DefenseCurrencyCost.AsCur2()} has been deducted from {string.Join(", ", Players)}. Good luck.");

        DefenseLogic.PlayerCount = PlayerCount;
        DefenseLogic.StartDefense();
        DefenseStarted = true;
    }
}
