﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Threading.Tasks;
using TwitchLib.Client.Models;
using UnityEngine;

namespace Assets.Modules
{
    public class WelcomeModule : ChatModule
    {
        [Setting("FOLLOWING_CURRENCY_1_BONUS", 5000)]
        public long S_FollowingCurrency1Bonus;

        [Setting("NAME", "Welcome")]
        public string S_Name;

        [Setting("RETURNING_BASE_INTEREST", 0.5)]
        public double S_ReturningBaseInterest;

        [Setting("RETURNING_CONSECUTIVE_INTEREST_INCERASE", 0.25)]
        public double S_ReturningConsecutiveInterestIncrease;

        [Setting("RETURNING_MAXIMUM_INTEREST", 5)]
        public int S_ReturningMaximumInterest;

        [Setting("WELCOME_BACK_CURRENCY_1_BONUS", 1000)]
        public int S_WelcomeBackCurrency1Bonus;

        [Setting("WELCOME_CURRENCY_1_BONUS", 500)]
        public int S_WelcomeCurrency1Bonus;

        [Setting("MAX_INTEREST_BONUS", 10000)]
        public int S_MaximumInterestBonus;

        public AudioClip WelcomeBackSound;
        public AudioClip WelcomeSound;
        public override string ModuleName => S_Name;

        public override async void ExecuteChatEvent(TwitchUser user, ChatMessage chatMessage)
        {
            if (chatMessage.DisplayName.ToLower() == TwitchManager.S_ClientUsername.ToLower() ||
                chatMessage.DisplayName.ToLower() == TwitchManager.S_BotUsername.ToLower() ||
                chatMessage.DisplayName.ToLower() == "streamelements")
            {
                return;
            }

            await WelcomeUser(user);
        }

        public override async void ExecuteCommandEvent(TwitchUser user, ChatMessage chatMessage)
        {
            if (chatMessage.DisplayName.ToLower() == TwitchManager.S_ClientUsername.ToLower() ||
                chatMessage.DisplayName.ToLower() == TwitchManager.S_BotUsername.ToLower() ||
                chatMessage.DisplayName.ToLower() == "streamelements")
            {
                return;
            }

            await WelcomeUser(user);
        }

        public override async void ExecuteFollowerEvent(TwitchUser newFollower)
        {
            await ThankUserForFollow(newFollower);
        }

        private void CalculateAndAddInterest(TwitchDBUser user, long lastSeenStreamId, long lastStreamId)
        {
            double adjustedInterest;
            // when user was in last stream just add the returning consecutive interest increase
            if (lastSeenStreamId == lastStreamId)
            {
                adjustedInterest = user.Interest + S_ReturningConsecutiveInterestIncrease;
                if (adjustedInterest > S_ReturningMaximumInterest)
                {
                    adjustedInterest = S_ReturningMaximumInterest;
                }
            }
            else
            {
                // otherwise subtract the amount of streams they missed from their consecutive streams they have been to
                adjustedInterest = user.Interest - ((lastStreamId - lastSeenStreamId) * S_ReturningConsecutiveInterestIncrease);
                if (adjustedInterest <= S_ReturningBaseInterest)
                {
                    adjustedInterest = S_ReturningBaseInterest;
                }
            }

            user.Interest = adjustedInterest;

            if (user.Interest > 0.5)
            {
                int interestAdjustment = (int)Math.Ceiling(user.Currency2 * (user.Interest / 100));

                // cap interest
                interestAdjustment = interestAdjustment > S_MaximumInterestBonus ? S_MaximumInterestBonus : interestAdjustment;

                user.Currency2 += interestAdjustment;

                string capped = interestAdjustment == S_MaximumInterestBonus ? "[cap]" : "";

                TwitchManager.SendChannelMessage($"{user} adjusted {Globals.S_Currency2Name} for attending consecutive streams, interest rate of {user.Interest}%: {interestAdjustment.AsCur2()} {capped} for a total of {user.Currency2.AsCur2()}");
            }
        }

        private async Task ThankUserForFollow(TwitchUser follower)
        {
            await Task.Delay(1000);
            while (MainManager.FollowerQueue.Contains(follower))
            {
                await Task.Delay(1000);
            }
            TwitchManager.SendChannelMessage($"rootdaToot Thank you for the follow, {follower.DisplayName} rootdaToot check out !help for all chat commands! (PS: I also have Discord rootdaSmug )");
            follower.TwitchDBUser.Currency1 += S_FollowingCurrency1Bonus;
        }

        private async Task WelcomeUser(TwitchUser user)
        {
            try
            {
                WelcomeMessage welcomeMessage = null;

                TwitchUser localuser = user;

                if (localuser?.TwitchDBUser.LastSeenStreamId == TwitchManager.ActiveStream.Id)
                {
                    return;
                }

                if (localuser.TwitchDBUser.LastSeenStreamId > 0 || TwitchManager.Followers.Contains(user))
                {
                    Log("User was here before: " + user.DisplayName);
                    welcomeMessage = new WelcomeMessage(localuser, WelcomeBackSound, false);
                }
                else
                {
                    Log("User was never here before: " + user.DisplayName);
                    welcomeMessage = new WelcomeMessage(localuser, WelcomeSound, true);
                }

                localuser.TwitchDBUser.Currency1 += welcomeMessage.FirstTime ? S_WelcomeBackCurrency1Bonus : S_WelcomeBackCurrency1Bonus;
                MainManager.WelcomeMessageQueue.Add(welcomeMessage);

                TwitchManager.SendChannelMessage(welcomeMessage.WelcomeMessageTwitchText);
                CalculateAndAddInterest(localuser.TwitchDBUser, localuser.TwitchDBUser.LastSeenStreamId, TwitchManager.ActiveStream.Id - 1);
                localuser.TwitchDBUser.LastSeenStreamId = TwitchManager.ActiveStream.Id;
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }
    }
}