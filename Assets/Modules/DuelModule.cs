﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Assets.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assets.Modules
{
    [Serializable]
    public class Duel
    {
        public long Amount;
        public TwitchDBUser Challenger;
        public TwitchDBUser Opponent;
    }

    [Serializable]
    public class DuelModule : ChatModule
    {
        public List<Duel> Duelists;

        [Setting("NAME", "⚔️ Duel")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("accept", Description = "Accept a duel request", DisplayInHelp = false)]
        [ValidateUserInDuel(nameof(Duelists), typeof(C_UserSelfAttribute), true)]
        public void Command_Accept([C_UserSelf] TwitchDBUser userName)
        {
            Duel duelistTuple = Duelists.SingleOrDefault(f => f.Opponent == userName);

            var random = new Random(DateTime.Now.Millisecond);

            TwitchDBUser winner;
            TwitchDBUser loser;
            if (random.Next(100) >= 50)
            {
                winner = duelistTuple.Challenger;
                loser = duelistTuple.Opponent;
            }
            else
            {
                winner = duelistTuple.Opponent;
                loser = duelistTuple.Challenger;
            }

            winner.Currency1 += duelistTuple.Amount;
            loser.Currency1 -= duelistTuple.Amount;

            TwitchManager.SendChannelMessage($"{winner} has won the duel and won {duelistTuple.Amount.AsCur1()}!");

            Duelists.Remove(duelistTuple);
            UsersBlockedFromTransactions.Remove(duelistTuple.Challenger);
            UsersBlockedFromTransactions.Remove(duelistTuple.Opponent);
        }

        [Command("deny", Description = "Deny a duel request.", DisplayInHelp = false)]
        [ValidateUserInDuel(nameof(Duelists), typeof(C_UserSelfAttribute), true)]
        public void Command_Deny([C_UserSelf] TwitchDBUser userName)
        {
            Duel duelistTuple = Duelists.SingleOrDefault(f => f.Opponent == userName);

            TwitchManager.SendChannelMessage($"{userName} has denied the duel!");

            Duelists.Remove(duelistTuple);
            UsersBlockedFromTransactions.Remove(duelistTuple.Challenger);
            UsersBlockedFromTransactions.Remove(duelistTuple.Opponent);
        }

        [Command("duel", Description = "Allows you to duel another user for [Globals.CURRENCY_1_NAME].")]
        [ValidatePositiveAmount]
        [ValidateOtherUserExists]
        [ValidateUserAmount(typeof(C_UserSelfAttribute), ErrorMessage = "You don't have [C_Amount] [Globals.CURRENCY_1_NAME]")]
        [ValidateUserAmount(typeof(C_UserOtherAttribute))]
        [ValidateValueEquality(typeof(C_UserSelfAttribute), typeof(C_UserOtherAttribute), false, ErrorMessage = "[C_UserSelf] you cannot duel yourself!")]
        [ValidateUserInDuel(nameof(Duelists), typeof(C_UserOtherAttribute), false, ErrorMessage = "[C_UserOther] is already in a duel.")]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false)]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserOtherAttribute), false)]
        public void Command_Duel([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser duelUser, [C_Amount] long amount)
        {
            TwitchManager.SendChannelMessage($"{duelUser}! {userName} challenges you for {amount.AsCur1()}! Type {Globals.S_CommandPrefix}accept or {Globals.S_CommandPrefix}deny to respond to the duel! You have 60 seconds to respond!");

            var duel = new Duel { Challenger = userName, Opponent = duelUser, Amount = amount };

            Duelists.Add(duel);
            UsersBlockedFromTransactions.Add(duel.Challenger);
            UsersBlockedFromTransactions.Add(duel.Opponent);

            Task.Run(() => RemoveDuelist(duel));
        }

        public override void InitializeModule()
        {
            Duelists = new List<Duel>();
        }

        public async override Task UpdateWithSettings()
        {
            if (Duelists == null)
            {
                Log("Rebuilding duelists");
                Duelists = new List<Duel>();
            }
        }

        private async Task RemoveDuelist(Duel duelist)
        {
            await Task.Delay(TimeSpan.FromSeconds(60));

            if (Duelists.Contains(duelist))
            {
                UsersBlockedFromTransactions.Remove(duelist.Challenger);
                UsersBlockedFromTransactions.Remove(duelist.Opponent);
                TwitchManager.SendChannelMessage($"{duelist.Opponent} has not responded to {duelist.Challenger} in time! The duel is aborted.");
                Duelists.Remove(duelist);
            }
        }
    }
}