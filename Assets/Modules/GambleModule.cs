﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Assets.Validators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Assets.Modules
{
    public class GambleModule : ChatModule
    {
        public List<TwitchDBUser> JoinedInRaffle;

        public bool RaffleRunning;

        public GambleAnimationLogic GambleAnimationLogic;

        [Setting("GAMBLE_ALL_IN_MULTIPLIER", 1.5)]
        public double S_GambleAllInMultiplier;

        [Setting("GAMBLE_TIMEOUT_SECONDS", 60)]
        public int S_GambleTimeoutSeconds;

        [Setting("NAME", "🎰 Gamble")]
        public string S_Name;

        public List<TwitchDBUser> TimedoutGamblers;
        public override string ModuleName => S_Name;

        [Command("gamble", Description = "Allows you to gamble [Globals.CURRENCY_1_NAME]. Gambling all gives you a max payout of [GambleModule.GAMBLE_ALL_IN_MULTIPLIER]x.")]
        [ValidateAttributeInTwitchDbList(nameof(TimedoutGamblers), typeof(C_UserSelfAttribute), false)]
        [ValidatePositiveAmount]
        [ValidateUserAmount]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false)]
        public void Command_Gamble([C_UserSelf] TwitchDBUser userName, [C_Amount] long amount)
        {
            GambleAnimationLogic.QueuedGambles.Add(new Gamble
            {
                Amount = amount,
                GambleType = GambleType.Coin,
                User = userName
            });

            Task.Run(() => TimeoutGambler(userName));
        }

        [Command("dice", Description = "Allows you to gamble [Globals.CURRENCY_1_NAME] with dice. Gambling pays you triple what you invested. Gambling all mulitplies the potential win by [GambleModule.GAMBLE_ALL_IN_MULTIPLIER]x.")]
        [ValidateAttributeInTwitchDbList(nameof(TimedoutGamblers), typeof(C_UserSelfAttribute), false)]
        [ValidatePositiveAmount]
        [ValidateUserAmount]
        [ValidateAttributeInTwitchDbList(nameof(UsersBlockedFromTransactions), typeof(C_UserSelfAttribute), false)]
        public void Command_Dice([C_UserSelf] TwitchDBUser userName, [C_Amount] long amount)
        {
            GambleAnimationLogic.QueuedGambles.Add(new Gamble
            {
                Amount = amount,
                GambleType = GambleType.Dice,
                User = userName
            });

            Task.Run(() => TimeoutGambler(userName));
        }

        [Command("join", Description = "Allows you to join a running raffle.", DisplayInHelp = false)]
        [ValidateAttributeInTwitchDbList(nameof(JoinedInRaffle), typeof(C_UserSelfAttribute), false)]
        [ValidateValueEquality(nameof(RaffleRunning), true)]
        public void Command_JoinRaffle([C_UserSelf] TwitchDBUser userName)
        {
            JoinedInRaffle.Add(userName);
            TwitchManager.SendChannelMessage($"{userName} joined the running raffle! ({JoinedInRaffle.Count} users)");
        }

        [Command("raffle", ModeratorLevel = 1, Description = "Allows you to start a raffle for users to join in and win currency.")]
        [ValidateModerator]
        [ValidatePositiveAmount]
        [ValidatePositiveAmount(typeof(C_TimeAttribute))]
        [ValidateValueEquality(nameof(RaffleRunning), false, ErrorMessage = "A raffle is already running.")]
        [ValidateAttributeAgainstLongValue(typeof(C_TimeAttribute), 300, ErrorMessage = "The time of [C_Time]s is too much. Maximum is 300s")]
        public void Command_Raffle([C_UserSelf] TwitchDBUser userName, [C_Amount(allowAll: false)] long amount, [C_Time] long time = 120)
        {
            TwitchManager.SendChannelMessage($"{userName} started a raffle for {amount.AsCur1()}! rootdaToot It will end in {time} seconds! Type in {Globals.S_CommandPrefix}join to join! rootdaAllEars");
            Task.Run(() => RaffleTask(time, amount));
            RaffleRunning = true;
        }

        public override void InitializeModule()
        {
            TimedoutGamblers = new List<TwitchDBUser>();
            RaffleRunning = false;
            JoinedInRaffle = new List<TwitchDBUser>();
        }

        public async Task RaffleTask(long time, long amount)
        {
            time = (int)(time / 2);
            await Task.Delay(TimeSpan.FromSeconds((int)time));
            TwitchManager.SendChannelMessage($"The raffle for {amount.AsCur1()} is half way rootdaStonk Don't forget to {Globals.S_CommandPrefix}join in");
            await Task.Delay(TimeSpan.FromSeconds((int)time));

            RaffleRunning = false;

            if (JoinedInRaffle.Count > 0)
            {
                var random = new Random(DateTime.Now.Millisecond);
                TwitchDBUser winner = JoinedInRaffle[random.Next(0, JoinedInRaffle.Count)];

                winner.Currency1 += amount;

                TwitchManager.SendChannelMessage($"The raffle is over and {winner} won {amount.AsCur1()}! rootdaToot");
            }
            else
            {
                TwitchManager.SendChannelMessage("The raffle ended and nobody joined in FeelsBadMan");
            }

            JoinedInRaffle = new List<TwitchDBUser>();
        }

        private async Task TimeoutGambler(TwitchDBUser userName)
        {
            Log($"{userName} gambled and is timed out");
            TimedoutGamblers.Add(userName);
            await Task.Delay(TimeSpan.FromSeconds(S_GambleTimeoutSeconds));
            TimedoutGamblers.Remove(userName);
            Log($"{userName} timeout over");
        }
    }
}