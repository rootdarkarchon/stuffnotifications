﻿using Assets.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web;
using Assets.Models;
using Assets.Validators;

namespace Assets.Modules
{
    public class SpotifyModule : ChatModule
    {
        [Setting("Name", "🎵 Spotify")]
        public string S_ModuleName;

        [Setting("REFRESH_TOKEN", "")]
        public string S_RefreshToken;

        [Setting("CLIENT_ID", "")]
        public string S_ClientID;

        [Setting("SECRET_ID", "")]
        public string S_SecretID;

        public SpotifyWebAPI WebAPI;
        public Token AuthToken;
        public AuthorizationCodeAuth auth;
        public FullTrack CurrentlyPlaying;

        public override string ModuleName => S_ModuleName;

        [Command("np")]
        public void Command_NowPlaying()
        {
            var track = WebAPI.GetPlayingTrack();
            if (track.IsPlaying)
            {
                TwitchManager.SendChannelMessage("Currently playing " + TrackToString(track, true));
            }
            else
            {
                TwitchManager.SendChannelMessage("Nothing is currently playing.");
            }
        }

        public string TrackToString(PlaybackContext track, bool showProgress = false)
        {
            var progress = showProgress ? $"[" + TimeSpan.FromMilliseconds(track.ProgressMs).ToString(@"mm\:ss") + " / " + TimeSpan.FromMilliseconds(track.Item.DurationMs).ToString(@"mm\:ss") + "]" : "";
            return TrackToString(track.Item) + progress;
        }

        public string TrackToString(FullTrack track)
        {
            return "\"" + track.Name + "\" by \"" + string.Join(", ", track.Artists.Select(a => a.Name)) + "\" from Album \"" + track.Album.Name + "\" ";
        }

        [Command("skip", ModeratorLevel = 1)]
        [ValidateModerator(ErrorMessage = "")]
        public void Command_Skip([C_UserSelf] TwitchDBUser user)
        {
            WebAPI.SkipPlaybackToNext();
        }

        [Command("m_pause", ModeratorLevel = 1)]
        [ValidateModerator(ErrorMessage = "")]
        public void Command_Pause([C_UserSelf] TwitchDBUser user)
        {
            WebAPI.PausePlayback();
        }

        [Command("m_play", ModeratorLevel = 1)]
        [ValidateModerator(ErrorMessage = "")]
        public void Command_Play([C_UserSelf] TwitchDBUser user)
        {
            WebAPI.ResumePlayback(offset: "");
        }

        public async override void InitializeModule()
        {
            auth = new AuthorizationCodeAuth(S_ClientID, S_SecretID, "http://localhost:4002", "http://localhost:4002",
                (Scope.UserModifyPlaybackState | Scope.UserReadCurrentlyPlaying | Scope.UserReadPlaybackState | Scope.PlaylistModifyPrivate | Scope.PlaylistReadPrivate));

            _ = RefreshTokenTask();
            _ = CurrentlyPlayingTask();

            if (!string.IsNullOrEmpty(S_RefreshToken)) return;

            auth.AuthReceived += async (sender, payload) =>
            {
                auth.Stop();
                AuthToken = await auth.ExchangeCode(payload.Code);
                S_RefreshToken = AuthToken.RefreshToken;
                SettingsManager.SetSetting("REFRESH_TOKEN", S_RefreshToken, this);
            };

            auth.Start();
            Log(auth.GetUri());
        }

        public async Task RefreshTokenTask()
        {
            while (true)
            {
                if ((AuthToken == null || (AuthToken != null && AuthToken.IsExpired())) && !string.IsNullOrEmpty(S_RefreshToken))
                {
                    AuthToken = await auth.RefreshToken(S_RefreshToken);
                    if (WebAPI == null)
                    {
                        WebAPI = new SpotifyWebAPI();
                    }
                    WebAPI.AccessToken = AuthToken.AccessToken;
                    WebAPI.TokenType = AuthToken.TokenType;
                }

                await Task.Delay(TimeSpan.FromSeconds(30));
            }
        }

        public async Task CurrentlyPlayingTask()
        {
            while (WebAPI == null)
            {
                await Task.Delay(1000);
            }

            while (true)
            {
                var playing = WebAPI.GetPlayingTrack();
                if (playing.IsPlaying && (CurrentlyPlaying == null || CurrentlyPlaying.Id != playing.Item.Id))
                {
                    CurrentlyPlaying = playing.Item;
                    TwitchManager.SendChannelMessage("🎵 Now Playing: " + TrackToString(CurrentlyPlaying) + " 🎵");
                }

                await Task.Delay(1000);
            }
        }
    }
}
