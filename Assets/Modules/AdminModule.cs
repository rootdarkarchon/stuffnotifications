﻿using Assets.Base;
using Assets.Models;
using Assets.Validators;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Assets.Modules
{
    internal class AdminModule : ChatModule
    {
        [Setting("NAME", "💻 Admin")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("get", ModeratorLevel = 10, Description = "Gets settings from the database")]
        [ValidateModerator(10)]
        [ValidateModule]
        [ValidateSetting]
        public void Command_GetSetting([C_UserSelf] TwitchDBUser userName, [C_Module] string modulename, [C_Setting(AllowEmpty = true)] string settingName = "")
        {
            IEnumerable<FieldInfo> settings = Assembly.GetExecutingAssembly().GetTypes()
                .SingleOrDefault(s => s.Name.ToLower() == modulename.ToLower())
                .GetFields().Where(f => f.GetCustomAttribute<SettingAttribute>() != null);

            if (string.IsNullOrEmpty(settingName))
            {
                var settingList = new List<string>();
                foreach (FieldInfo field in settings)
                {
                    string name = field.GetCustomAttribute<SettingAttribute>().SettingName;
                    string type = field.FieldType.Name.ToString();
                    settingList.Add($"{name} ({type})");
                }

                TwitchManager.SendChannelMessage(string.Join(", ", settingList));
            }
            else
            {
                string settingValue = SettingsManager.GetSettingSpecific<string>($"{modulename}.{settingName}");

                TwitchManager.SendChannelMessage($"{settingName} = {settingValue}");
            }
        }

        [Command("mod", ModeratorLevel = 10, Description = "Adds user to moderators")]
        [ValidateModerator(10)]
        [ValidateOtherUserExists]
        public void Command_Mod([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser userToMod)
        {
            userToMod.ModeratorLevel = 1;
            TwitchManager.SendChannelMessage($"{userToMod} is now a moderator of this bot");
        }

        [Command("modules", ModeratorLevel = 10, Description = "Displays all modules")]
        [ValidateModerator(10)]
        public void Command_Modules([C_UserSelf] TwitchDBUser userName)
        {
            TwitchManager.SendChannelMessage(string.Join(", ", MainManager.RegisteredModules.Select(m => m.GetType().Name.ToString())));
        }

        [Command("set", ModeratorLevel = 10, Description = "Changes a setting in the database")]
        [ValidateModerator(10)]
        [ValidateModule]
        [ValidateSetting]
        [ValidateSettingValue]
        public void Command_SetSetting([C_UserSelf] TwitchDBUser userName, [C_Module] string modulename, [C_Setting] string settingName, [C_SettingValue] string value)
        {
            SettingsManager.SetSettingSpecific($"{modulename}.{settingName}", value);
            TwitchManager.SendChannelMessage($"{userName} adjusted {settingName} to {value}");
        }

        [Command("unmod", ModeratorLevel = 10, Description = "Removes user from moderators")]
        [ValidateModerator(10)]
        [ValidateOtherUserExists]
        public void Command_UnMod([C_UserSelf] TwitchDBUser userName, [C_UserOther] TwitchDBUser userToMod)
        {
            userToMod.ModeratorLevel = 0;
            TwitchManager.SendChannelMessage($"{userToMod} is not moderator of this bot anymore");
        }
    }
}