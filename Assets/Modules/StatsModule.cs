﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Assets.Validators;
using System;
using System.Linq;
using System.Text;

namespace Assets.Modules
{
    public class StatsModule : ChatModule
    {
        [Setting("SESSION_CURRENCY_DOWN", "↘️")]
        public string S_CurrencyDown;

        [Setting("SESSION_CURRENCY_NOCHANGE", "➡️")]
        public string S_CurrencyNoChange;

        [Setting("SESSION_CURRENCY_UP", "↗️")]
        public string S_CurrencyUp;

        [Setting("NAME", "📈 Stats")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("leaderboard", Description = "Shows the current top 5 leaderboard in [Globals.CURRENCY_1_NAME]. Also displays current rank.")]
        public void Command_DisplayLeaderboard([C_UserSelf] TwitchDBUser userName)
        {
            var users = TwitchManager.AllUsers.OrderByDescending(c => c.TwitchDBUser?.Currency2).ToList();

            var output = new StringBuilder();

            output.Append("TOP 5: ");

            for (int i = 0; i < (5 < users.Count ? 5 : users.Count); i++)
            {
                output.Append($"#{i + 1} {users[i].DisplayName} ({users[i].TwitchDBUser.Currency2}) ⏸️ ");
            }

            output.Append($"Your rank is #{users.IndexOf(users.First(f => f.TwitchDBUser == userName)) + 1}/{users.Count} ({userName.Currency2.AsCur2()})");

            TwitchManager.SendChannelMessage(output.ToString());
        }

        [Command("points", Description = "Displays your points or the points of a different user.")]
        [ValidateOtherUserExists]
        public void Command_DisplayPoints([C_UserSelf] TwitchDBUser userName, [C_UserOther(allowEmpty: true)] TwitchDBUser otherUser = null)
        {
            TwitchDBUser actualUser = otherUser ?? userName;

            TwitchManager.SendChannelMessage($"{actualUser} has {actualUser.Currency1.AsCur1()} and {actualUser.Currency2.AsCur2()}");
        }

        [Command("rank", Description = "Shows your rank")]
        public void Command_DisplayRank([C_UserSelf] TwitchDBUser userName)
        {
            (long, long) ranks = Database.GetRank(userName.ToString());

            TwitchManager.SendChannelMessage($"Your rank is #{ranks.Item1}/{ranks.Item2}");
        }

        [Command("followage", Description = "Shows your follow age for this streamer")]
        public void Command_FollowAge([C_UserSelf] TwitchDBUser userName)
        {
            TimeSpan followAge = DateTime.Now - userName.TwitchUser.FollowTime;
            TwitchManager.SendChannelMessage($"Following since {(int)(followAge.Days / 365.2425)} years, {(int)(followAge.Days / 30.436875)} months, {followAge.Days - (int)(((int)(followAge.Days / 30.436875)) * 30.436875)} days, {followAge.Hours} hours, {followAge.Minutes} minutes and {followAge.Seconds} seconds.");
        }

        [Command("stats", Description = "Shows your various chat statistics")]
        public void Command_DisplayStats([C_UserSelf] TwitchDBUser user, [C_UserOther(allowEmpty: true)] TwitchDBUser otherUser = null)
        {
            TwitchDBUser actualUser = otherUser ?? user;

            (long, long, long, long) stats = Database.GetStatistics(actualUser.ToString());

            TwitchManager.SendChannelMessage($"{actualUser} attended {stats.Item1} streams, wrote {stats.Item2} words with {stats.Item3} characters and used {stats.Item4} commands.");
        }

        [Command("topchatters", Description = "Displays the top chatters of the current stream.")]
        public void Command_TopChatters([C_UserSelf] TwitchDBUser userName)
        {
            var chatters = Database.GetAllChatters(TwitchManager.ActiveStream);
            var ordered = chatters.Select(c => (c.Key, c.Value.SelectMany(k => k.Split(new[] { " " }, StringSplitOptions.None)).Count())).OrderByDescending(c => c.Item2).ToList();

            StringBuilder sb = new StringBuilder();
            sb.Append("Top Chatters of today: ");
            for (int i = 0; i < (ordered.Count() < 5 ? ordered.Count() : 5); i++)
            {
                sb.Append($"#{i + 1}: {ordered[i].Key} ({ordered[i].Item2} words) ⏸️ ");
            }

            sb.Append($"You wrote {ordered.Single(f => f.Key == userName.Username).Item2} words.");

            TwitchManager.SendChannelMessage(sb.ToString());
        }

        [Command("session", Description = "Displays how much currency you gained or lost during this stream.")]
        [ValidateOtherUserExists]
        public void Command_DisplaySession([C_UserSelf] TwitchDBUser user, [C_UserOther(allowEmpty: true)] TwitchDBUser otherUser = null)
        {
            var userName = otherUser ?? user;
            var sb = new StringBuilder();

            sb.Append(userName + ": ");
            if (userName.SessionCurrency1 < 0)
            {
                sb.Append(S_CurrencyDown);
            }
            else if (userName.SessionCurrency1 == 0)
            {
                sb.Append(S_CurrencyNoChange);
            }
            else
            {
                sb.Append(S_CurrencyUp);
            }

            sb.Append(" " + Math.Abs(userName.SessionCurrency1).AsCur1());

            sb.Append(" 󠀽");

            if (userName.SessionCurrency2 < 0)
            {
                sb.Append(S_CurrencyDown);
            }
            else if (userName.SessionCurrency2 == 0)
            {
                sb.Append(S_CurrencyNoChange);
            }
            else
            {
                sb.Append(S_CurrencyUp);
            }

            sb.Append(" " + Math.Abs(userName.SessionCurrency2).AsCur2());

            TwitchManager.SendChannelMessage(sb.ToString());
        }

        [Command("uptime", Description = "Displays the current uptime of the stream.")]
        public void Command_DisplayUptime()
        {
            if (TwitchManager.ActiveStream == null)
            {
                return;
            }

            TwitchManager.SendChannelMessage($"{TwitchManager.S_ClientUsername} is online for {TwitchManager.ActiveStream.Uptime.Hours} hours and {TwitchManager.ActiveStream.Uptime.Minutes} minutes.");
        }
    }
}