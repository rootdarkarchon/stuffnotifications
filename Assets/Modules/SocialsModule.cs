﻿using Assets.Base;
using Assets.Models;

namespace Assets.Modules
{
    public class SocialsModule : ChatModule
    {
        [Setting("NAME", "🔗 Socials")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("discord", Description = "Shows the link to my discord")]
        public void Discord([C_UserSelf] TwitchDBUser userName)
        {
            TwitchManager.SendChannelMessage($"{userName}, come and join the best Discord community! https://discord.gg/WTRQvRw rootdaAllEars");
        }

        [Command("twitter", Description = "Shows the link to my twitter")]
        public void Twitter([C_UserSelf] TwitchDBUser userName)
        {
            TwitchManager.SendChannelMessage($"{userName}, I occassionally post something on Twitter https://twitter.com/darkDoingStuff");
        }
    }
}