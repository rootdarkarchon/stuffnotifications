﻿using Assets.Base;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Modules
{
    public class CounterModule : ChatModule
    {
        public List<string> CounterCommands;

        public List<Counter> Counters;

        [Setting("NAME", "🔢 Counter")]
        public string S_Name;

        public override string ModuleName => S_Name;

        [Command("counters", Description = "Shows all available counters")]
        public void Command_Counters([C_UserSelf] TwitchDBUser userName)
        {
            var sb = new StringBuilder();

            sb.Append($"{userName} available counters: ");

            sb.Append(string.Join(", ", Counters
                .Select(c => Globals.S_CommandPrefix + c.Command)));

            TwitchManager.SendChannelMessage(sb.ToString());
        }

        [Command("[SpecificCounter]", ActualCommandListName = nameof(CounterCommands))]
        public void Command_PostCounterNew([C_UserSelf] TwitchDBUser userName, [C_Counter] string counterParam)
        {
            Counter counter = Counters.Single(c => c.Command.ToLower() == counterParam.ToLower());

            if (counter.IsOnGlobalCooldown())
            {
                Log("Counter " + counter.Name + " is on cooldown");
                return;
            }

            counter.CounterNumber += 1;

            TwitchManager.SendChannelMessage(counter.ReplacedMessage);

            Database.IncreaseCounter(counter);

            Task.Run(() => counter.SetCooldownForGlobal());
        }

        public override void InitializeModule()
        {
            try
            {
                Counters = Database.GetCounters();
                CounterCommands = new List<string>();
                CounterCommands.AddRange(Counters.Select(c => c.Command));
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        public void ReloadCounters()
        {
            Counters = Database.GetCounters();
            CounterCommands = new List<string>();
            CounterCommands.AddRange(Counters.Select(c => c.Command));
        }
    }
}