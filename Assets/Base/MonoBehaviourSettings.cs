﻿using Assets.Managers;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Base
{
    [Serializable]
    public abstract class MonoBehaviourSettings : MonoBehaviour
    {
        protected Globals Globals;
        protected GUIManager GUIManager;
        protected bool InitializedSettings;
        protected bool IsInitialized;
        protected SettingsManager SettingsManager;
        public virtual string ModuleName => GetType().Name;

        public void InitializeAllSettings()
        {
            // get all settings with reflection by name
            foreach (FieldInfo field in GetType().GetFields().Where(f => f.GetCustomAttribute<SettingAttribute>() != null))
            {
                // fieldtype since otherwise you get MonoField
                Type type = field.FieldType;
                string settingName = field.GetCustomAttribute<SettingAttribute>().SettingName;
                // instantiaze generic method
                MethodInfo method = typeof(SettingsManager).GetMethod(nameof(SettingsManager.GetSetting));
                MethodInfo genericMethod = method.MakeGenericMethod(type);
                // get result and set the field value
                object result = genericMethod.Invoke(SettingsManager, new object[] { settingName, this });
                field.SetValue(this, result);
            }
        }

        public void Log<T>(T message, bool warn = false)
        {
            if (warn)
            {
                Debug.LogWarning($"{ModuleName.ToUpper()} WARN".PadRight(30, ' ') + message);
            }
            else if (message is Exception)
            {
                Debug.LogError($"{ModuleName.ToUpper()} FAULT".PadRight(30, ' ') + message);
            }
            else
            {
                Debug.Log($"{ModuleName.ToUpper().PadRight(30, ' ')} {message}");
            }
        }

        public async void Start()
        {
            SettingsManager = GameObject.Find("ScriptHolder").GetComponent<SettingsManager>();
            Globals = GameObject.Find("ScriptHolder").GetComponent<Globals>();
            GUIManager = GameObject.Find("ScriptHolder").GetComponent<GUIManager>();

            await SettingsManager.WaitForInit();

            InitializeAllSettings();

            InitializedSettings = true;

            await StartWithSettings();

            IsInitialized = true;
            GUIManager.SetUpText(this.GetType().Name + " initialized");
        }

        public async virtual Task StartWithSettings()
        {
            // stub
        }

        public async void Update()
        {
            if (!InitializedSettings || !IsInitialized)
            {
                return;
            }

            await UpdateWithSettings();
        }

        public async virtual Task UpdateWithSettings()
        {
            // stub
        }

        public async Task WaitForInit()
        {
            while (IsInitialized == false)
            {
                await Task.Delay(100);
            }
        }
    }
}