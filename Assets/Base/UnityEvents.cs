﻿using Assets.Models;
using System;
using System.Collections.Generic;
using TwitchLib.Api.V5.Models.Subscriptions;
using TwitchLib.Client.Models;
using UnityEngine.Events;

namespace Assets.Base
{
    [Serializable]
    public class OnCommandMessageEvent : BaseTwitchEvent<TwitchUser, ChatMessage>
    {
        protected override void AddToListener(ChatModule chatModule)
        {
            AddListener(new UnityAction<TwitchUser, ChatMessage>(chatModule.ExecuteCommandEvent));
        }
    }

    [Serializable]
    public class OnFollowerEvent : BaseTwitchEvent<TwitchUser>
    {
        protected override void AddToListener(ChatModule chatModule)
        {
            AddListener(new UnityAction<TwitchUser>(chatModule.ExecuteFollowerEvent));
        }
    }

    [Serializable]
    public class OnBitsEvent : BaseTwitchEvent<BitEvent>
    {
        protected override void AddToListener(ChatModule chatModule)
        {
            AddListener(new UnityAction<BitEvent>(chatModule.ExecuteBitsEvent));
        }
    }


    [Serializable]
    public class OnChatMessageEvent : BaseTwitchEvent<TwitchUser, ChatMessage>
    {
        protected override void AddToListener(ChatModule chatModule)
        {
            AddListener(new UnityAction<TwitchUser, ChatMessage>(chatModule.ExecuteChatEvent));
        }
    }

    [Serializable]
    public class OnSubscriberEvent : BaseTwitchEvent<TwitchUser, Subscription>
    {
        protected override void AddToListener(ChatModule chatModule)
        {
            AddListener(new UnityAction<TwitchUser, Subscription>(chatModule.ExecuteSubscriberEvent));
        }
    }

    [Serializable]
    public abstract class BaseTwitchEvent<T, T2> : UnityEvent<T, T2>
    {
        [NonSerialized]
        protected List<ChatModule> chatModules = new List<ChatModule>();

        public virtual void AddChatModule(ChatModule chatModule)
        {
            if (!chatModules.Contains(chatModule))
            {
                chatModule.Log($"Listening to {GetType().Name}");
                chatModules.Add(chatModule);
                AddToListener(chatModule);
            }
        }

        protected abstract void AddToListener(ChatModule chatModule);
    }

    [Serializable]
    public abstract class BaseTwitchEvent<T> : UnityEvent<T>
    {
        [NonSerialized]
        protected List<ChatModule> chatModules = new List<ChatModule>();

        public virtual void AddChatModule(ChatModule chatModule)
        {
            if (!chatModules.Contains(chatModule))
            {
                chatModule.Log($"Listening to {GetType().Name}");
                chatModules.Add(chatModule);
                AddToListener(chatModule);
            }
        }

        protected abstract void AddToListener(ChatModule chatModule);
    }
}