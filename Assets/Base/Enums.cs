﻿namespace Assets.Base
{
    public enum StreamEvent
    {
        STREAMSTART,
        STREAMEND,
        JOIN,
        MESSAGE,
        COMMAND,
        LEAVE
    }

    public enum CurrencyType
    {
        Currency1,
        Currency2
    }

    public enum CameraState
    {
        Inactive,
        FadeOutFollower,
        RotateAroundBox,
        RotateAroundCoinBox
    }

    public enum DefenseDirection
    {
        North,
        East,
        South,
        West,
        N = North,
        E = East,
        S = South,
        W = West,
    }

    public enum DefenseDirectionNuance
    {
        Center,
        Left,
        Right
    }
}