﻿using Assets.Managers;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using TwitchLib.Api.V5.Models.Subscriptions;
using TwitchLib.Client.Models;
using UnityEngine;
using Asyncoroutine;

namespace Assets.Base
{
    [Serializable]
    public abstract class ChatModule : MonoBehaviourSettings
    {
        public DatabaseLayer Database;
        public MainManager MainManager { get; private set; }
        public SoundManager SoundManager { get; private set; }

        public TwitchManager TwitchManager;

        public ValidationManager ValidationManager { get; private set; }

        public static List<TwitchDBUser> UsersBlockedFromTransactions = new List<TwitchDBUser>();

        [MethodImpl(MethodImplOptions.NoInlining)]
        public string CommandWithPrefix()
        {
            var st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            string methodCommand = sf.GetMethod().GetCustomAttribute<CommandAttribute>()?.Command;

            return methodCommand == null ? "" : Globals.S_CommandPrefix + methodCommand;
        }

        public void ErrorLog<T>(T message) => UnityEngine.Debug.LogError($"Module {ModuleName.ToUpper()}: {message}");

        public virtual void ExecuteChatEvent(TwitchUser user, ChatMessage chatMessage)
        {
            // stub
        }

        public virtual void ExecuteCommandEvent(TwitchUser user, ChatMessage chatMessage)
        {
            StartCoroutine(Task.Run(() =>
            {
                (string command, string[] splitMsg) = SplitChatMessage(chatMessage);

                MethodInfo method = GetType().GetMethods()
                              .SingleOrDefault(m => m.GetCustomAttribute<CommandAttribute>()?.Command == command.ToLower());
                if (method == null)
                {
                    method = GetType().GetMethods()
                        .Where(m => !string.IsNullOrEmpty(m.GetCustomAttribute<CommandAttribute>()?.ActualCommandListName))
                        .SingleOrDefault(m => ((List<string>)GetType().GetField(m.GetCustomAttribute<CommandAttribute>().ActualCommandListName).GetValue(this))
                        .Select(l => l.ToLower()).Contains(command.ToLower()));
                }

                if (method == null)
                {
                    return;
                }

                ValidateAndCallMethod(user, splitMsg, method);
            }).AsCoroutine());
        }

        private void ValidateAndCallMethod(TwitchUser user, string[] splitMsg, MethodInfo method)
        {
            if (method != null)
            {
                object[] parameters = ValidationManager.ParseAndValidate(
                    user.TwitchDBUser,
                    splitMsg.ToArray(), this, method);

                if (parameters != null)
                {
                    method.Invoke(this, parameters);
                    return;
                }
            }
        }

        public string GetCommandUsage(MethodInfo method)
        {
            ParameterInfo[] parameters = method.GetParameters();

            var usages = new List<string>();

            foreach (ParameterInfo parameter in parameters)
            {
                Attribute attr = parameter.GetCustomAttributes().Where(c => c.GetType() != typeof(C_UserSelfAttribute) && c.GetType().IsSubclassOf(typeof(C_BaseAttribute))).SingleOrDefault();
                if (attr == null)
                {
                    continue;
                }

                string text = ((C_BaseAttribute)attr).Description;

                if (parameter.HasDefaultValue)
                {
                    if (!string.IsNullOrEmpty(parameter.DefaultValue.ToString()))
                    {
                        usages.Add($"[{text}={parameter.DefaultValue}]");
                    }
                    else
                    {
                        usages.Add($"[{text}]");
                    }
                }
                else
                {
                    usages.Add(text);
                }
            }

            string prefix = Globals.S_CommandPrefix;
            string command = method.GetCustomAttribute<CommandAttribute>().Command;

            return prefix + command + " " + string.Join(" ", usages);
        }

        public virtual void ExecuteFollowerEvent(TwitchUser newFollower)
        {
            // stub
        }

        public virtual void ExecuteSubscriberEvent(TwitchUser user, Subscription subscription)
        {
            // stub
        }

        public virtual void ExecuteBitsEvent(BitEvent bitevent)
        {
            // stub
        }

        public virtual void InitializeModule()
        {
            // stub
        }

        public override async Task StartWithSettings()
        {
            try
            {
                MainManager = GameObject.Find("ScriptHolder").GetComponent<MainManager>();
                TwitchManager = MainManager.TwitchManager;
                Database = MainManager.Database;
                SoundManager = MainManager.SoundManager;
                ValidationManager = MainManager.ValidationManager;

                MainManager.RegisterModule(this);

                await SettingsManager.WaitForInit();

                await TwitchManager.WaitForInit();

                InitializeModule();

                Log("Initialized");

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        protected (string, string[]) SplitChatMessage(ChatMessage chatMessage)
        {
            string[] splitMsg = chatMessage.Message.Split(new[] { " " }, StringSplitOptions.None);
            splitMsg[0] = splitMsg[0].Replace(Globals.S_CommandPrefix, "");
            string command = splitMsg[0];
            for (int i = 0; i < splitMsg.Length; i++)
            {
                splitMsg[i] = splitMsg[i].Replace("@", "");
            }

            var actualSplit = new List<string>();

            bool foundMulti = false;
            string tempString = "";
            foreach (string split in splitMsg)
            {
                if (split.StartsWith("\""))
                {
                    foundMulti = true;
                }
                if (foundMulti)
                {
                    tempString += " " + split;
                }
                if (!foundMulti)
                {
                    actualSplit.Add(split);
                }
                if (foundMulti && split.EndsWith("\""))
                {
                    foundMulti = false;
                    actualSplit.Add(tempString.Trim().Substring(1, tempString.Trim().Length - 2));
                    tempString = "";
                }
            }

            return (command, actualSplit.ToArray());
        }
    }
}