﻿using Assets.Models;
using System;

namespace Assets.Base
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CommandAttribute : Attribute
    {
        public string Command;
        public long Cost;
        public string Description;
        public CurrencyType Currency = CurrencyType.Currency1;
        public string ActualCommandListName;
        public int ModeratorLevel;
        public bool DisplayInHelp = true;

        public CommandAttribute(string command)
        {
            Command = command;
            Cost = 0;
            Description = "";
            ModeratorLevel = 0;
            Currency = CurrencyType.Currency1;
            ActualCommandListName = "";
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class SettingAttribute : Attribute
    {
        public string DefaultValue;
        public string SettingName;

        public SettingAttribute(string settingName) => SettingName = settingName;

        public SettingAttribute(string settingName, string defaultValue = "") : this(settingName) => DefaultValue = defaultValue;

        public SettingAttribute(string settingName, int defaultValue = 0) : this(settingName, defaultValue.ToString())
        {
        }

        public SettingAttribute(string settingName, double defaultValue = 0) : this(settingName, defaultValue.ToString())
        {
        }

        public SettingAttribute(string settingName, float defaultValue = 0) : this(settingName, defaultValue.ToString())
        {
        }

        public SettingAttribute(string settingName, bool defaultValue = false) : this(settingName, defaultValue.ToString())
        {
        }
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public abstract class C_BaseAttribute : Attribute
    {
        public abstract string Description { get; }
        public abstract Type AllowedType { get; }

        public object Value { get; set; }

        public string StringValue => Value.ToString();

        public long LongValue => (long)Value;

        public TwitchDBUser TwitchDBUserValue => (TwitchDBUser)Value;
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_UserSelfAttribute : C_BaseAttribute
    {
        public override string Description => "";

        public override Type AllowedType => typeof(TwitchDBUser);
    }


    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_DirectionAttribute : C_BaseAttribute
    {
        public override string Description => "direction";

        public override Type AllowedType => typeof(DefenseDirection);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_FreeTextAttribute : C_BaseAttribute
    {
        public override string Description => "free text input";

        public override Type AllowedType => typeof(string[]);
    }


    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_CounterAttribute : C_BaseAttribute
    {
        public override string Description => "countername";

        public override Type AllowedType => typeof(string);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_SoundEffectAttribute : C_BaseAttribute
    {
        public override string Description => "soundeffect";

        public override Type AllowedType => typeof(string);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_CommandAttribute : C_BaseAttribute
    {
        public override string Description => "command";

        public override Type AllowedType => typeof(string);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_SettingAttribute : C_BaseAttribute
    {
        public bool AllowEmpty = false;

        public override string Description => "setting";

        public override Type AllowedType => typeof(string);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_ModuleAttribute : C_BaseAttribute
    {
        public override string Description => "modulename";

        public override Type AllowedType => typeof(string);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_SettingValueAttribute : C_BaseAttribute
    {
        public override string Description => "settingValue";

        public override Type AllowedType => typeof(string);

        public bool FullyQualified = true;
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_AmountAttribute : C_BaseAttribute
    {
        public CurrencyType CurrencyType;
        public bool AllowAll;
        public bool AllowEmpty = false;

        public C_AmountAttribute(CurrencyType currencyType = CurrencyType.Currency1, bool allowAll = true)
        {
            CurrencyType = currencyType;
            AllowAll = allowAll;
        }

        public override string Description => "amount" + (AllowAll ? "/all/max" : "");

        public override Type AllowedType => typeof(long);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_TimeAttribute : C_BaseAttribute
    {
        public override string Description => "timeInSeconds";

        public override Type AllowedType => typeof(long);
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class C_UserOtherAttribute : C_BaseAttribute
    {
        public bool AllowOtherUserToBeSelf;
        public bool AllowOtherUserToBeEmpty;

        public C_UserOtherAttribute(bool allowToBeSelf = false, bool allowEmpty = false)
        {
            AllowOtherUserToBeEmpty = allowEmpty;
            AllowOtherUserToBeSelf = allowToBeSelf;
        }

        public override string Description => "user";

        public override Type AllowedType => typeof(TwitchDBUser);
    }
}