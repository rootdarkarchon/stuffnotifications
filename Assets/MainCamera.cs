﻿using Assets.Base;
using Assets.Helpers;
using Assets.Managers;
using Assets.Models;
using Asyncoroutine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class MainCamera : MonoBehaviourSettings
{
    public AudioClip Bits10000DonationClip;
    public AudioClip Bits1000DonationClip;
    public AudioClip Bits100DonationClip;
    public AudioClip Bits1DonationClip;
    public AudioClip Bits5000DonationClip;
    public AudioClip BitsDonationClip;
    public BoxCreatorHelper BoxCreatorHelper;
    public CoinGenerator CoinGenerator;
    public MainManager MainManager;
    public SoundManager SoundManager;
    public TableCreatorHelper TableCreatorHelper;
    private readonly Vector3 cameraPosBits = new Vector3(10f, 10f, 100f);
    private readonly Vector3 cameraPosOut = new Vector3(-1.267f, 3.86f, 10f);
    private bool animationRunning = false;
    private GameObject bananabox;
    private float cameraMovementSpeed = 0.05f;
    private Vector3 CameraPosOverBox;
    private CameraState cameraState;
    private bool followerAnimationDone = true;
    private GameObject plinko;
    private float rotations = 0.0f;
    private GameObject rublebox;
    private Vector3 velocity = Vector3.zero;
    private bool isWelcoming = false;

    public async override Task StartWithSettings()
    {
        //GuiInitText = "Starting up";

        cameraState = CameraState.Inactive;

        PositionPlinko();
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    public async override Task UpdateWithSettings()
    {
        await new WaitForMainThread();
        if (GameObject.Find("DefendingCamera").GetComponent<Camera>().enabled) return;

        if (MainManager.FollowerQueue.Any() && !animationRunning)
        {
            StartCoroutine(Animate_SpawnFollowerCoin());
        }

        if (MainManager.WelcomeMessageQueue.Any() && !isWelcoming)
        {
            StartCoroutine(Animate_PostWelcomeMessage());
        }

        if (MainManager.BitsQueue.Any() && !animationRunning)
        {
            StartCoroutine(Animate_BitsEvent());
        }

        MoveCamera();
    }

    private IEnumerator Animate_BitsEvent()
    {
        animationRunning = true;

        transform.position = cameraPosBits;
        CameraPosOverBox = plinko.transform.position + Vector3.up * 1.5f;
        transform.LookAt(CameraPosOverBox);

        GameObject coinHolder = new GameObject("CoinHolder");

        foreach (var bitevent in new List<BitEvent>(MainManager.BitsQueue))
        {
            List<GameObject> coins = new List<GameObject>();

            GameObject coin = CoinGenerator.CreateBitsCoin(bitevent);

            Destroy(coin.GetComponent<MeshCollider>());
            coin.AddComponent<BitCoinCollideHelper>();
            coin.GetComponent<BitCoinCollideHelper>().BoxY = bananabox.transform.position.y;
            coin.GetComponent<BitCoinCollideHelper>().BananaBox = bananabox;
            coin.GetComponent<BitCoinCollideHelper>().RubleBox = rublebox;
            coin.GetComponent<BitCoinCollideHelper>().enabled = false;
            coin.AddComponent<AudioSource>();
            coin.transform.localScale = new Vector3(1, 1, 1);
            var sizeX = coin.GetComponent<MeshRenderer>().bounds.size.x;

            coin.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

            GameObject coll = new GameObject("mesh");
            coll.AddComponent<MeshCollider>();
            var meshcoll = coll.GetComponent<MeshCollider>();
            meshcoll.convex = true;
            meshcoll.transform.SetParent(coll.transform);
            meshcoll.sharedMesh = coin.GetComponent<MeshFilter>().mesh;
            meshcoll.material = Resources.Load<PhysicMaterial>("Material/CoinPhysMat");
            meshcoll.enabled = false;
            meshcoll.cookingOptions = MeshColliderCookingOptions.CookForFasterSimulation | MeshColliderCookingOptions.EnableMeshCleaning | MeshColliderCookingOptions.WeldColocatedVertices;
            meshcoll.transform.Rotate(-90, 0, 0, Space.Self);
            coll.transform.localScale = new Vector3(1, 1, 1);
            coll.layer = 10;
            coll.transform.SetParent(coin.transform);

            coin.transform.position = cameraPosBits + (Vector3.down * 20);

            PlayBitsEventClip(bitevent);

            yield return new WaitForSecondsRealtime(1);

            // make shit visible
            Globals.SetMeshRendererVisibilityForGameObject(plinko, true);
            Globals.SetMeshRendererVisibilityForGameObject(rublebox, true);
            Globals.SetMeshRendererVisibilityForGameObject(bananabox, true);
            rublebox.GetComponentInChildren<RectTransform>().GetComponent<Canvas>().enabled = true;
            bananabox.GetComponentInChildren<RectTransform>().GetComponent<Canvas>().enabled = true;
            plinko.GetComponent<PlinkoHelper>().EnableCanvas(true);

            InitializeBitDonatorCoins(bitevent, out GameObject bitDonator, out GameObject bitDonatorClone);

            // enable camera rotation
            cameraState = CameraState.RotateAroundCoinBox;

            GUIManager.SetLowText($"{bitevent.User.DisplayName} just cheered!");

            System.Random r = new System.Random(DateTime.Now.Millisecond);

            plinko.GetComponent<PlinkoHelper>().TotalBits = (int)bitevent.Amount;

            for (int i = 0; i < bitevent.Amount; i++)
            {
                var lastcoin = Instantiate(coin);

                lastcoin.transform.SetParent(coinHolder.transform);
                lastcoin.name = "coin " + i;
                lastcoin.GetComponent<BitCoinCollideHelper>().enabled = true;
                StartCoroutine(lastcoin.GetComponent<BitCoinCollideHelper>().EnableCollision());

                lastcoin.transform.position = new Vector3((plinko.transform.position + Vector3.up * 15).x
                    + 0.04f
                    ,
                    plinko.transform.position.y + 15,
                    plinko.transform.position.z - 6 * sizeX + ((i % 13) * sizeX));

                lastcoin.GetComponent<Rigidbody>().AddTorque(new Vector3((float)r.NextDouble() * 500 + 15, (float)r.NextDouble() * 500 + 15, (float)r.NextDouble() * 500 + 15), ForceMode.Acceleration);

                coins.Add(lastcoin);
                if (i % 5 == 0)
                {
                    yield return new WaitForSeconds(0.05f);
                }

                plinko.GetComponent<PlinkoHelper>().CurrentBits = i;
                plinko.GetComponent<PlinkoHelper>().WriteText($"{i} bits");
            }

            plinko.GetComponent<PlinkoHelper>().WriteText($"{bitevent.Amount} bits");
            StartCoroutine(WaitForBitsTimeout());
            yield return new WaitUntil(() => bitsAnimDone || coins.All(c => c == null || c.GetComponent<BitCoinCollideHelper>().Collided || c.GetComponent<Rigidbody>() == null));

            var countRubles = rublebox.GetComponent<BoxCounter>().Count * 10;
            var countBanana = bananabox.GetComponent<BoxCounter>().Count;

            bitevent.User.TwitchDBUser.Currency1 += countRubles;
            bitevent.User.TwitchDBUser.Currency2 += countBanana;

            MainManager.TwitchManager.SendChannelMessage($"{bitevent.User.DisplayName} thank you for your donation! You have been awarded {countRubles.AsCur1()} and {countBanana.AsCur2()}! rootdaToot");

            StartCoroutine(SoundManager.FadeOutAudio());
            yield return new WaitForSecondsRealtime(5);

            MainManager.BitsQueue.Remove(bitevent);
            Destroy(coin);
            Destroy(coinHolder);
            Destroy(bitDonator);
            Destroy(bitDonatorClone);

            GUIManager.ClearLowText();
            plinko.GetComponent<PlinkoHelper>().WriteText("");

            bananabox.GetComponent<BoxCounter>().Count = 0;
            rublebox.GetComponent<BoxCounter>().Count = 0;
            bananabox.GetComponent<BoxCounter>().WriteText("");
            rublebox.GetComponent<BoxCounter>().WriteText("");

            foreach (var c in bananabox.GetComponentsInChildren<MeshCollider>().Where(c => c.gameObject != bananabox))
            {
                Destroy(c.gameObject);
            }

            foreach (var c in rublebox.GetComponentsInChildren<MeshCollider>().Where(c => c.gameObject != rublebox))
            {
                Destroy(c.gameObject);
            }

            cameraState = CameraState.Inactive;
        }

        Globals.SetMeshRendererVisibilityForGameObject(bananabox, false);
        Globals.SetMeshRendererVisibilityForGameObject(rublebox, false);
        rublebox.GetComponentInChildren<RectTransform>().GetComponent<Canvas>().enabled = false;
        bananabox.GetComponentInChildren<RectTransform>().GetComponent<Canvas>().enabled = false;
        plinko.GetComponent<PlinkoHelper>().EnableCanvas(false);
        Globals.SetMeshRendererVisibilityForGameObject(plinko, false);

        animationRunning = false;

        cameraMovementSpeed = 0.05f;
    }

    private bool bitsAnimDone = false;

    private IEnumerator WaitForBitsTimeout()
    {
        bitsAnimDone = false;
        yield return new WaitForSeconds(10);
        bitsAnimDone = true;
    }

    private IEnumerator Animate_PostWelcomeMessage()
    {
        isWelcoming = true;
        while (MainManager.WelcomeMessageQueue.Count > 0)
        {
            WelcomeMessage welcome = MainManager.WelcomeMessageQueue.FirstOrDefault();

            GameObject cloneCoin = null;
            string soundPath = Path.Combine(Globals.S_SoundDataFolder, welcome.User.DisplayName + ".mp3");
            if (File.Exists(soundPath) && welcome.User.IsSub)
            {
                AudioClip craftClip = Globals.CreateSoundClipFromFile(soundPath);
                welcome.AudioClip = craftClip;
            }

            try
            {
                SoundManager.PlaySoundEffect(new SoundEffect() { AudioClip = welcome.AudioClip, Volume = 0.7f });

                cloneCoin = CoinGenerator.CreateCoinWithTexture(welcome.User);

                GUIManager.SetWelcomeBackText(welcome.WelcomeMessageGuiText);

                GUIManager.AddGameObjectToWelcomeBack(cloneCoin);

                Globals.SetMeshRendererVisibilityForGameObject(cloneCoin, true);
                Rigidbody body = cloneCoin.GetComponent<Rigidbody>();
                body.angularDrag = 0;
                body.drag = 0;
                body.useGravity = false;
                body.angularVelocity = body.transform.up * 2;
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }

            yield return new WaitForSecondsRealtime(5);

            Destroy(cloneCoin);
            GUIManager.ClearWelcomeBackText();
            GUIManager.RemoveGameObjectFromWelcomeBack();
            MainManager.WelcomeMessageQueue.Remove(welcome);
        }
        isWelcoming = false;
    }

    private IEnumerator Animate_SpawnFollowerCoin()
    {
        Debug.Log("Spawn coin");

        // initialize spawn
        animationRunning = true;
        BoxCreatorHelper.SetVisibilityForBoxObjects(true);
        TableCreatorHelper.SetVisibilityForTableObjects(false);

        transform.position = new Vector3(BoxCreatorHelper.GetParentTransform.position.x,
            BoxCreatorHelper.GetParentTransform.position.y + 1.3f,
            BoxCreatorHelper.GetParentTransform.transform.position.z + 0.5f);
        transform.LookAt(BoxCreatorHelper.GetParentTransform);
        cameraState = CameraState.RotateAroundBox;

        // play sound
        AudioClip intro = Resources.Load<AudioClip>("sounds/openIntro");
        AudioClip followerSound = Resources.Load<AudioClip>("sounds/openChest");

        SoundManager.EnqueueSoundEffect(new SoundEffect() { AudioClip = intro, Volume = 0.7f });

        BoxCreatorHelper.GetParentTransform.gameObject.GetComponentInChildren<Text>().text = MainManager.FollowerQueue.FirstOrDefault().DisplayName + " just followed!\r\nWelcome to the box of STUFF!";

        // wait for camera to move + 0.5s
        yield return new WaitUntil(() => cameraState == CameraState.Inactive);
        yield return new WaitForSeconds(0.5f);

        while (MainManager.FollowerQueue.Count > 0)
        {
            BoxCreatorHelper.GetParentTransform.gameObject.GetComponentInChildren<Text>().text = MainManager.FollowerQueue.FirstOrDefault().DisplayName + " just followed!\r\nWelcome to the box of STUFF!";

            TwitchUser follower = MainManager.FollowerQueue.FirstOrDefault();

            SoundManager.PlaySoundEffect(new SoundEffect() { AudioClip = followerSound, Volume = 0.7f });

            // create new textured pill and position it
            GameObject newFollower = CoinGenerator.CreateCoinWithTexture(follower);
            newFollower.transform.position = transform.position + transform.forward * 0.5f;
            newFollower.transform.LookAt(transform.position, Vector3.up);
            newFollower.transform.Rotate(new Vector3(0, 0, 0));
            Rigidbody body = newFollower.GetComponent<Rigidbody>();
            body.angularVelocity = body.transform.up * 2f;
            newFollower.AddComponent<AudioSource>();
            newFollower.AddComponent<UserCoinCollideHelper>();
            UserCoinCollideHelper userpillSound = newFollower.GetComponent<UserCoinCollideHelper>();

            // play welcome animation

            StartCoroutine(PlayFlickingAnimation(follower, newFollower.GetComponent<Rigidbody>(), userpillSound));

            yield return new WaitUntil(() => followerAnimationDone == true);

            MainManager.FollowerQueue.Remove(follower);
        }

        // move camera back
        cameraState = CameraState.FadeOutFollower;

        // wait for camera to move
        yield return new WaitUntil(() => cameraState == CameraState.Inactive);

        BoxCreatorHelper.SetVisibilityForBoxObjects(false);
        animationRunning = false;
    }

    private void InitializeBitDonatorCoins(BitEvent bitevent, out GameObject bitDonator, out GameObject bitDonatorClone)
    {
        // create new textured pill and position it
        bitDonator = CoinGenerator.CreateCoinWithTexture(bitevent.User);
        bitDonator.transform.position = plinko.transform.position + Vector3.back * 8;
        bitDonator.transform.localScale = new Vector3(250, 250, 20);
        bitDonator.transform.position = bitDonator.transform.position + Vector3.up * 4;
        bitDonator.transform.LookAt(plinko.transform.position, Vector3.up);
        bitDonator.transform.Rotate(new Vector3(0, 0, 0));
        bitDonator.transform.GetComponent<MeshCollider>().enabled = false;
        bitDonator.layer = 8;
        Rigidbody body = bitDonator.GetComponent<Rigidbody>();
        body.angularVelocity = body.transform.up * 2f;
        body.useGravity = false;
        body.angularDrag = 0;
        Globals.SetMeshRendererVisibilityForGameObject(bitDonator, true);

        // clone
        bitDonatorClone = Instantiate(bitDonator);
        bitDonatorClone.transform.position = plinko.transform.position + Vector3.forward * 8 + Vector3.up * 4;
        bitDonatorClone.transform.LookAt(plinko.transform.position, Vector3.up);
        bitDonator.transform.Rotate(new Vector3(0, 0, 0));
        bitDonatorClone.transform.GetComponent<MeshCollider>().enabled = false;
        Rigidbody body2 = bitDonatorClone.GetComponent<Rigidbody>();
        bitDonatorClone.layer = 8;
        body2.angularVelocity = body2.transform.up * 2f;
        body2.angularDrag = 0;
        body2.useGravity = false;
    }

    private void MoveCamera()
    {
        switch (cameraState)
        {
            case CameraState.Inactive:
                return;

            case CameraState.FadeOutFollower:
                // move box out of image
                float moveSpeed = 5f;
                transform.position = Vector3.SmoothDamp(transform.position, cameraPosOut, ref velocity, 0.1f, moveSpeed);
                cameraState = (transform.position == cameraPosOut) ? CameraState.Inactive : CameraState.FadeOutFollower;
                break;

            case CameraState.RotateAroundBox:
                // rotate around box
                transform.LookAt(BoxCreatorHelper.GetParentTransform);
                cameraMovementSpeed += Time.deltaTime * cameraMovementSpeed;
                transform.position += -transform.forward * System.Math.Min(0.5f, cameraMovementSpeed) * Time.deltaTime;

                float rotSpeed = 50f * Time.deltaTime;

                transform.RotateAround(BoxCreatorHelper.GetParentTransform.position + new Vector3(0, 1, 0), Vector3.up, rotSpeed);
                rotations += rotSpeed;
                if (rotations > 360)
                {
                    cameraState = CameraState.Inactive;
                    rotations = 0;
                    cameraMovementSpeed = 0.05f;
                }
                break;
            case CameraState.RotateAroundCoinBox:
                transform.LookAt(CameraPosOverBox);
                float rotSpeed2 = 25f * Time.deltaTime;
                transform.RotateAround(CameraPosOverBox + Vector3.up * 25, Vector3.up, rotSpeed2);
                break;
        }
    }

    private void PlayBitsEventClip(BitEvent bitevent)
    {
        AudioClip bitsDonationClip = Bits1DonationClip;
        if (bitevent.Amount >= 100)
        {
            bitsDonationClip = Bits100DonationClip;
        }
        if (bitevent.Amount >= 1000)
        {
            bitsDonationClip = Bits1000DonationClip;
        }
        if (bitevent.Amount >= 5000)
        {
            bitsDonationClip = Bits5000DonationClip;
        }
        if (bitevent.Amount >= 10000)
        {
            bitsDonationClip = Bits10000DonationClip;
        }

        SoundManager.EnqueueSoundEffect(new SoundEffect() { Name = "BitEvent", AudioClip = bitsDonationClip, Volume = 0.5f });
    }
    /// <summary>
    /// Animation of the flicking in of the coins
    /// </summary>
    /// <param name="username"></param>
    /// <param name="body"></param>
    /// <param name="collideHelper"></param>
    /// <returns></returns>
    private IEnumerator PlayFlickingAnimation(TwitchUser follower, Rigidbody body, UserCoinCollideHelper collideHelper)
    {
        // set text and disable gravity
        followerAnimationDone = false;
        body.useGravity = false;

        // wait 3 seconds for the rotation
        yield return new WaitForSecondsRealtime(3);

        // enable gravity and flick it in
        body.useGravity = true;
        body.velocity = new Vector3(UnityEngine.Random.Range(-1.5f, 1.5f), 0f, UnityEngine.Random.Range(-1.5f, -3.5f));
        body.angularVelocity = new Vector3(50f, 0f, UnityEngine.Random.Range(15f, 50f));
        body.maxAngularVelocity = 50f;

        // wait for animation and reset text
        yield return new WaitUntil(() => collideHelper.Collided);
        yield return new WaitForSeconds(1.5f);

        followerAnimationDone = true;
        Destroy(collideHelper);
    }

    private void PositionPlinko()
    {
        plinko = GameObject.Find("board");
        plinko.transform.position = cameraPosBits + (Vector3.forward * 20) + (Vector3.down * 17);

        GameObject reflectionObj = new GameObject("Reflection Probe");
        reflectionObj.AddComponent<ReflectionProbe>();
        var reflectionProbe = reflectionObj.GetComponent<ReflectionProbe>();
        reflectionProbe.customBakedTexture = Resources.Load<Texture>("Textures/Cube/horizontal-cross_1");
        reflectionProbe.mode = UnityEngine.Rendering.ReflectionProbeMode.Custom;
        reflectionProbe.size = new Vector3(30, 25, 15);
        reflectionObj.transform.position = plinko.transform.position + Vector3.up * 5;
        plinko.transform.localScale = new Vector3(550, 400, 400);

        bananabox = GameObject.Find("box_bananas");
        bananabox.transform.position = plinko.transform.position + Vector3.down * 5;
        bananabox.transform.position = bananabox.transform.position + Vector3.right * 7f;
        bananabox.transform.localScale = new Vector3(400, 400, 400);
        var bcanvas = bananabox.GetComponentInChildren<RectTransform>();
        bcanvas.position = bananabox.transform.position + Vector3.up * 3.5f;
        rublebox = GameObject.Find("box_rubles");
        rublebox.transform.position = plinko.transform.position + Vector3.down * 5;
        rublebox.transform.position = rublebox.transform.position + Vector3.left * 7f;
        rublebox.transform.localScale = new Vector3(400, 400, 400);
        var rcanvas = rublebox.GetComponentInChildren<RectTransform>();
        rcanvas.position = rublebox.transform.position + Vector3.up * 3.5f;

        plinko.GetComponent<PlinkoHelper>().FrontCanvas.transform.position = plinko.transform.position + -Vector3.back * 7 + Vector3.up * 8;
        plinko.GetComponent<PlinkoHelper>().BackCanvas.transform.position = plinko.transform.position + Vector3.back * 7 + Vector3.up * 8;
        plinko.GetComponent<PlinkoHelper>().EnableCanvas(false);

        Globals.SetMeshRendererVisibilityForGameObject(plinko, false);
        Globals.SetMeshRendererVisibilityForGameObject(rublebox, false);
        Globals.SetMeshRendererVisibilityForGameObject(bananabox, false);
    }
}