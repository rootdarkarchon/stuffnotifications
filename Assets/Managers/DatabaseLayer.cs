﻿using Assets.Base;
using Assets.Models;
using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Managers
{
    public class DatabaseLayer : MonoBehaviour
    {
        public bool IsInitialized;
        public TwitchManager TwitchManager;
        private static object dbLock;
        private IDbConnection dbConnection;

        public void AdjustSessionCurrency(string username, long currency, CurrencyType currencyType, long activeStreamId)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                IDbDataParameter streamIdParam = dbCmd.CreateParameter();
                streamIdParam.Value = activeStreamId;
                streamIdParam.ParameterName = "@streamid";

                IDbDataParameter currencyParam = dbCmd.CreateParameter();
                currencyParam.Value = currency;
                currencyParam.ParameterName = "@currency";

                dbCmd.Parameters.Add(usernameParam);
                dbCmd.Parameters.Add(currencyParam);
                dbCmd.Parameters.Add(streamIdParam);

                dbCmd.CommandText = $"INSERT INTO UserSession (name, stream_id, currency_1, currency_2) VALUES (@name, @streamid, 0, 0) ON CONFLICT(name) DO UPDATE SET stream_id=@streamid, {GetCurrencyDbName(currencyType)}=@currency";
                dbCmd.ExecuteNonQuery();
            });
        }

        // todo: remove consecutive and put it into a user,streamid PK table
        public void AdjustUserInterest(string username, double interest)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                IDbDataParameter interestParam = dbCmd.CreateParameter();
                interestParam.Value = interest;
                interestParam.ParameterName = "@interest";

                dbCmd.Parameters.Add(interestParam);
                dbCmd.Parameters.Add(usernameParam);

                string sqlCmd = "INSERT INTO UserInterest (name, interest) VALUES (@name, @interest) ON CONFLICT(name) DO UPDATE SET interest = @interest";

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public void GenerateSettings(IDbCommand command)
        {
            try
            {
                IDbDataParameter settingNameParam = command.CreateParameter();
                settingNameParam.ParameterName = "@key";
                IDbDataParameter settingValueParam = command.CreateParameter();
                settingValueParam.ParameterName = "@value";

                command.Parameters.Add(settingNameParam);
                command.Parameters.Add(settingValueParam);

                foreach (Type type in Assembly.GetExecutingAssembly().GetTypes().Where(f => f.GetFields().Any(m => m.GetCustomAttribute<SettingAttribute>() != null)))
                {
                    foreach (FieldInfo field in type.GetFields().Where(f => f.GetCustomAttribute<SettingAttribute>() != null))
                    {
                        if (type.IsAbstract)
                        {
                            continue;
                        }

                        SettingAttribute setting = field.GetCustomAttribute<SettingAttribute>();
                        string settingName = setting.SettingName;
                        settingNameParam.Value = type.Name + "." + settingName;

                        command.CommandText = "SELECT key from Settings WHERE Key = @key";

                        bool exists = true;
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (!reader.Read())
                            {
                                exists = false;
                            }
                        }

                        if (exists)
                        {
                            continue;
                        }

                        settingValueParam.Value = field.GetCustomAttribute<SettingAttribute>().DefaultValue;

                        command.CommandText = "INSERT INTO Settings (key, value) VALUES(@key, @value)";
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }

        public Dictionary<string, List<string>> GetAllChatters(TwitchStream stream)
        {
            return ExecuteLocked((dbCmd) =>
            {
                Dictionary<string, List<string>> output = new Dictionary<string, List<string>>();

                string sqlCheck = $"SELECT username, message FROM History WHERE event = '{StreamEvent.MESSAGE.ToString()}' AND streamid = @streamid ORDER BY username, date";

                IDbDataParameter streamIDParam = dbCmd.CreateParameter();
                streamIDParam.Value = stream.Id;
                streamIDParam.DbType = DbType.Int64;
                streamIDParam.ParameterName = "@streamid";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(streamIDParam);

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    string chatter = "";
                    List<string> chatLines = new List<string>();
                    while (reader.Read())
                    {
                        string newChatter = reader[0].ToString();
                        if (chatter == "") chatter = newChatter;
                        string msg = reader[1].ToString();

                        if (newChatter != chatter)
                        {
                            output.Add(chatter, chatLines);
                            chatLines = new List<string>();
                            chatter = newChatter;
                        }
                        else
                        {
                            chatLines.Add(msg);
                        }
                    }

                    output.Add(chatter, chatLines);

                    reader.Close();
                }

                return output;
            });
        }

        public List<Setting> GetAllSettings()
        {
            return ExecuteLocked((dbCmd) =>
            {
                var settings = new List<Setting>();

                string sqlCheck = "SELECT * FROM Settings";

                dbCmd.CommandText = sqlCheck;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    Setting setting = null;
                    while (reader.Read())
                    {
                        setting = new Setting()
                        {
                            RawSettingKey = reader["key"].ToString(),
                            RawValue = reader["value"].ToString()
                        };

                        settings.Add(setting);
                    }

                    reader.Close();
                }

                return settings;
            });
        }

        public List<Counter> GetCounters()
        {
            return ExecuteLocked((dbCmd) =>
            {
                var returnList = new List<Counter>();

                string sqlGet = "SELECT * FROM Counters";
                dbCmd.CommandText = sqlGet;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        long.TryParse(reader["counter"].ToString(), out long counterNumber);
                        long.TryParse(reader["global_cooldown"].ToString(), out long globalCooldown);

                        var counter = new Counter
                        {
                            Command = (string)reader["command"],
                            Message = (string)reader["message"],
                            GlobalCooldown = globalCooldown,
                            Name = (string)reader["name"],
                            CounterNumber = counterNumber
                        };

                        returnList.Add(counter);
                    }
                }

                return returnList;
            });
        }

        public DateTime? GetHistoryLastNonLeaveEvent(TwitchStream activeStream, string username)
        {
            return ExecuteLocked((dbCmd) =>
            {
                DateTime? returnValue = null;

                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@username";

                IDbDataParameter streamIDParam = dbCmd.CreateParameter();
                streamIDParam.Value = activeStream.Id;
                streamIDParam.DbType = DbType.Int64;
                streamIDParam.ParameterName = "@streamid";

                IDbDataParameter eventParam = dbCmd.CreateParameter();
                eventParam.Value = StreamEvent.LEAVE.ToString();
                eventParam.ParameterName = "@event";

                string sqlCmd = "SELECT date FROM History WHERE streamid = @streamid AND username LIKE @username AND event != @event ORDER BY date DESC LIMIT 1";

                dbCmd.Parameters.Add(streamIDParam);
                dbCmd.Parameters.Add(usernameParam);
                dbCmd.Parameters.Add(eventParam);

                dbCmd.CommandText = sqlCmd;

                using (var reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        returnValue = DateTime.Parse(reader["date"].ToString());
                    }

                    reader.Close();
                }

                return returnValue;
            });
        }

        public DateTime? GetHistoryLastUserEvent(TwitchStream activeStream, string username, StreamEvent streamEvent)
        {
            return ExecuteLocked((dbCmd) =>
            {
                DateTime? returnValue = null;

                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@username";

                IDbDataParameter streamIDParam = dbCmd.CreateParameter();
                streamIDParam.Value = activeStream.Id;
                streamIDParam.DbType = DbType.Int64;
                streamIDParam.ParameterName = "@streamid";

                IDbDataParameter eventParam = dbCmd.CreateParameter();
                eventParam.Value = streamEvent.ToString();
                eventParam.ParameterName = "@event";

                string sqlCmd = "SELECT date FROM History WHERE streamid = @streamid AND username LIKE @username AND event = @event ORDER BY date DESC LIMIT 1";

                dbCmd.Parameters.Add(streamIDParam);
                dbCmd.Parameters.Add(usernameParam);
                dbCmd.Parameters.Add(eventParam);

                dbCmd.CommandText = sqlCmd;
                using (var reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        returnValue = DateTime.Parse(reader["date"].ToString());
                    }

                    reader.Close();
                }

                return returnValue;
            });
        }

        public long GetModeratorLevel(string username)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = "SELECT level FROM Moderators WHERE name LIKE @name";
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(usernameParam);

                long moderatorlevel = 0;
                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        moderatorlevel = (long)reader["level"];
                    }

                    reader.Close();
                }

                return moderatorlevel;
            });
        }

        public (long,long,long,long) GetStatistics(string username)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = @"SELECT COUNT(distinct streamid) as NumOfStreams, SUM(
                      CASE WHEN length(message) >= 1
                      THEN
                        (length(message) - length(replace(message, ' ', ''))) + 1
                      ELSE
                        (length(message) - length(replace(message, ' ', '')))
                      END) as NumOfWords,
                      SUM(LENGTH(replace(message, ' ',''))) as NumOfChars
                      FROM History WHERE username LIKE @name AND event LIKE 'message'";
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(usernameParam);

                long numOfStreams = 0;
                long numOfWords = 0;
                long numOfChars = 0;
                long numOfCommands = 0;
                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        numOfStreams = (long)reader["NumOfStreams"];
                        numOfWords = (long)reader["NumOfWords"];
                        numOfChars = (long)reader["NumOfChars"];
                    }

                    reader.Close();
                }

                sqlCheck = @"SELECT COUNT(*) as NumOfCommands FROM History WHERE username LIKE @name AND event LIKE 'command'";
                dbCmd.CommandText = sqlCheck;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        numOfCommands = (long)reader["NumOfCommands"];
                    }

                    reader.Close();
                }

                return (numOfStreams, numOfWords, numOfChars, numOfCommands);
            });
        }

        public (long, long) GetRank(string username)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = "SELECT name, currency_2 from UserCurrency ORDER BY currency_2 DESC";

                dbCmd.CommandText = sqlCheck;

                long totalRanks = 1;
                long rank = 0;
                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader[0].ToString() == username)
                        {
                            rank = totalRanks;
                        }
                        totalRanks++;
                    }

                    reader.Close();
                }

                return (rank, totalRanks);
            });
        }

        public long GetSessionCurrency(string username, CurrencyType currencyType, long activeStreamId)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = $"SELECT {GetCurrencyDbName(currencyType)} FROM UserSession WHERE name LIKE @name AND stream_id = @streamid";

                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                IDbDataParameter streamIdParam = dbCmd.CreateParameter();
                streamIdParam.Value = activeStreamId;
                streamIdParam.ParameterName = "@streamid";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(usernameParam);
                dbCmd.Parameters.Add(streamIdParam);

                long current1 = 0;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        current1 = (long)reader[0];
                    }
                    reader.Close();
                }

                return current1;
            });
        }

        public List<SoundEffect> GetSoundEffects()
        {
            return ExecuteLocked((dbCmd) =>
            {
                var returnList = new List<SoundEffect>();

                string sqlGet = "SELECT * FROM SoundEffects";
                dbCmd.CommandText = sqlGet;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        long.TryParse(reader["cost_currency_1"].ToString(), out long costCurrency1);
                        long.TryParse(reader["cost_currency_2"].ToString(), out long costCurrency2);
                        long.TryParse(reader["global_cooldown"].ToString(), out long globalCooldown);
                        long.TryParse(reader["user_cooldown"].ToString(), out long userCooldown);

                        float.TryParse(reader["volume"].ToString(),
                            System.Globalization.NumberStyles.Any,
                            System.Globalization.CultureInfo.InvariantCulture,
                            out float volume);

                        var newEffect = new SoundEffect
                        {
                            Command = (string)reader["command"],
                            CostCurrency1 = costCurrency1,
                            CostCurrency2 = costCurrency2,
                            FileName = (string)reader["file_name"],
                            Message = (string)reader["message"],
                            GlobalCooldownTime = globalCooldown,
                            UserCooldownTime = userCooldown,
                            Name = (string)reader["name"],
                            UserName = (string)reader["user_name"],
                            Volume = volume
                        };

                        returnList.Add(newEffect);
                    }
                }

                return returnList;
            });
        }

        public long GetUserCurrency(string username, CurrencyType currencyType)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string currency = currencyType == CurrencyType.Currency1 ? "currency_1" : "currency_2";

                string sqlCheck = $"SELECT {currency} FROM UserCurrency WHERE name LIKE @name";

                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(usernameParam);

                long current1 = -1;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        current1 = (long)reader[0];
                    }

                    reader.Close();
                }

                return current1;
            });
        }

        public double GetUserInterest(string username)
        {
            return ExecuteLocked((dbCmd) =>
             {
                 string sqlCheck = "SELECT interest FROM UserInterest WHERE name LIKE @name";
                 IDbDataParameter usernameParam = dbCmd.CreateParameter();
                 usernameParam.Value = username;
                 usernameParam.ParameterName = "@name";

                 dbCmd.CommandText = sqlCheck;
                 dbCmd.Parameters.Add(usernameParam);

                 double interest = 0.5;

                 using (IDataReader reader = dbCmd.ExecuteReader())
                 {
                     if (reader.Read())
                     {
                         interest = (double)reader[0];
                     }

                     reader.Close();
                 }

                 return interest;
             });
        }

        public long GetUserLastSeenStreamId(string username)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = $"SELECT streamid FROM History WHERE username LIKE @name AND (event = '{StreamEvent.COMMAND.ToString()}' OR event ='{StreamEvent.MESSAGE.ToString()}') ORDER BY date DESC LIMIT 1";
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(usernameParam);

                long lastId = -1;

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        lastId = (long)reader[0];
                    }

                    reader.Close();
                }

                return lastId;
            });
        }

        public List<string> GetUsersFromStream(TwitchStream stream)
        {
            return ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = $"SELECT DISTINCT username FROM History WHERE (event = '{StreamEvent.COMMAND.ToString()}' OR event = '{StreamEvent.MESSAGE.ToString()}') AND streamid = @streamid";

                IDbDataParameter streamIDParam = dbCmd.CreateParameter();
                streamIDParam.Value = stream.Id;
                streamIDParam.DbType = DbType.Int64;
                streamIDParam.ParameterName = "@streamid";

                dbCmd.Parameters.Add(streamIDParam);
                dbCmd.CommandText = sqlCheck;

                var usersInStream = new List<string>();

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        usersInStream.Add(reader[0].ToString());
                    }

                    reader.Close();
                }

                return usersInStream;
            });
        }

        public void IncreaseCounter(Counter counter)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter nameParam = dbCmd.CreateParameter();
                nameParam.Value = counter.Name;
                nameParam.ParameterName = "@name";

                string sqlCmd = $"UPDATE Counters SET counter = counter + 1 WHERE name LIKE @name";

                dbCmd.Parameters.Add(nameParam);

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public TwitchStream InsertTwitchStream(string title, string started)
        {
            return ExecuteLocked((dbCmd) =>
            {
                long lastId = 0;

                IDbDataParameter startedParameter = dbCmd.CreateParameter();
                startedParameter.DbType = DbType.String;
                startedParameter.Size = 500;
                startedParameter.Value = started;
                startedParameter.ParameterName = "@started";

                string sqlCheck = "SELECT id from TwitchStreams WHERE started = @started";
                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(startedParameter);
                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    if (!reader.Read())
                    {
                        reader.Close();
                        string sqlExec = $"INSERT INTO TwitchStreams (streamtitle, started) VALUES (@title, @started)";
                        IDbDataParameter titleParameter = dbCmd.CreateParameter();
                        titleParameter.DbType = DbType.String;
                        titleParameter.Size = 500;
                        titleParameter.Value = title;
                        titleParameter.ParameterName = "@title";

                        dbCmd.Parameters.Add(titleParameter);
                        dbCmd.Parameters.Add(startedParameter);

                        dbCmd.CommandText = sqlExec;

                        dbCmd.ExecuteNonQuery();

                        string sqlReturn = @"select last_insert_rowid()";
                        dbCmd.CommandText = sqlReturn;
                        lastId = (long)dbCmd.ExecuteScalar();
                    }
                    else
                    {
                        lastId = (long)reader[0];
                        reader.Close();
                    }
                }

                var stream = new TwitchStream()
                {
                    Id = lastId,
                    StreamStartDate = started,
                    StreamEndDate = "",
                    StreamTitle = title
                };

                return stream;
            });
        }

        public void LogEvent(TwitchStream activeStream, StreamEvent streamEvent, TwitchUser user, string message)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = user?.DisplayName ?? "";
                usernameParam.ParameterName = "@username";

                IDbDataParameter dateParam = dbCmd.CreateParameter();
                dateParam.Value = DateTime.Now.ToString("o");
                dateParam.ParameterName = "@date";

                IDbDataParameter streamIDParam = dbCmd.CreateParameter();
                streamIDParam.Value = activeStream.Id;
                streamIDParam.DbType = DbType.Int64;
                streamIDParam.ParameterName = "@streamid";

                IDbDataParameter eventParam = dbCmd.CreateParameter();
                eventParam.Value = streamEvent.ToString();
                eventParam.ParameterName = "@event";

                IDbDataParameter messageParam = dbCmd.CreateParameter();
                messageParam.Value = message;
                messageParam.ParameterName = "@message";

                string sqlCmd = "INSERT INTO History (date, streamid, event, username, message) VALUES (@date, @streamid, @event, @username, @message)";

                dbCmd.Parameters.Add(streamIDParam);
                dbCmd.Parameters.Add(usernameParam);
                dbCmd.Parameters.Add(dateParam);
                dbCmd.Parameters.Add(eventParam);
                dbCmd.Parameters.Add(messageParam);

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public void ResetSessionCurrencies(TwitchStream stream)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter streamIdParam = dbCmd.CreateParameter();
                streamIdParam.Value = stream.Id;
                streamIdParam.ParameterName = "@streamid";

                dbCmd.Parameters.Add(streamIdParam);

                dbCmd.CommandText = $"UPDATE UserSession SET currency_1 = 0, currency_2 = 0 WHERE stream_id != @streamid";
                dbCmd.ExecuteNonQuery();
            });
        }

        public void SetEndDateTwitchStream(TwitchStream stream)
        {
            ExecuteLocked((dbCmd) =>
            {
                string sqlExec = $"UPDATE TwitchStreams SET ended = @ended WHERE id = {stream.Id}";

                IDbDataParameter endedParam = dbCmd.CreateParameter();
                endedParam.Value = stream.StreamEndDate;
                endedParam.ParameterName = "@ended";

                dbCmd.Parameters.Add(endedParam);
                dbCmd.CommandText = sqlExec;

                dbCmd.ExecuteNonQuery();
            });
        }

        public void SetUserCurrency(string username, long currency, CurrencyType currencyType)
        {
            ExecuteLocked((dbCmd) =>
            {
                string usedCurrency = currencyType == CurrencyType.Currency1 ? "currency_1" : "currency_2";

                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                IDbDataParameter currencyParam = dbCmd.CreateParameter();
                currencyParam.Value = currency;
                currencyParam.ParameterName = "@currency";

                string values = currencyType == CurrencyType.Currency1 ? "(@name, @currency, 0)" : "(@name, 0, @currency)";

                string sqlCmd = $"INSERT INTO UserCurrency (name, currency_1, currency_2) VALUES {values} ON CONFLICT(name) DO UPDATE SET {usedCurrency} = @currency";

                dbCmd.Parameters.Add(currencyParam);
                dbCmd.Parameters.Add(usernameParam);

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public void SetUserModeratorLevel(string username, long modlevel)
        {
            ExecuteLocked((dbCmd) =>
            {
                IDbDataParameter usernameParam = dbCmd.CreateParameter();
                usernameParam.Value = username;
                usernameParam.ParameterName = "@name";

                IDbDataParameter modlevelParam = dbCmd.CreateParameter();
                modlevelParam.Value = modlevel;
                modlevelParam.ParameterName = "@modlevel";

                string sqlCmd = $"INSERT INTO Moderators (name, level) VALUES (@name, @modlevel) ON CONFLICT(name) DO UPDATE SET level = @modlevel";

                dbCmd.Parameters.Add(modlevelParam);
                dbCmd.Parameters.Add(usernameParam);

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public void Start()
        {
            ExecuteLocked((dbcmd) =>
            {
                string createTwitchStreamsTable =
                  "CREATE TABLE IF NOT EXISTS TwitchStreams (id INTEGER PRIMARY KEY AUTOINCREMENT, streamtitle TEXT, started TEXT, ended TEXT)";

                dbcmd.CommandText = createTwitchStreamsTable;
                dbcmd.ExecuteNonQuery();

                string createUserCurrencyTable =
                  "CREATE TABLE IF NOT EXISTS UserCurrency (name TEXT PRIMARY KEY, currency_1 INTEGER, currency_2 INTEGER)";

                dbcmd.CommandText = createUserCurrencyTable;
                dbcmd.ExecuteNonQuery();

                string createUserSessionTable =
                    "CREATE TABLE IF NOT EXISTS UserSession (name TEXT PRIMARY KEY, stream_id INTEGER, currency_1 INTEGER, currency_2 INTEGER)";

                dbcmd.CommandText = createUserSessionTable;
                dbcmd.ExecuteNonQuery();

                string createUserInterestTable =
                    "CREATE TABLE IF NOT EXISTS UserInterest (name TEXT PRIMARY KEY, interest FLOAT, consecutive INTEGER)";

                dbcmd.CommandText = createUserInterestTable;
                dbcmd.ExecuteNonQuery();

                string createSoundEffectsTable =
                    "CREATE TABLE IF NOT EXISTS SoundEffects (name TEXT PRIMARY KEY, command TEXT, user_name TEXT, file_name TEXT, message TEXT, cost_currency_1 INTEGER, cost_currency_2 INTEGER, global_cooldown INTEGER, user_cooldown INTEGER, volume REAL)";

                dbcmd.CommandText = createSoundEffectsTable;
                dbcmd.ExecuteNonQuery();

                string createCountersTable =
                    "CREATE TABLE IF NOT EXISTS Counters (name TEXT PRIMARY KEY, command TEXT, counter INTEGER, message TEXT, global_cooldown INTEGER)";

                dbcmd.CommandText = createCountersTable;
                dbcmd.ExecuteNonQuery();

                string createModeratorsTable =
                    "CREATE TABLE IF NOT EXISTS Moderators (name TEXT PRIMARY KEY, level INTEGER)";

                dbcmd.CommandText = createModeratorsTable;
                dbcmd.ExecuteNonQuery();

                string createSettingsTable =
                    "CREATE TABLE IF NOT EXISTS Settings (key TEXT PRIMARY KEY, value TEXT)";

                dbcmd.CommandText = createSettingsTable;
                dbcmd.ExecuteNonQuery();

                string createHistoryTable =
                    "CREATE TABLE IF NOT EXISTS History (date TEXT, streamid INTEGER, event TEXT, username TEXT, message TEXT)";

                dbcmd.CommandText = createHistoryTable;
                dbcmd.ExecuteNonQuery();

                GenerateSettings(dbcmd);
            });

            IsInitialized = true;
        }

        private float backupTime;

        public void Update()
        {
            if (dbConnection == null)
            {
                CreateDbConection();
            }

            backupTime += Time.deltaTime;

            if (backupTime > 5 * 60)
            {
                if (dbLock == null) dbLock = new object();
                lock (dbLock)
                {
                    backupTime = 0;
                    System.IO.File.Copy("TwitchDB.sqlite", "TwitchDB.sqlite.bak", true);
                }
            }
        }

        public void UpdateSetting(Setting setting)
        {
            ExecuteLocked((dbCmd) =>
            {
                string sqlCheck = "SELECT key FROM Settings WHERE key LIKE @key";
                IDbDataParameter keyNameParam = dbCmd.CreateParameter();
                keyNameParam.Value = setting.RawSettingKey;
                keyNameParam.ParameterName = "@key";

                dbCmd.CommandText = sqlCheck;
                dbCmd.Parameters.Add(keyNameParam);

                IDbDataParameter valueParam = dbCmd.CreateParameter();
                valueParam.Value = setting.RawValue;
                valueParam.ParameterName = "@value";
                string sqlCmd = "UPDATE Settings SET value = @value WHERE key LIKE @key";

                using (IDataReader reader = dbCmd.ExecuteReader())
                {
                    sqlCmd = reader.Read()
                        ? $"UPDATE Settings SET value = @value WHERE key LIKE @key"
                        : $"INSERT INTO Settings (key, value) VALUES (@key, @value)";

                    reader.Close();
                }

                dbCmd.Parameters.Add(valueParam);

                dbCmd.CommandText = sqlCmd;
                dbCmd.ExecuteNonQuery();
            });
        }

        public async Task WaitForInit()
        {
            while (!IsInitialized)
            {
                await Task.Delay(100);
            }
        }

        private void CreateDbConection()
        {
            string connection = "URI=file:TwitchDB.sqlite";
            dbConnection = new SqliteConnection(connection);
        }

        private void ExecuteLocked(Action<IDbCommand> act)
        {
            if (dbConnection == null)
            {
                CreateDbConection();
            }

            if (dbLock == null)
            {
                dbLock = new object();
            }

            lock (dbLock)
            {
                dbConnection.Open();
                using (var dbCommand = dbConnection.CreateCommand())
                {
                    act.Invoke(dbCommand);
                }
                dbConnection.Close();
            }
        }

        private T ExecuteLocked<T>(Func<IDbCommand, T> func)
        {
            if (dbConnection == null)
            {
                CreateDbConection();
            }

            T result;
            if (dbLock == null)
            {
                dbLock = new object();
            }

            lock (dbLock)
            {
                dbConnection.Open();
                using (var dbCommand = dbConnection.CreateCommand())
                {
                    result = func.Invoke(dbCommand);
                }
                dbConnection.Close();
            }
            return result;
        }

        private string GetCurrencyDbName(CurrencyType currencyType)
        {
            return currencyType == CurrencyType.Currency1 ? "currency_1" : "currency_2";
        }
    }
}