﻿using Assets.Base;
using Assets.Models;
using Asyncoroutine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TwitchLib.Api;
using TwitchLib.Api.V5.Models.Channels;
using TwitchLib.Api.V5.Models.Subscriptions;
using TwitchLib.Api.V5.Models.Users;
using TwitchLib.Client.Models;
using TwitchLib.PubSub.Enums;
using TwitchLib.PubSub.Events;
using TwitchLib.Unity;
using UnityEngine;

namespace Assets.Managers
{
    [Serializable]
    public class TwitchManager : MonoBehaviourSettings
    {
        public TwitchStream ActiveStream;
        public List<TwitchUser> AllUsers;

        public DatabaseLayer Database;

        public List<TwitchUser> Followers;

        public MainManager MainManager;

        public string OnlineSince;

        [Setting("BOT_OAUTH", "")]
        public string S_BotOauth;

        [Setting("BOT_USERNAME", "")]
        public string S_BotUsername;

        [Setting("CLIENT_OAUTH", "")]
        public string S_ClientOauth;

        [Setting("CLIENT_USERNAME", "")]
        public string S_ClientUsername;

        public List<TwitchUser> Subscribers;
        private Task backgroundCheckTask;
        private Task backgroundOnlineUsersTask;
        private bool debugCall;
        private PubSub pubSub;
        private bool pubSubConnected;
        private TwitchAPI twitchAPI;
        private Client twitchClient;
        private string twitchID;

        public List<TwitchUser> OnlineUsersAccordingToTwitch;

        [NonSerialized]
        public TwitchLib.Api.V5.Models.Bits.Cheermotes CheerEmotes;
        public bool FinishedDownloadingFollowers;

        public TwitchManager()
        {
        }

        public List<TwitchDBUser> GetActiveChatters()
        {
            return AllUsers.Where(u => u.IsOnline).Select(c => c.TwitchDBUser).ToList();
        }

        public async Task<TwitchLib.Api.V5.Models.Streams.StreamByUser> GetStreamInfoAsync() => await twitchAPI.V5.Streams.GetStreamByUserAsync(await GetTwitchId());

        public async Task<bool> IsBroadcasterOnlineAsync() => await twitchAPI.V5.Streams.BroadcasterOnlineAsync(await GetTwitchId());

        public override async Task StartWithSettings()
        {
            Followers = new List<TwitchUser>();
            Subscribers = new List<TwitchUser>();
            AllUsers = new List<TwitchUser>();
            OnlineUsersAccordingToTwitch = new List<TwitchUser>();

            KeepTwitchApiAlive();

            while (pubSubConnected == false)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(100));
            }

            _ = Task.Run(() => DownloadEmotes());
            _ = Task.Run(() => DownloadAndPopulateFollowersData());

            backgroundCheckTask = Task.Run(Background_WatchStreamStatus);
            backgroundOnlineUsersTask = Task.Run(Background_WatchOnlineChatters);
        }

        public async Task DownloadEmotes()
        {
            while (CheerEmotes == null)
            {
                try
                {
                    var chatterUsers = Task.Run(async () => await twitchAPI.V5.Bits.GetCheermotesAsync());
                    CheerEmotes = chatterUsers.Result;
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }
        }

        public async override Task UpdateWithSettings()
        {
            if (backgroundCheckTask == null)
            {
                Log("Background Check Task was null, restarting");
                backgroundCheckTask = Task.Run(Background_WatchStreamStatus);
            }

            if (backgroundOnlineUsersTask == null)
            {
                Log("Online User Check Task was null, restarting");
                backgroundOnlineUsersTask = Task.Run(Background_WatchOnlineChatters);
            }

            KeepTwitchApiAlive();
        }

        internal async Task<List<ChannelFollow>> GetAllFollows() => (await twitchAPI.V5.Channels.GetAllFollowersAsync(await GetTwitchId())).ToList();

        internal async Task<List<Subscription>> GetAllSubscriptions() => await twitchAPI.V5.Channels.GetAllSubscribersAsync(await GetTwitchId());

        internal async Task<User> GetUserByName(string username) => (await twitchAPI.V5.Users.GetUserByNameAsync(username)).Matches.First();

        internal void ParseTestPubSubMessage(string msg)
        {
            debugCall = true;
            try
            {
                pubSub.TestMessageParser(msg);
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        internal void SendChannelMessage(string messageToSend)
        {
            if (twitchClient.JoinedChannels.Count == 0)
            {
                twitchClient.JoinChannel(S_ClientUsername, true);
            }

            twitchClient.SendMessage(S_ClientUsername, messageToSend);
        }

        internal void SendPrivateMessage(string username, string messageToSend)
        {
            if (twitchClient.JoinedChannels.Count == 0)
            {
                twitchClient.JoinChannel(S_ClientUsername, true);
            }

            twitchClient.SendWhisper(username, messageToSend);
        }

        private async Task Background_WatchStreamStatus()
        {
            // todo: refactor this out? in stats module maybe?
            while (true)
            {
                while (!FinishedDownloadingFollowers)
                {
                    await Task.Delay(250);
                }
                try
                {
                    bool broadcasterOnline = await IsBroadcasterOnlineAsync();

                    if (!broadcasterOnline)
                    {
                        if (ActiveStream.Id != 0)
                        {
                            ActiveStream.StreamEndDate = DateTime.Now.ToString("o");
                            Database.SetEndDateTwitchStream(ActiveStream);
                            Log("Stream went offline", true);
                            List<string> usersInStream = Database.GetUsersFromStream(ActiveStream);
                            Database.LogEvent(ActiveStream, StreamEvent.STREAMEND, null, "");
                            ActiveStream = null;
                            Globals.WritePeopleFromLastStream(usersInStream);
                        }

                        MainManager.DebugUIText = "Stream offline";
                    }
                    else
                    {
                        if (ActiveStream.Id == 0)
                        {
                            try
                            {
                                TwitchLib.Api.V5.Models.Streams.StreamByUser stream = await GetStreamInfoAsync();

                                ActiveStream = Database.InsertTwitchStream(stream.Stream.Channel.Status, stream.Stream.CreatedAt.ToString("o"));

                                SendChannelMessage($"{stream.Stream.Channel.DisplayName} is now online! On the menu today is {stream.Stream.Channel.Game}! {stream.Stream.Channel.Status}");

                                foreach (var user in AllUsers)
                                {
                                    user.TwitchDBUser.UpdateActiveStream(ActiveStream);
                                }

                                Database.ResetSessionCurrencies(ActiveStream);

                                Log("Stream is online; ID is now " + ActiveStream.Id, true);

                                Database.LogEvent(ActiveStream, StreamEvent.STREAMSTART, null, stream.Stream.Channel.Status);
                            }
                            catch (Exception ex)
                            {
                                Log(ex);
                            }
                        }
                    }

                    await Task.Delay(TimeSpan.FromSeconds(30));
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }
        }

        private async Task Background_WatchOnlineChatters()
        {
            // todo: refactor this out? in stats module maybe?
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(10));
                try
                {
                    if (ActiveStream != null)
                    {
                        List<TwitchUser> result = (await twitchAPI.Undocumented.GetChattersAsync(S_ClientUsername))
                            .Select(f => AllUsers.SingleOrDefault(u => u.DisplayName.ToLower() == f.Username.ToLower()))
                            .Where(c => c != null && c?.TwitchDBUser.Currency1 > 0).ToList();

                        foreach (var user in result)
                        {
                            if (!OnlineUsersAccordingToTwitch.Any(r => r.DisplayName == user.DisplayName))
                            {
                                Log($"{user.DisplayName} joined");
                                OnlineUsersAccordingToTwitch.Add(user);
                                user.TwitchDBUser.LastNonLeaveEvent = DateTime.Now;
                                Database.LogEvent(ActiveStream, StreamEvent.JOIN, user, "");
                            }
                        }

                        foreach (var user in new List<TwitchUser>(OnlineUsersAccordingToTwitch))
                        {
                            if (!result.Any(r => r.DisplayName == user.DisplayName))
                            {
                                Log($"{user.DisplayName} left");
                                OnlineUsersAccordingToTwitch.RemoveAll(r => r.DisplayName == user.DisplayName);
                                user.TwitchDBUser.LastLeaveEvent = DateTime.Now;
                                Database.LogEvent(ActiveStream, StreamEvent.LEAVE, user, "");
                            }
                        }
                    }

                    await Task.Delay(TimeSpan.FromSeconds(30));
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }
        }

        private async Task DownloadAndPopulateFollowersData()
        {
            await new WaitForMainThread();

            MainManager.DebugUIText = "Getting followers off twitch";

            // get all followers and subs
            List<ChannelFollow> allFollowers = await GetAllFollows();
            var allSubscriptions = new List<Subscription>();
            try
            {
                allSubscriptions = await GetAllSubscriptions();
                allSubscriptions.RemoveAll(s => s.User.DisplayName.ToLower() == "darkdoingstuff");
            }
            catch (Exception ex)
            {
                Log(ex, true);
            }

            MainManager.DebugUIText = "Init and download data";

            /*while (ActiveStream.Id == 0)
            {
                await Task.Delay(1000);
            }*/

            using (var client = new WebClient())
            {
                //  build followers + subs list
                foreach (User follower in allFollowers.Select(f => f.User)
                    .Union(allSubscriptions.Select(s => s.User))
                    .GroupBy(c => c.DisplayName)
                    .Select(c => c.First()))
                {
                    try
                    {
                        MainManager.DebugUIText = "Getting " + follower.DisplayName;

                        var newFollower = new TwitchUser(follower, allFollowers.Single(f => f.User == follower).CreatedAt,
                            allSubscriptions.FirstOrDefault(s => s.User.DisplayName == follower.DisplayName),
                            true);
                        newFollower.TwitchDBUser = CreateTwitchDBUser(newFollower);

                        await Globals.GenerateTextureData(newFollower);

                        Followers.Add(newFollower);
                        if (newFollower.IsSub)
                        {
                            var subFollower = new TwitchUser(follower, allFollowers.SingleOrDefault(f => f.User == follower).CreatedAt,
                                 allSubscriptions.FirstOrDefault(s => s.User.DisplayName == follower.DisplayName),
                                 true);
                            subFollower.TwitchDBUser = CreateTwitchDBUser(subFollower);
                            Subscribers.Add(subFollower);
                        }

                        AllUsers.Add(newFollower);
                    }
                    catch
                    {
                        Log("Could not add " + follower.DisplayName, true);
                    }
                }
            }

            MainManager.DebugUIText = "";

            FinishedDownloadingFollowers = true;
        }

        private async Task<string> GetTwitchId()
        {
            if (string.IsNullOrEmpty(twitchID))
            {
                twitchID = (await GetUserByName(S_ClientUsername)).Id;
            }

            return twitchID;
        }

        private void KeepTwitchApiAlive()
        {
            if (twitchAPI != null && twitchClient != null && pubSub != null)
            {
                return;
            }

            Log("Getting settings");

            Log("Got all values");

            // init TwitchAPI
            twitchAPI = new TwitchAPI();
            twitchAPI.Settings.AccessToken = S_ClientOauth;

            // init TwitchClient
            twitchClient = new Client();
            twitchClient.OnLog += TwitchClient_OnLog;

            twitchClient.Initialize(new ConnectionCredentials(S_BotUsername, S_BotOauth, disableUsernameCheck: true), S_ClientUsername);

            twitchClient.OnConnected += TwitchClient_OnConnected;
            twitchClient.OnJoinedChannel += TwitchClient_OnJoinedChannel;
            twitchClient.OnLeftChannel += TwitchClient_OnLeftChannel;
            twitchClient.OnMessageReceived += TwitchClient_OnMessageReceived;
            twitchClient.OnWhisperSent += TwitchClient_OnWhisperSent;

            twitchClient.Connect();

            // init PubSub
            pubSub = new PubSub();
            pubSub.OnLog += PubSub_OnLog;
            pubSub.OnFollow += PubSub_OnFollow;
            pubSub.OnChannelSubscription += PubSub_OnChannelSubscription;
            pubSub.OnPubSubServiceConnected += PubSub_OnPubSubServiceConnected;
            pubSub.OnPubSubServiceError += PubSub_OnPubSubServiceError;
            pubSub.OnBitsReceived += PubSub_OnBitsReceived;
            pubSub.Connect();
        }

        private void PubSub_OnLog(object sender, OnLogArgs e)
        {
            Log(e.Data);
        }

        private async void PubSub_OnBitsReceived(object sender, OnBitsReceivedArgs e)
        {
            Log("On bits " + e.Username + ":" + e.BitsUsed);
            var user = await GetUserByName(e.Username);
            while (CheerEmotes == null)
            {
                try
                {
                    CheerEmotes = await twitchAPI.V5.Bits.GetCheermotesAsync();
                }
                catch (Exception ex)
                {
                    Log(ex);
                }
            }

            Log("Got all emotes");

            var emote = e.ChatMessage.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                .First(f => CheerEmotes.Actions.Select(a => a.Prefix).Any(a => f.ToLower().StartsWith(a.ToLower())));
            TwitchLib.Api.V5.Models.Bits.Action usedCheer = CheerEmotes.Actions.Single(a => emote.StartsWith(a.Prefix));

            var tuser = AllUsers.SingleOrDefault(u => user.DisplayName == u.DisplayName);
            var bitEvent = new BitEvent { Amount = e.BitsUsed, Emote = usedCheer, User = tuser };

            bitEvent.Image = await Globals.LoadEmoteImage(bitEvent);

            Log("Added to bits queue");
            MainManager.BitsQueue.Add(bitEvent);
        }

        private async void PubSub_OnChannelSubscription(object sender, OnChannelSubscriptionArgs e)
        {
            Log("On sub " + e.Subscription.DisplayName);
            /*User user = await GetUserByName(e.Subscription.DisplayName);
            List<Subscription> subs = await GetAllSubscriptions();

            var newSubscriber = new TwitchUser(user,
                subs.SingleOrDefault(s => s.User.DisplayName == user.DisplayName),
                Followers.Any(f => f.DisplayName == user.DisplayName),
                CreateTwitchDBUser(user.DisplayName));

            MainManager.OnSubscriberEvent.Invoke(newSubscriber, subs.SingleOrDefault(f => f.User.DisplayName == user.DisplayName));

            // todo: existing subscriber
            // todo: sub with follow
            // todo: sub without follow

            Subscribers.Add(newSubscriber);

            string subPlan = "1000";

            switch (e.Subscription.SubscriptionPlan)
            {
                case SubscriptionPlan.Prime:
                case SubscriptionPlan.Tier1:
                    subPlan = "1000";
                    break;

                case SubscriptionPlan.Tier2:
                    subPlan = "2000";
                    break;

                case SubscriptionPlan.Tier3:
                    subPlan = "3000";
                    break;
            }*/

            // todo: subscriber queue
        }

        public TwitchDBUser CreateTwitchDBUser(TwitchUser user)
        {
            var tdbuser = new TwitchDBUser(user.DisplayName, Database, ActiveStream);
            tdbuser.TwitchUser = user;
            return tdbuser;
        }

        private async void PubSub_OnFollow(object sender, OnFollowArgs e)
        {
            Log("On follow " + e.DisplayName);

            if (Followers.Any(f => f.DisplayName == e.DisplayName) && !debugCall)
            {
                Log("User already in list", true);
                return;
            }

            try
            {
                User user = await GetUserByName(e.DisplayName);
                var subs = new List<Subscription>();
                try
                {
                    subs = await GetAllSubscriptions();
                }
                catch { }

                var newFollower = new TwitchUser(user, DateTime.Now,
                    subs.SingleOrDefault(s => s.User.DisplayName == user.DisplayName), true);
                newFollower.TwitchDBUser = CreateTwitchDBUser(newFollower);

                Log("Got user " + newFollower.DisplayName);

                if (debugCall)
                {
                    debugCall = false;
                }

                MainManager.OnFollowerEvent.Invoke(newFollower);

                Followers.Add(newFollower);

                if (!AllUsers.Any(f => f.DisplayName == newFollower.DisplayName))
                {
                    AllUsers.Add(newFollower);
                }

                Log("added user to followers");

                await Globals.GenerateTextureData(newFollower);

                Log("Adding to queue");

                MainManager.FollowerQueue.Add(newFollower);
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        private async void PubSub_OnPubSubServiceConnected(object sender, System.EventArgs e)
        {
            Log("PubSub Service connected");

            pubSub.ListenToFollows(await GetTwitchId());
            pubSub.ListenToBitsEvents(await GetTwitchId());
            pubSub.ListenToSubscriptions(await GetTwitchId());

            pubSub.SendTopics(S_ClientOauth);

            pubSubConnected = true;
        }

        private void PubSub_OnPubSubServiceError(object sender, OnPubSubServiceErrorArgs e) => Log(e.Exception.Message);

        private void TwitchClient_OnConnected(object sender, TwitchLib.Client.Events.OnConnectedArgs e) => Log("TwitchClient connected");

        private void TwitchClient_OnJoinedChannel(object sender, TwitchLib.Client.Events.OnJoinedChannelArgs e) => Log("Joined: " + e.Channel);

        private void TwitchClient_OnLeftChannel(object sender, TwitchLib.Client.Events.OnLeftChannelArgs e) => Log("Left: " + e.Channel);

        private void TwitchClient_OnLog(object sender, TwitchLib.Client.Events.OnLogArgs e)
        {
            //Log("TwitchClientLog: " + e.Data);
        }

        private async void TwitchClient_OnMessageReceived(object sender, TwitchLib.Client.Events.OnMessageReceivedArgs e)
        {
            await WaitForInit();

            if (ActiveStream.Id == 0 || !FinishedDownloadingFollowers)
            {
#if !DEBUG
                return;
#endif
            }

            TwitchUser user = AllUsers.SingleOrDefault(f => f.DisplayName.ToLower() == e.ChatMessage.DisplayName.ToLower());
            if (user == null)
            {
                User tuser = await GetUserByName(e.ChatMessage.Username);
                user = new TwitchUser(tuser, DateTime.Now, null, false);
                user.TwitchDBUser = CreateTwitchDBUser(user);
                await Globals.GenerateTextureData(user);
                AllUsers.Add(user);
            }

            if (e.ChatMessage.Message.StartsWith(Globals.S_CommandPrefix))
            {
                MainManager.OnCommandMessageEvent.Invoke(user, e.ChatMessage);
                user.TwitchDBUser.LastNonLeaveEvent = DateTime.Now;
                Database.LogEvent(ActiveStream, StreamEvent.COMMAND, user, e.ChatMessage.Message);
            }
            else
            {
                if (e.ChatMessage.DisplayName.ToLower() == S_ClientUsername.ToLower() ||
                    e.ChatMessage.DisplayName.ToLower() == S_BotUsername.ToLower() ||
                    e.ChatMessage.DisplayName.ToLower() == "streamelements")
                {
                    return;
                }

                MainManager.OnChatMessageEvent.Invoke(user, e.ChatMessage);
                user.TwitchDBUser.LastNonLeaveEvent = DateTime.Now;
                Database.LogEvent(ActiveStream, StreamEvent.MESSAGE, user, e.ChatMessage.Message);
            }
        }

        private void TwitchClient_OnWhisperSent(object sender, TwitchLib.Client.Events.OnWhisperSentArgs e) => Log($"Sent whisper to {e.Receiver}, message {e.Message}");
    }
}