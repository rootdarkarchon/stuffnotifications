﻿using Assets.Base;
using Assets.Helpers.AI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Managers
{
    public class DebugManager : MonoBehaviourSettings
    {
        public MainManager MainManager;

        public async override Task UpdateWithSettings()
        {
            await MainManager.WaitForInit();

            string randName = "MissMinty18";
            string msg = "";

            if (Input.GetKeyDown(KeyCode.C))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"following.56570811\",\"message\": { \"user_id\":\"56570811\",\"display_name\":\"" + randName + "\",\"username\":\"" + randName + "\" } } }";
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-subscribe-events-v1.56570811\",\"message\": { \"user_id\":\"56570811\",\"sub_message\": { \"message\": \"TestSubMessage\", \"emotes\": null },\"months\":\"1\",\"cumulative-months\":\"3\",\"sub_plan\":\"1000\",\"display_name\":\"" + randName + "\",\"user_name\":\"" + randName + "\" } } }";
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-subscribe-events-v1.56570811\",\"message\": { \"user_id\":\"56570811\",\"sub_message\": { \"message\": \"TestSubMessage\", \"emotes\": null },\"months\":\"1\",\"cumulative-months\":\"3\",\"sub_plan\":\"2000\",\"display_name\":\"" + randName + "\",\"user_name\":\"" + randName + "\" } } }";
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-subscribe-events-v1.56570811\",\"message\": { \"user_id\":\"56570811\",\"sub_message\": { \"message\": \"TestSubMessage\", \"emotes\": null },\"months\":\"1\",\"cumulative-months\":\"3\",\"sub_plan\":\"3000\",\"display_name\":\"" + randName + "\",\"user_name\":\"" + randName + "\" } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad0))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa50 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"50\",\"total_bits_used\":\"1000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa150 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"150\",\"total_bits_used\":\"1000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa1000 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"1000\",\"total_bits_used\":\"1000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa5000 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"5000\",\"total_bits_used\":\"5000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa10000 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"10000\",\"total_bits_used\":\"5000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                msg = "{ \"type\":\"MESSAGE\",\"data\": { \"topic\":\"channel-bits-events-v1.56570811\",\"message\": { \"data\": { \"user_id\":\"56570811\",\"chat_message\":\"TestChatMessage Kappa10000 Kappa1000 Kappa1000 Kappa1000 Kappa1000\",\"bits_used\":\"20000\",\"total_bits_used\":\"5000\",\"user_name\":\"" + randName + "\" } } } }";
            }

            if (msg != "")
            {
                MainManager.TwitchManager.ParseTestPubSubMessage(msg);
            }

            if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                GameObject.Find("DefendingLayer").GetComponent<DefenseGameLogic>().DebugSpawnWave(DefenseDirection.North);
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                GameObject.Find("DefendingLayer").GetComponent<DefenseGameLogic>().DebugSpawnWave(DefenseDirection.West);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                GameObject.Find("DefendingLayer").GetComponent<DefenseGameLogic>().DebugSpawnWave(DefenseDirection.East);
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                GameObject.Find("DefendingLayer").GetComponent<DefenseGameLogic>().DebugSpawnWave(DefenseDirection.South);
            }

            var playerAI = GameObject.FindObjectOfType<PlayerAI>();
            if (Input.GetKeyDown(KeyCode.I))
            {
                await GameObject.Find("DefendingLayer").GetComponent<PlayerAIControl>().MovePlayerDestination(playerAI, DefenseDirection.North);
            }
            if (Input.GetKeyDown(KeyCode.J))
            {
                await GameObject.Find("DefendingLayer").GetComponent<PlayerAIControl>().MovePlayerDestination(playerAI, DefenseDirection.West);
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                await GameObject.Find("DefendingLayer").GetComponent<PlayerAIControl>().MovePlayerDestination(playerAI, DefenseDirection.East);
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                await GameObject.Find("DefendingLayer").GetComponent<PlayerAIControl>().MovePlayerDestination(playerAI, DefenseDirection.South);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Log("Forcing settings database reload");
                SettingsManager.ReloadSettings();

                foreach (MonoBehaviourSettings instance in new List<MonoBehaviourSettings>(SettingsManager.Instances))
                {
                    instance.InitializeAllSettings();
                }
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                Log("Forcing user output");

                var chatters = MainManager.Database.GetAllChatters(MainManager.TwitchManager.ActiveStream);
                var ordered = chatters.Select(c => (c.Key, c.Value.SelectMany(k => k.Split(new[] { " " }, StringSplitOptions.None)).Count())).OrderByDescending(c => c.Item2).ToList();

                Globals.WritePeopleFromLastStream(ordered.Select(c => c.Key).ToList());

                System.IO.File.Copy("TwitchDB.sqlite", $"TwitchDB.{DateTime.Now.ToString("yyyyMMddHHmmss")}.sqlite", true);
            }
        }
    }
}