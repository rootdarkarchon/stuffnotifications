﻿using Assets.Base;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Managers
{
    [Serializable]
    public class SettingsManager : MonoBehaviour
    {
        public DatabaseLayer Database;
        public List<Setting> Settings;
        private bool initialized;

        public List<MonoBehaviourSettings> Instances;

        public SettingsManager()
        {
        }

        public async void Start()
        {
            Instances = new List<MonoBehaviourSettings>();
            await Database.WaitForInit();
            ReloadSettings();
        }

        public async Task WaitForInit()
        {
            while (initialized == false)
            {
                await Task.Delay(100);
            }
        }

        public void Update()
        {
            if (Settings == null)
            {
                ReloadSettings();
            }
        }

        public void ReloadSettings()
        {
            try
            {
                initialized = false;
                Settings = Database.GetAllSettings();
                initialized = true;
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.Log(ex);
            }
        }

        public T GetSetting<T>(string key, MonoBehaviourSettings instance)
        {
            Setting setting = Settings.FirstOrDefault(f => f.Key().ToLower() == key.ToLower() && f.Module().ToLower() == instance.GetType().Name.ToLower());

            if (setting == null)
            {
                return default;
            }
            else
            {
                if (!Instances.Contains(instance))
                {
                    Instances.Add(instance);
                }

                return setting.Value<T>();
            }
        }

        public T GetSettingSpecific<T>(string sourceWithKey)
        {
            Setting setting = Settings.FirstOrDefault(f => f.ToString().ToLower() == sourceWithKey.ToLower());

            if (setting == null)
            {
                return default;
            }
            else
            {
                return setting.Value<T>();
            }
        }

        public void SetSetting(string key, string value, MonoBehaviourSettings instance)
        {
            Setting setting = Settings.FirstOrDefault(f => f.Key().ToLower() == key.ToLower() && f.Module().ToLower() == instance.GetType().Name.ToLower());
            setting.RawValue = value;

            Database.UpdateSetting(setting);
        }

        public void SetSettingSpecific(string sourceWithKey, string value)
        {
            Setting setting = Settings.FirstOrDefault(f => f.ToString().ToLower() == sourceWithKey.ToLower());
            setting.RawValue = value;

            Database.UpdateSetting(setting);

            ReloadSettings();

            foreach (MonoBehaviourSettings manager in Instances)
            {
                manager.InitializeAllSettings();
            }
        }
    }
}