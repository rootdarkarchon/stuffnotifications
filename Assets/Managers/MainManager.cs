﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.Linq;
using Asyncoroutine;
using System.Threading;
using System.Reflection;

namespace Assets.Managers
{
    [Serializable]
    public class MainManager : MonoBehaviourSettings
    {
        public List<TwitchUser> FollowerQueue;
        public List<TwitchUser> SubscriberQueue;
        public List<BitEvent> BitsQueue;
        public List<WelcomeMessage> WelcomeMessageQueue;
        public TwitchManager TwitchManager;
        public BoxCreatorHelper BoxCreatorHelper;
        public TableCreatorHelper TableCreatorHelper;
        public SoundManager SoundManager;
        public DatabaseLayer Database;
        public ValidationManager ValidationManager;
        public MainCamera MainCamera;
        public List<ChatModule> RegisteredModules;
        public OnCommandMessageEvent OnCommandMessageEvent;
        public OnChatMessageEvent OnChatMessageEvent;
        public OnSubscriberEvent OnSubscriberEvent;
        public OnFollowerEvent OnFollowerEvent;
        public OnBitsEvent OnBitsEvent;

        [Setting("DEBUGMODE", false)]
        public bool IsDebug;

        public string DebugUIText
        {
            get
            {
                return GUIManager.GetUpText();
            }
            set
            {
                if (IsDebug)
                {
                    GUIManager.SetUpText(value);
                }
                else
                {
                    GUIManager.ClearUpText();
                }
            }
        }

        public override async Task StartWithSettings()
        {
            CurrencyExtensions.Globals = Globals;

            QualitySettings.vSyncCount = 1;
            FollowerQueue = new List<TwitchUser>();
            SubscriberQueue = new List<TwitchUser>();
            WelcomeMessageQueue = new List<WelcomeMessage>();
            BitsQueue = new List<BitEvent>();

            TaskScheduler.UnobservedTaskException += (s, e) => Debug.LogError(e.Exception);

            RegisteredModules = RegisteredModules ?? new List<ChatModule>();
            OnCommandMessageEvent = OnCommandMessageEvent ?? new OnCommandMessageEvent();
            OnChatMessageEvent = OnChatMessageEvent ?? new OnChatMessageEvent();
            OnSubscriberEvent = OnSubscriberEvent ?? new OnSubscriberEvent();
            OnFollowerEvent = OnFollowerEvent ?? new OnFollowerEvent();
            OnBitsEvent = OnBitsEvent ?? new OnBitsEvent();

            Globals.CreateFoldersIfNecessary();

            await TwitchManager.WaitForInit();
            await BoxCreatorHelper.WaitForInit();
            await TableCreatorHelper.WaitForInit();

            GUIManager.SetUpText("Initialized");

            //await new WaitForMainThread();
            //(Resources.FindObjectsOfTypeAll(typeof(GameObject)).Single(f => f.name == "DefendingLayer") as GameObject).SetActive(false);
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        public async override Task UpdateWithSettings()
        {
            KeepChatModulesAlive();
        }

        private void KeepChatModulesAlive()
        {
            foreach (ChatModule chatModule in RegisteredModules)
            {
                OnCommandMessageEvent.AddChatModule(chatModule);
                OnChatMessageEvent.AddChatModule(chatModule);
                OnSubscriberEvent.AddChatModule(chatModule);
                OnFollowerEvent.AddChatModule(chatModule);
                OnBitsEvent.AddChatModule(chatModule);
            }
        }

        internal void RegisterModule(ChatModule chatModule)
        {
            if (RegisteredModules == null)
            {
                RegisteredModules = new List<ChatModule>();
            }

            if (!RegisteredModules.Contains(chatModule))
            {
                RegisteredModules.Add(chatModule);
            }
        }

        void OnApplicationQuit()
        {
#if UNITY_EDITOR
            var constructor = SynchronizationContext.Current.GetType().GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(int) }, null);
            var newContext = constructor.Invoke(new object[] { Thread.CurrentThread.ManagedThreadId });
            SynchronizationContext.SetSynchronizationContext(newContext as SynchronizationContext);
#endif
        }
    }
}