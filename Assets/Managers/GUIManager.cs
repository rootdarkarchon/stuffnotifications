﻿using Assets.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Managers
{
    public class GUIManager : MonoBehaviourSettings
    {
        public Canvas MainCanvas;
        public Canvas LowTextCanvas;
        public Canvas UpTextCanvas;
        public Canvas WelcomeBackCanvas;

        public Text LowText;
        public Text UpText;
        public Text WelcomeBackText;

        private List<string> TopTexts;

        public async override Task StartWithSettings()
        {
            LowText = LowTextCanvas.GetComponent<Text>();
            UpText = UpTextCanvas.GetComponent<Text>();
            WelcomeBackText = WelcomeBackCanvas.GetComponent<Text>();
            GameObject.Find("CanvasCamera").GetComponent<Camera>().enabled = false;
            TopTexts = new List<string>();
        }

        public string GetUpText()
        {
            return UpText.text;
        }

        public void SetUpText(string text)
        {
            if(TopTexts == null) { return; }
            TopTexts.Add(text);
            UpText.text = string.Join(Environment.NewLine, TopTexts);
            StartCoroutine(ClearTopTextCountdown(text));
        }

        private IEnumerator ClearTopTextCountdown(string text)
        {
            yield return new WaitForSecondsRealtime(5);
            TopTexts.RemoveAll(f => f == text);
            UpText.text = string.Join(Environment.NewLine, TopTexts);
        }

        public void ClearUpText()
        {
            UpText.text = "";
        }

        public string GetLowText()
        {
            return LowText.text;
        }

        public void SetLowText(string text)
        {
            LowText.text = text;
        }

        public void ClearLowText()
        {
            LowText.text = "";
        }

        public void AddGameObjectToWelcomeBack(GameObject gameObject)
        {
            var camera = GameObject.Find("CanvasCamera").GetComponent<Camera>();
            gameObject.transform.SetParent(WelcomeBackCanvas.transform);
            gameObject.transform.localScale = gameObject.transform.localScale * 500;
            gameObject.transform.rotation = new Quaternion();
            gameObject.layer = 8;
            gameObject.transform.position = camera.transform.position + Vector3.forward * 500;
            camera.enabled = true;
        }

        public void RemoveGameObjectFromWelcomeBack()
        {
            GameObject.Find("CanvasCamera").GetComponent<Camera>().enabled = false;
        }

        public void SetWelcomeBackText(string text)
        {
            WelcomeBackText.text = text;
        }

        public void ClearWelcomeBackText()
        {
            WelcomeBackText.text = "";
        }
    }
}
