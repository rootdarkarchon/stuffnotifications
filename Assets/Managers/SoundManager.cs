﻿using Assets.Base;
using Assets.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Audio;

namespace Assets.Managers
{
    public class SoundManager : MonoBehaviourSettings
    {
        public AudioSource AudioSource;
        public List<SoundEffect> SoundEffectQueue;

        public AudioMixerSnapshot FadeOut;
        public AudioMixerSnapshot Normal;

        public bool PlayingQueuedSoundEffects;

        public async override Task StartWithSettings()
        {
            SoundEffectQueue = new List<SoundEffect>();
            PlayingQueuedSoundEffects = false;
        }

        private IEnumerator PlayQueuedSoundEffects()
        {
            if (!PlayingQueuedSoundEffects && SoundEffectQueue.Any())
            {
                PlayingQueuedSoundEffects = true;
                foreach (SoundEffect soundEffect in new List<SoundEffect>(SoundEffectQueue))
                {
                    FadeInAudio();
                    CurrentlyPlayingFromQueue = soundEffect;
                    AudioSource.clip = soundEffect.AudioClip;
                    //AudioSource.Play();
                    AudioSource.volume = soundEffect.Volume;
                    AudioSource.loop = false;
                    AudioSource.priority = 0;
                    PlaySoundEffect(soundEffect);
                    yield return new WaitUntil(() => !AudioSource.isPlaying);
                    SoundEffectQueue.Remove(soundEffect);
                    AudioSource.clip = null;
                    CurrentlyPlayingFromQueue = null;
                }
                PlayingQueuedSoundEffects = false;
            }
        }

        public IEnumerator FadeOutAudio()
        {
            FadeOut.TransitionTo(5);
            yield return new WaitForSeconds(5);
            AudioSource.Stop();
        }

        public void FadeInAudio()
        {
            Normal.TransitionTo(0);
        }

        public SoundEffect CurrentlyPlayingFromQueue;

        public void EnqueueSoundEffect(SoundEffect soundEffect) => SoundEffectQueue.Add(soundEffect);

        public void PlaySoundEffect(SoundEffect soundEffect)
        {
            FadeInAudio();
            AudioSource.PlayOneShot(soundEffect.AudioClip, soundEffect.Volume);
        }

        public async override Task UpdateWithSettings() => _ = StartCoroutine(PlayQueuedSoundEffects());
    }
}