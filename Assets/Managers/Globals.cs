﻿using Assets.Base;
using Assets.Models;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Managers
{
    public class Globals : MonoBehaviourSettings
    {
        // none of the previous oauth keys are valid anymore, don't even try ;)

        [Setting("COMMAND_PREFIX", "!")]
        public string S_CommandPrefix;

        [Setting("CURRENCY_1_NAME", "rubles 💰")]
        public string S_Currency1Name;

        [Setting("CURRENCY_2_NAME", "bananas 🍌")]
        public string S_Currency2Name;

        [Setting("FOLDER_IMAGEDATA", "imagedata")]
        public string S_ImageDataFolder;

        [Setting("FILE_PEOPLEINSTREAM_NAME", "peopleInStream.txt")]
        public string S_PeopleInStreamFile;

        [Setting("FOLDER_SOUNDDATA_WELCOME", "sounddata")]
        public string S_SoundDataFolder;

        [Setting("FOLDER_SOUNDDATA_EFFECTS", "soundeffects")]
        public string S_SoundEffectsFolder;

        public static T Cast<T>(object o) => (T)o;

        /// <summary>
        /// Interleaves two equally long lists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static IEnumerable<T> InterleaveEnumerationsOfEqualLength<T>(
        IEnumerable<T> first,
        IEnumerable<T> second)
        {
            using (IEnumerator<T>
                enumerator1 = first.GetEnumerator(),
                enumerator2 = second.GetEnumerator())
            {
                while (enumerator1.MoveNext() && enumerator2.MoveNext())
                {
                    yield return enumerator1.Current;
                    yield return enumerator2.Current;
                }
            }
        }

        public static void SetMeshRendererVisibilityForGameObject(GameObject gameObject, bool isVisible) => gameObject.GetComponent<MeshRenderer>().enabled = isVisible;

        public void CreateFoldersIfNecessary()
        {
            if (!Directory.Exists(S_ImageDataFolder))
            {
                Directory.CreateDirectory(S_ImageDataFolder);
            }

            if (!Directory.Exists(Path.Combine(S_ImageDataFolder, "emotes")))
            {
                Directory.CreateDirectory(Path.Combine(S_ImageDataFolder, "emotes"));
            }

            if (!Directory.Exists(S_SoundDataFolder))
            {
                Directory.CreateDirectory(S_SoundDataFolder);
            }

            if (!Directory.Exists(S_SoundEffectsFolder))
            {
                Directory.CreateDirectory(S_SoundEffectsFolder);
            }
        }

        public async Task<byte[]> LoadEmoteImage(BitEvent bitEvent)
        {
            byte[] result;

            string localFile = Path.Combine(S_ImageDataFolder, "emotes", bitEvent.Emote.Prefix + bitEvent.AmountNormalized + ".png");

            if (File.Exists(localFile))
            {
                result = File.ReadAllBytes(localFile);
            }
            else
            {
                using (WebClient wc = new WebClient())
                {
                    result = await wc.DownloadDataTaskAsync(bitEvent.EmoteUrl);
                }
                File.WriteAllBytes(localFile, result);
            }

            return result;
        }

        public AudioClip CreateSoundClipFromFile(string soundPath)
        {
            AudioClip craftClip;
            using (var aud = new AudioFileReader(soundPath))
            {
                float[] audioData = new float[(int)aud.Length];
                aud.Read(audioData, 0, (int)aud.Length);
                craftClip = AudioClip.Create(Path.GetFileNameWithoutExtension(soundPath),
                    (int)(aud.Length / 8), aud.WaveFormat.Channels, aud.WaveFormat.SampleRate, false);
                Log($"Creating clip with length {(int)aud.Length} and samplerate {aud.WaveFormat.SampleRate} and bits per sample {aud.WaveFormat.BitsPerSample}");
                craftClip.SetData(audioData, 0);
            }

            return craftClip;
        }

        public async Task GenerateTextureData(TwitchUser twitchUser)
        {
            try
            {
                using (var client = new WebClient())
                {
                    byte[] imageToSave;
                    if (!File.Exists(Path.Combine(S_ImageDataFolder, twitchUser.DisplayName + ".png")) ||
                        (DateTime.Now - twitchUser.UpdatedAt).TotalDays < 1 ||
                        twitchUser.IsSub)
                    {
                        byte[] data = await client.DownloadDataTaskAsync(twitchUser.Logo);
                        imageToSave = twitchUser.GenerateUVTextureForCoin(data);
                        File.WriteAllBytes(Path.Combine(S_ImageDataFolder, twitchUser.MainTexture), imageToSave);
                    }

                    imageToSave = twitchUser.GenerateEmissionTextureForCoin();
                    File.WriteAllBytes(Path.Combine(S_ImageDataFolder, twitchUser.EmissionTexture), imageToSave);
                }
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        public string GetCurrencyName(CurrencyType currency)
        {
            switch (currency)
            {
                case CurrencyType.Currency1:
                    return S_Currency1Name;

                case CurrencyType.Currency2:
                    return S_Currency2Name;
            }

            return S_Currency1Name;
        }

        public string ReplaceTextWithSettings(string description)
        {
            foreach (Match match in Regex.Matches(description, @"\[(\w)+\.(\w)+\]"))
            {
                string setting = SettingsManager.GetSettingSpecific<string>(match.Value.Replace("[", "").Replace("]", ""));
                try
                {
                    description = description.Replace(match.Value, setting.ToString());
                }
                catch { }
            }

            return description;
        }

        public void WritePeopleFromLastStream(List<string> usersInLastStream) => File.WriteAllLines(S_PeopleInStreamFile, usersInLastStream);

        internal byte[] ReadAllImageBytes(string emissionTexture) => File.ReadAllBytes(Path.Combine(S_ImageDataFolder, emissionTexture));
    }
}