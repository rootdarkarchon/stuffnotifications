﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Assets.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Assets.Managers
{
    public class ValidationManager : MonoBehaviourSettings
    {
        public DatabaseLayer Database;
        public TwitchManager TwitchManager;

        internal object[] ParseAndValidate(TwitchDBUser userName, string[] methodParameters, ChatModule invoker, MethodInfo method)
        {
            ParameterInfo[] parameters = method.GetParameters().ToArray();

            CommandAttribute command = method.GetCustomAttribute<CommandAttribute>();

            // this is different, we are not knowing the command that is called, it is in the list
            // keep original methodparameters if the commandlistname is not empty
            if (string.IsNullOrEmpty(command.ActualCommandListName))
            {
                methodParameters = methodParameters.Skip(1).ToArray();
            }

            // verify that if self is available, self is the first parameter. also add executing username to the parameter mix then.
            if (parameters.Any(c => c.GetCustomAttribute<C_UserSelfAttribute>() != null))
            {
                if (parameters[0].GetCustomAttribute<C_UserSelfAttribute>().GetType() != typeof(C_UserSelfAttribute))
                {
                    throw new Exception($"if {nameof(C_UserSelfAttribute)} is used it needs to be the first parameter in the method");
                }
                else
                {
                    methodParameters = new List<string>() { userName.Username }.Union(methodParameters).ToArray();
                }
            }

            // if non-default parameter count is not equal to the called parameters, show the usage
            if (parameters.Where(p => !p.HasDefaultValue).Count() > methodParameters.Length)
            {
                CallHelp(userName.Username, invoker, method);
                return null;
            }

            var initializedParameters = new AttributeTypedList();

            // iterate through all parameters of the function
            for (int i = 0; i < parameters.Length; i++)
            {
                // verify that the parameter has an attribute linked to the base attribute
                if (!(parameters[i].GetCustomAttributes()
                    .SingleOrDefault(f => f.GetType().IsSubclassOf(typeof(C_BaseAttribute))) is C_BaseAttribute paramAttr))
                {
                    throw new Exception($"All parameters of commands need to inherit from {nameof(C_BaseAttribute)}");
                }

                if (parameters[i].ParameterType != paramAttr.AllowedType)
                {
                    throw new Exception($"{parameters[i].ParameterType} of parameter {parameters[i].Name} is invalid for {paramAttr.GetType()}, expected {paramAttr.AllowedType}");
                }

                try
                {
                    // if the attribute type is amount then max and all is valid even if the type is a long
                    // so we replace the user input parameter by the maximum amount of currency that they have
                    if (paramAttr.GetType() == typeof(C_AmountAttribute))
                    {
                        if (((C_AmountAttribute)paramAttr).AllowAll
                            && (methodParameters[i].ToLower() == "max" || methodParameters[i].ToLower() == "all"))
                        {
                            methodParameters[i] = userName.GetCurrency(((C_AmountAttribute)paramAttr).CurrencyType).ToString();
                        }
                    }

                    if (paramAttr.GetType() == typeof(C_FreeTextAttribute))
                    {
                        methodParameters[i] = string.Join(" ", methodParameters.Skip(i));
                    }

                    paramAttr.Value = ValidateParameter(methodParameters[i], parameters[i]);
                }
                catch (IndexOutOfRangeException)
                {
                    // if there are not enough method parameters we take the function default value
                    paramAttr.Value = parameters[i].DefaultValue;
                }

                // if we fail to convert an object then we need to let the user know that the input was invalid
                // exception is for twitchdbuser which eventually can be null but this runs against the validator
                if (paramAttr.Value == null && parameters[i].ParameterType != typeof(TwitchDBUser))
                {
                    SendError(userName.Username, methodParameters[i], parameters[i].Name);
                    return null;
                }

                // finally add the converted object to the parameter attribute
                initializedParameters.Add(paramAttr);
            }

            // now we get all method custom attributes that are validators
            IEnumerable<Attribute> validateAttributes = method.GetCustomAttributes().Where(f => f.GetType().IsSubclassOf(typeof(ValidateAttribute)));
            foreach (Attribute attr in validateAttributes)
            {
                // and we validate each attribute
                var validAttr = (ValidateAttribute)attr;
                bool isValid = ValidateAttribute(validAttr, command, invoker, initializedParameters);

                // if invalid we need to send the error message
                if (!isValid)
                {
                    Log($"{validAttr.GetType().Name} for {command.Command} failed to validate", true);

                    SendErrorValidation(validAttr);

                    return null;
                }
            }

            // finally return the completely parsed object array of all parameters
            return initializedParameters.Select(k => k.Value).ToArray();
        }

        private void SendErrorValidation(ValidateAttribute attr)
        {
            if (!string.IsNullOrEmpty(attr.ErrorMessage))
            {
                TwitchManager.SendChannelMessage(attr.ErrorMessage);
            }
        }

        private bool ValidateAttribute(ValidateAttribute attr, CommandAttribute command, ChatModule invoker, AttributeTypedList mappedParameters)
        {
            Log($"Validating {attr.GetType().Name} for {command.Command} of {invoker.GetType().Name} and values {string.Join(", ", mappedParameters.Select(c => c.GetType().Name + "=" + c.Value?.ToString()))}");
            attr.Database = Database;
            attr.SettingsManager = SettingsManager;
            attr.Globals = Globals;
            return attr.Validate(invoker, command, mappedParameters);
        }

        private object ValidateParameter(string parameterValue, ParameterInfo parameterInfo)
        {
            if (parameterInfo.ParameterType == typeof(TwitchDBUser))
            {
                return TwitchManager.AllUsers.SingleOrDefault(f => f.DisplayName.ToLower() == parameterValue.ToLower())?.TwitchDBUser;
            }
            if (parameterInfo.ParameterType == typeof(string[]))
            {
                return parameterValue.Split(new string[] { " " }, StringSplitOptions.None);
            }
            try
            {
                return TypeDescriptor.GetConverter(parameterInfo.ParameterType).ConvertFromString(parameterValue);
            }
            catch
            {
                Log($"Could not convert \"{parameterValue}\" to {parameterInfo.ParameterType}", true);
                return null;
            }
        }

        private void SendError(string userName, string value, string parameterName) => TwitchManager.SendChannelMessage($"{userName}, \"{value}\" is an invalid value for \"{parameterName}\"");

        private void CallHelp(string userName, ChatModule invoker, MethodInfo method) => TwitchManager.SendChannelMessage($"{userName}: {invoker.GetCommandUsage(method)}");
    }
}