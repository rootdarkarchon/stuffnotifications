﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateModuleAttribute : ValidateAttribute
    {
        public ValidateModuleAttribute() => ErrorMessage = $"[C_UserSelf] the module [C_Module] does not exist.";

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_UserSelfAttribute), typeof(C_ModuleAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            return Assembly.GetExecutingAssembly().GetTypes().Count(t => t.Name.ToLower() == mapping.C_Module.StringValue) == 1;
        }
    }
}