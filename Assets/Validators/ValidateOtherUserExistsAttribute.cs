﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ValidateOtherUserExistsAttribute : ValidateAttribute
    {
        public ValidateOtherUserExistsAttribute() => ErrorMessage = "[C_UserOther] does not exist";

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_UserOtherAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            TwitchDBUser otherUserName = mapping.C_UserOther.TwitchDBUserValue;

            if (otherUserName == null)
            {
                return mapping.C_UserOther.AllowOtherUserToBeEmpty;
            }

            bool valid = otherUserName?.Currency1 >= 0;
            if (!valid)
            {
                ErrorMessage = $"{otherUserName} does not exist.";
            }

            return valid;
        }
    }
}