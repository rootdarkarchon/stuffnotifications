﻿using Assets.Base;
using Assets.Helpers;
using Assets.Managers;
using Assets.Models;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateUserBuyAmountAttribute : ValidateAttribute
    {
        public ValidateUserBuyAmountAttribute() => ErrorMessage = "[C_UserSelf] you do not have enough [Globals.CURRENCY_1_NAME] to buy [C_Amount] [Globals.CURRENCY_2_NAME]";

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_UserSelfAttribute), typeof(C_AmountAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            long cost = SettingsManager.GetSettingSpecific<long>("CurrencyModule.CURRENCY_2_COST");

            TwitchDBUser user = mapping.C_UserSelf.TwitchDBUserValue;
            C_AmountAttribute amount = mapping.C_Amount;

            if (user.Currency1 == amount.LongValue)
            {
                amount.Value = amount.LongValue / cost;
                if(amount.LongValue == 0)
                {
                    return false;
                }
                return true;
            }

            return cost * amount.LongValue <= user.Currency1;
        }
    }
}