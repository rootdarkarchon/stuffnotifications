﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateSettingValueAttribute : ValidateAttribute
    {
        public ValidateSettingValueAttribute() => ErrorMessage = $"[C_UserSelf] the value \"[C_SettingValue]\" for \"[C_Setting]\" is illegal. Error: ";

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_SettingAttribute), typeof(C_SettingValueAttribute), typeof(C_ModuleAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            C_BaseAttribute setting = mapping.C_Setting;

            string classToCheck;
            string settingToCheck;

            classToCheck = mapping.C_Module.StringValue;
            settingToCheck = setting.StringValue;

            Type type = Assembly.GetExecutingAssembly().GetTypes().SingleOrDefault(a => a.Name.ToLower() == classToCheck.ToLower());
            if (type == null)
            {
                ErrorMessage += $"{classToCheck} does not exist";
                return false;
            }

            FieldInfo field = type.GetFields().SingleOrDefault(f => f.GetCustomAttribute<SettingAttribute>()?.SettingName.ToLower() == settingToCheck.ToLower());
            if (field == null)
            {
                ErrorMessage += $"{settingToCheck} does not exist";
                return false;
            }

            try
            {
                _ = TypeDescriptor.GetConverter(field.FieldType).ConvertFromString(mapping.C_SettingValue.StringValue);
                return true;
            }
            catch
            {
                ErrorMessage += $"expected {settingToCheck} be {field.FieldType}";
                return false;
            }
        }
    }
}