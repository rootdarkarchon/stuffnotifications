﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateAttributeInRangeAttribute : ValidateAttribute
    {
        public long MaxValue;
        public long MinValue;
        public Type AttributeType;

        public ValidateAttributeInRangeAttribute(Type attributeType, long minValue = 0, long maxValue = long.MaxValue)
        {
            AttributeType = attributeType;
            MinValue = minValue;
            MaxValue = maxValue;
            ErrorMessage = "";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { AttributeType };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            C_BaseAttribute attr = mapping.Get(AttributeType);

            bool valid = attr.LongValue <= MaxValue && attr.LongValue >= MinValue;
            if (!valid)
            {
                ErrorMessage = $"{attr.Value} needs to be between {MinValue} and {MaxValue}";
            }

            return valid;
        }
    }
}