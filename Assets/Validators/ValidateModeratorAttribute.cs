﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateModeratorAttribute : ValidateAttribute
    {
        private readonly long ModeratorLevel;

        public ValidateModeratorAttribute(long moderatorLevel = 1)
        {
            ModeratorLevel = moderatorLevel;

            ErrorMessage = $"[C_UserSelf] you are not a moderator of at least level {moderatorLevel} and cannot run this command.";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_UserSelfAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            return ModeratorLevel <= mapping.C_UserSelf.TwitchDBUserValue.ModeratorLevel;
        }
    }
}