﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using Assets.Modules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateUserInDuelAttribute : ValidateAttribute
    {
        public string ListName;
        public bool ShouldBeInDuel;
        public Type DuelUserToCheck;

        public ValidateUserInDuelAttribute(string listName, Type duelUserCheck, bool shouldBeInDuel)
        {
            ListName = listName;
            ShouldBeInDuel = shouldBeInDuel;

            DuelUserToCheck = duelUserCheck;

            ErrorMessage = "";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { DuelUserToCheck };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            TwitchDBUser user = mapping.Get(DuelUserToCheck).TwitchDBUserValue;

            List<Duel> list = GetFieldValue<List<Duel>>(invoker, ListName);

            return list.Any(f => f.Opponent == user) == ShouldBeInDuel;
        }
    }
}