﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateUserAmountAttribute : ValidateAttribute
    {
        private readonly Type UserParameterType;

        public ValidateUserAmountAttribute(Type parameterType = null)
        {
            if (parameterType == null)
            {
                parameterType = typeof(C_UserSelfAttribute);
            }

            if (parameterType != typeof(C_UserOtherAttribute) && parameterType != typeof(C_UserSelfAttribute))
            {
                throw new Exception($"ParameterType needs to be {nameof(C_UserOtherAttribute)} or {nameof(C_UserSelfAttribute)}");
            }

            UserParameterType = parameterType;

            ErrorMessage = $"[{UserParameterType.Name}] does not have [C_Amount] [Globals.CURRENCY_1_NAME].";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { UserParameterType, typeof(C_AmountAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            TwitchDBUser user = mapping.Get(UserParameterType).TwitchDBUserValue;
            long amount;
            CurrencyType currencyType;
            try
            {
                C_AmountAttribute attr = mapping.C_Amount;
                amount = attr.LongValue;
                currencyType = attr.CurrencyType;
            }
            catch
            {
                amount = command.Cost;
                currencyType = command.Currency;
            }

            return user.GetCurrency(currencyType) >= amount;
        }
    }
}