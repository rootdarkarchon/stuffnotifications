﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidatePositiveAmountAttribute : ValidateAttribute
    {
        public Type AttributeType;

        public ValidatePositiveAmountAttribute(Type attribute = null)
        {
            if (attribute == null)
            {
                attribute = typeof(C_AmountAttribute);
            }

            AttributeType = attribute;

            ErrorMessage = $"[{attribute.Name.Replace("Attribute", "")}] needs to be larger than 0";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { AttributeType };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            C_BaseAttribute amount = mapping.Get(AttributeType);

            return amount.LongValue > 0;
        }
    }
}