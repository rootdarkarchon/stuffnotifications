﻿using Assets.Base;
using Assets.Helpers;
using Assets.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method)]
    public abstract class ValidateAttribute : Attribute
    {
        public DatabaseLayer Database;
        public Globals Globals;
        public SettingsManager SettingsManager;
        public string ErrorMessage { get; set; }
        public abstract List<Type> RequiredParameterTypes { get; }

        public abstract bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList attributes);

        public bool Validate(ChatModule instance, CommandAttribute command, AttributeTypedList mapping)
        {
            ValidateMapping(command, mapping);
            bool valid = OnValidate(instance, command, mapping);
            if (!valid)
            {
                ErrorMessage = ReplaceInErrorMessage(ErrorMessage, command, mapping);
            }

            return valid;
        }

        protected T GetFieldValue<T>(ChatModule invoker, string fieldName)
        {
            FieldInfo[] fields = invoker.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.NonPublic);

            T output = default;
            try
            {
                output = (T)fields.Single(p => p.Name == fieldName).GetValue(invoker);
            }
            catch
            {
                try
                {
                    output = (T)fields.Single(p => p.Name == fieldName).GetValue(null);
                }
                catch
                {
                    ErrorMessage = $"Could not find field {fieldName}";
                }
            }

            return output;
        }

        protected bool ExistsFieldValue(ChatModule invoker, string fieldName)
        {
            return invoker.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Static | BindingFlags.NonPublic).Count(c => c.Name == fieldName) > 0;
        }

        protected void Log(object message) => UnityEngine.Debug.LogWarning(GetType() + ": " + message.ToString());

        private string ReplaceInErrorMessage(string errorMessage, CommandAttribute command, List<C_BaseAttribute> attributes)
        {
            foreach (C_BaseAttribute attr in attributes)
            {
                string value = attr.Value.ToString();

                errorMessage = errorMessage.Replace("[" + attr.GetType().Name.Replace("Attribute", "") + "]", value);
                errorMessage = errorMessage.Replace("[" + attr.GetType().Name + "]", value);
            }

            errorMessage = errorMessage.Replace("[C_Amount]", command.Cost.ToString());

            errorMessage = Globals.ReplaceTextWithSettings(errorMessage);

            return errorMessage;
        }

        private void ValidateMapping(CommandAttribute command, List<C_BaseAttribute> mapping)
        {
            foreach (Type attr in RequiredParameterTypes)
            {
                bool found = false;
                foreach (C_BaseAttribute map in mapping)
                {
                    if (map.GetType() == attr)
                    {
                        found = true;
                        continue;
                    }
                }

                if (!found && attr == typeof(C_AmountAttribute))
                {
                    if (command.Cost > 0)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    throw new Exception($"Missing: {attr.Name}. All Required Parameter Types attributes need to be included in the function parameter attributes");
                }
            }
        }
    }
}