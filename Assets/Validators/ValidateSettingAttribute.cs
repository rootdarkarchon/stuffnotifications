﻿using Assets.Base;
using Assets.Helpers;
using Assets.Managers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateSettingAttribute : ValidateAttribute
    {
        public ValidateSettingAttribute() => ErrorMessage = $"[C_UserSelf] the setting [C_Setting] does not exist.";

        public override List<Type> RequiredParameterTypes => new List<Type> { typeof(C_SettingAttribute), typeof(C_ModuleAttribute) };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            C_SettingAttribute attr = mapping.C_Setting;

            if (attr.AllowEmpty && string.IsNullOrEmpty(attr.StringValue))
            {
                return true;
            }

            return SettingsManager.GetSettingSpecific<string>(mapping.C_Module.StringValue + "." + attr.StringValue) != default;
        }
    }
}