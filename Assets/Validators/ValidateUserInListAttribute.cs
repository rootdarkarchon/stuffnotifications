﻿using Assets.Base;
using Assets.Helpers;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateAttributeInTwitchDbListAttribute : ValidateAttribute
    {
        public string ListName;
        public Type UserTypeAttribute;
        public bool ShouldBeInList;

        public ValidateAttributeInTwitchDbListAttribute(string listName, Type attributeValue, bool shouldBeInList)
        {
            ListName = listName;
            UserTypeAttribute = attributeValue;
            ShouldBeInList = shouldBeInList;
            ErrorMessage = "";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { UserTypeAttribute };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            TwitchDBUser user = mapping.Get(UserTypeAttribute).TwitchDBUserValue;

            List<TwitchDBUser> list = GetFieldValue<List<TwitchDBUser>>(invoker, ListName);

            return list.Any(f => f == user) == ShouldBeInList;
        }
    }
}