﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateAttributeAgainstLongValueAttribute : ValidateAttribute
    {
        public long MaxValue;
        public Type AttributeType;

        public ValidateAttributeAgainstLongValueAttribute(Type attributeType, long value)
        {
            AttributeType = attributeType;
            MaxValue = value;
            ErrorMessage = "";
        }

        public override List<Type> RequiredParameterTypes => new List<Type> { AttributeType };

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            C_BaseAttribute attr = mapping.Get(AttributeType);

            bool valid = MaxValue >= attr.LongValue;
            if (!valid)
            {
                ErrorMessage = $"{attr.Value} is too high, maximum is {MaxValue}";
            }

            return valid;
        }
    }
}