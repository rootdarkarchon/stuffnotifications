﻿using Assets.Base;
using Assets.Helpers;
using System;
using System.Collections.Generic;

namespace Assets.Validators
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateValueEqualityAttribute : ValidateAttribute
    {
        public object ExpectedState;
        public object ValueToCheck;
        public bool ShouldBeEqual;
        private readonly List<Type> requiredParameters;

        public ValidateValueEqualityAttribute(object value, object expectedState, bool shouldBeEqual = true)
        {
            ValueToCheck = value;
            ExpectedState = expectedState;
            ShouldBeEqual = shouldBeEqual;
            requiredParameters = new List<Type>();
            if (ExpectedState is Type)
            {
                if (((Type)ExpectedState).IsSubclassOf(typeof(C_BaseAttribute)))
                {
                    requiredParameters.Add((Type)ExpectedState);
                }
            }

            if (ValueToCheck is Type)
            {
                if (((Type)ValueToCheck).IsSubclassOf(typeof(C_BaseAttribute)))
                {
                    requiredParameters.Add((Type)ValueToCheck);
                }
            }
        }

        public override List<Type> RequiredParameterTypes => requiredParameters;

        public override bool OnValidate(ChatModule invoker, CommandAttribute command, AttributeTypedList mapping)
        {
            object fieldValue = null;

            if (ValueToCheck is string)
            {
                fieldValue = GetFieldValue<object>(invoker, (string)ValueToCheck);
                if (fieldValue == default && !ExistsFieldValue(invoker, (string)ValueToCheck))
                {
                    fieldValue = ValueToCheck;
                }
            }
            else if (ValueToCheck is Type)
            {
                if (((Type)ValueToCheck).IsSubclassOf(typeof(C_BaseAttribute)))
                {
                    fieldValue = mapping.Get((Type)ValueToCheck).StringValue.ToLower();
                }
            }

            if (ExpectedState is Type)
            {
                if (((Type)ExpectedState).IsSubclassOf(typeof(C_BaseAttribute)))
                {
                    ExpectedState = mapping.Get((Type)ExpectedState).Value;
                    if (ValueToCheck is Type)
                    {
                        ExpectedState = ExpectedState.ToString().ToLower();
                    }
                }
            }

            if (fieldValue == null && ExpectedState == null)
            {
                return ShouldBeEqual;
            }

            return fieldValue.Equals(ExpectedState) == ShouldBeEqual;
        }
    }
}