﻿using System;
using UnityEngine;

namespace Assets.Helpers
{
    // todo: refactor this

    [Serializable]
    public class SubTableHelper
    {
        public const float MaxCountSubs1000 = 13;
        public const float MaxCountSubs2000 = 3;
        public const float MaxCountSubs3000 = 2;

        public SubTableHelper()
        {
        }

        public SubTableHelper(GameObject table)
        {
            this.Table = table;
        }

        public float CurIncX1000 { get; set; } = 0f;
        public float CurIncX2000 { get; set; } = 0f;
        public float CurIncX3000 { get; set; } = 0f;
        public float CurIncZ1000 { get; set; } = 0f;
        public float CurIncZ2000 { get; set; } = 0f;
        public float CurIncZ3000 { get; set; } = 0f;
        public int CurSubs1000 { get; set; } = 0;
        public int CurSubs2000 { get; set; } = 0;
        public int CurSubs3000 { get; set; } = 0;
        public GameObject Table { get; set; }

        public int GetCurSubsInRow(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return CurSubs1000;

                case "2000":
                    return CurSubs2000;

                case "3000":
                    return CurSubs3000;

                default:
                    return 0;
            }
        }

        public Vector3 GetNextPosition(string subLevel)
        {
            var output = new Vector3(GetCurX(subLevel) - GetCurIncX(subLevel),
                GetCurY(), GetCurZ(subLevel) + GetCurIncZ(subLevel));

            IncCurSubs(subLevel);
            SetCurIncX(subLevel, GetCurIncX(subLevel) + GetIncTot(subLevel));
            if (GetCurSubsInRow(subLevel) >= GetMaxSubs(subLevel))
            {
                SetCurIncX(subLevel, 0);
                SetCurIncZ(subLevel, GetCurIncZ(subLevel) + GetIncTot(subLevel));
                ResetCurSubsInRow(subLevel);
            }

            return output;
        }

        public void IncCurSubs(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    CurSubs1000++;
                    break;

                case "2000":
                    CurSubs2000++;
                    break;

                case "3000":
                    CurSubs3000++;
                    break;
            }
        }

        public void ResetCurSubsInRow(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    CurSubs1000 = 0;
                    break;

                case "2000":
                    CurSubs2000 = 0;
                    break;

                case "3000":
                    CurSubs3000 = 0;
                    break;
            }
        }

        private float GetCurIncX(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return CurIncX1000;

                case "2000":
                    return CurIncX2000;

                case "3000":
                    return CurIncX3000;

                default:
                    return float.NaN;
            }
        }

        private float GetCurIncZ(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return CurIncZ1000;

                case "2000":
                    return CurIncZ2000;

                case "3000":
                    return CurIncZ3000;

                default:
                    return float.NaN;
            }
        }

        private float GetCurX(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return Table.transform.position.x + 1.4f;

                case "2000":
                    return Table.transform.position.x + 6.8f;

                case "3000":
                    return Table.transform.position.x + 6.5f;

                default:
                    return float.NaN;
            }
        }

        private float GetCurY()
        {
            return Table.transform.position.y + 4f;
        }

        private float GetCurZ(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return Table.transform.position.z - 2.5f;

                case "2000":
                    return Table.transform.position.z + 0.5f;

                case "3000":
                    return Table.transform.position.z - 2f;

                default:
                    return float.NaN;
            }
        }

        private float GetIncTot(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return 0.9f;

                case "2000":
                    return 1.8f;

                case "3000":
                    return 3f;

                default:
                    return float.NaN;
            }
        }

        private float GetMaxSubs(string subLevel)
        {
            switch (subLevel)
            {
                case "1000":
                    return MaxCountSubs1000;

                case "2000":
                    return MaxCountSubs2000;

                case "3000":
                    return MaxCountSubs3000;

                default:
                    return float.NaN;
            }
        }

        private void SetCurIncX(string subLevel, float value)
        {
            switch (subLevel)
            {
                case "1000":
                    CurIncX1000 = value;
                    break;

                case "2000":
                    CurIncX2000 = value;
                    break;

                case "3000":
                    CurIncX3000 = value;
                    break;
            }
        }

        private void SetCurIncZ(string subLevel, float value)
        {
            switch (subLevel)
            {
                case "1000":
                    CurIncZ1000 = value;
                    break;

                case "2000":
                    CurIncZ2000 = value;
                    break;

                case "3000":
                    CurIncZ3000 = value;
                    break;
            }
        }
    }
}