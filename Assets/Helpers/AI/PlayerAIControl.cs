﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Assets.Base;
using Assets.Models;
using UnityEngine.AI;
using System.Threading.Tasks;
using Asyncoroutine;

namespace Assets.Helpers.AI
{
    public class PlayerAIControl : MonoBehaviourSettings
    {
        public List<PlayerMovementHelper> Positions;
        public GameObject PlayerPrefab;
        public DefenseGameLogic WaveSpawner;
        public IntroUITextHelper UITextHelper;

        [Setting("PLAYER_KILLTIMEFRAME", 1f)]
        public float S_PlayerKillTimeFrame;
        [Setting("PLAYER_KILLSPERTIMEFRAME", 8)]
        public int S_PlayerKillsPerTimeFrame;
        [Setting("PLAYER_SHOTDISTANCE", 125)]
        public int S_PlayerShotDistance;

        // Start is called before the first frame update
        public override async Task StartWithSettings()
        {
            await new WaitForMainThread();

            Positions = FindObjectsOfType<PlayerMovementHelper>().ToList();

            await WaveSpawner.WaitForInit();
        }

        public PlayerAI SpawnPlayer(TwitchDBUser player, int playerNumber)
        {
            var playerobj = Instantiate(PlayerPrefab);

            IEnumerable<PlayerMovementHelper> targetPositions = Positions
                .Where(p => p.DefenseDirectionNuance == DefenseDirectionNuance.Center && !p.Occupied).ToList();

            Color color = new Color(1, 0, 0);
            string spokenColor = "Red";
            if (playerNumber == 2)
            {
                color = new Color(0, 1, 0);
                spokenColor = "Green";
            }
            else if (playerNumber == 3)
            {
                color = new Color(0, 0, 1);
                spokenColor = "Blue";
            }
            else if (playerNumber == 4)
            {
                color = new Color(0, 1, 1);
                spokenColor = "Cyan";
            }

            playerobj.name = player.Username;

            playerobj.GetComponent<PlayerAI>()
                .Spawn(targetPositions.First(), color, playerNumber, S_PlayerShotDistance, S_PlayerKillTimeFrame, S_PlayerKillsPerTimeFrame);

            var targetPos = targetPositions.First();
            playerobj.transform.position = targetPos.transform.position;
            NavMesh.SamplePosition(playerobj.transform.position, out NavMeshHit closestHit, 500, 1);
            playerobj.transform.position = closestHit.position;

            playerobj.transform.SetParent(targetPos.transform);

            playerobj.GetComponent<NavMeshAgent>().enabled = true;
            playerobj.GetComponent<PlayerAI>().enabled = true;
            playerobj.GetComponent<PlayerAI>().SpokenColor = spokenColor;

            Task.Run(() => UITextHelper.DisplayUserText(playerobj));

            return playerobj.GetComponent<PlayerAI>();
        }

        public async Task MovePlayerDestination(PlayerAI player, DefenseDirection targetDirection)
        {
            IEnumerable<PlayerMovementHelper> targetPositions = Positions.Where(p => p.DefenseDirection == targetDirection && !p.Occupied).ToList();
            if (!Positions.Any() || player.CurrentPosition.DefenseDirection == targetDirection)
            {
                return;
            }

            var centerDirection = targetPositions.SingleOrDefault(p => p.Occupied == false && p.DefenseDirectionNuance == DefenseDirectionNuance.Center);

            if (centerDirection != null)
            {
                await player.MoveToDefensePoint(centerDirection);
                return;
            }

            PlayerMovementHelper preferredTarget =
                targetPositions.FirstOrDefault(p => p.DefenseDirectionNuance != player.CurrentPosition.DefenseDirectionNuance);

            if (targetDirection == DefenseDirection.North && player.CurrentPosition.DefenseDirection == DefenseDirection.East)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left);
            }

            if (targetDirection == DefenseDirection.North && player.CurrentPosition.DefenseDirection == DefenseDirection.West)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right);
            }

            if (targetDirection == DefenseDirection.East && player.CurrentPosition.DefenseDirection == DefenseDirection.South)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left);
            }

            if (targetDirection == DefenseDirection.East && player.CurrentPosition.DefenseDirection == DefenseDirection.North)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right);
            }

            if (targetDirection == DefenseDirection.South && player.CurrentPosition.DefenseDirection == DefenseDirection.West)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left);
            }

            if (targetDirection == DefenseDirection.South && player.CurrentPosition.DefenseDirection == DefenseDirection.East)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right);
            }

            if (targetDirection == DefenseDirection.West && player.CurrentPosition.DefenseDirection == DefenseDirection.North)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left);
            }

            if (targetDirection == DefenseDirection.West && player.CurrentPosition.DefenseDirection == DefenseDirection.South)
            {
                preferredTarget = targetPositions.SingleOrDefault(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Left)
                    ?? targetPositions.Single(t => t.DefenseDirectionNuance == DefenseDirectionNuance.Right);
            }

            if (preferredTarget == null)
            {
                preferredTarget = targetPositions.First();
            }

            await player.MoveToDefensePoint(preferredTarget);
        }
    }
}