﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Assets.Base;
using Assets.Models;
using Asyncoroutine;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Assets.Helpers.AI
{
    public partial class DefenseGameLogic : MonoBehaviourSettings
    {
        public GameObject EnemyPrefab;
        public Transform NorthSpawn;
        public Transform SouthSpawn;
        public Transform EastSpawn;
        public Transform WestSpawn;

        public TextMeshProUGUI RemainingCurrencyText;

        public StreamBaseDefenseModule DefenseModule;

        public bool DefenseStarted;
        public GameObject DefenseCountDown;
        public int PlayerCount;

        public DefenseCameraControl CameraControl;

        public int TimeRemaining;

        public int CurrentRound;

        public List<DirectionWaveHelper> WaveHelpers;

        [Setting("MIN_ENEMIES", 20)]
        public int S_MinimumEnemiesPerWaveBase = 20;
        [Setting("MAX_ENEMIES", 60)]
        public int S_MaximumEnemiesPerWaveBase = 60;
        [Setting("BASE_MULTIPLIER", 1.125f)]
        public float S_BaseMultiplier = 1.125f;
        [Setting("MAX_ENEMIES_MULTIPLIER", 1.5f)]
        public float S_MaxEnemiesMultiplier = 1.5f;
        [Setting("MAX_WAVE_INC", 8)]
        public float S_PerWaveIncreaseMax = 8;
        [Setting("MIN_WAVE_INC", 6)]
        public float S_PerWaveIncreaseMin = 6;
        [Setting("ENEMY_PER_CURRENCY", 1.5)]
        public double S_EnemyPerCurrencyMultiplier = 1.5;
        [Setting("WAVE_COOLDOWN", 3)]
        public float S_CooldownBetweenWaves = 3f;

        private long remainingCurrencyToDefend;
        public long RemainingCurrencyToDefend
        {
            get
            {
                return remainingCurrencyToDefend;
            }
            set
            {
                remainingCurrencyToDefend = value;
                if (remainingCurrencyToDefend <= 0) remainingCurrencyToDefend = 0;

                _ = Task.Run(() => SetRemainingCurrency(remainingCurrencyToDefend));
            }
        }

        private string remainderString;
        public string RemainderString
        {
            get
            {
                return remainderString;
            }
            set
            {
                remainderString = value;
                RemainingCurrencyText.text = remainderString;
            }
        }

        private async Task SetRemainingCurrency(long currencyToDefend)
        {
            await new WaitForMainThread();
            if (currencyToDefend > 0)
            {
                RemainderString = $"{currencyToDefend}";
            }
            else
            {
                RemainderString = "";
            }
        }

        private string startText;
        private Vector3 DefenseCountDownStartPosition;

        public string StartText
        {
            get
            {
                return startText;
            }
            set
            {
                startText = value;
                GameObject.Find("RoundAnnouncement").GetComponent<Text>().text = startText;
            }
        }

        public async Task StartCountdown(int length, CancellationTokenSource ct, bool showGo = true)
        {
            TimeRemaining = length;

            await new WaitForMainThread();
            DefenseCountDown.transform.position = DefenseCountDownStartPosition;

            while (TimeRemaining > 0)
            {
                DefenseCountDown.GetComponent<Text>().text = TimeRemaining.ToString();

                DefenseCountDown.transform.position = DefenseCountDown.transform.position + (Vector3.down * 200);

                int timeStep = (int)(Time.deltaTime * 1000);

                for (int i = 0; i < 500; i += timeStep)
                {
                    DefenseCountDown.transform.position = DefenseCountDown.transform.position + (Vector3.up * 10);
                    if (DefenseCountDown.transform.position.y >= DefenseCountDownStartPosition.y)
                    {
                        await Task.Delay(500 - i);
                        break;
                    }

                    timeStep = (int)(Time.deltaTime * 1000);

                    await Task.Delay(timeStep);
                }

                for (int i = 0; i < 300; i += timeStep)
                {
                    DefenseCountDown.transform.position = DefenseCountDown.transform.position + (Vector3.right * 10 * i / 50.0f);
                    timeStep = (int)(Time.deltaTime * 1000);
                    await Task.Delay(timeStep);
                }

                DefenseCountDown.transform.position = DefenseCountDownStartPosition;
                TimeRemaining--;

                if (ct != null && ct.IsCancellationRequested)
                {
                    return;
                }
            }

            if (showGo)
            {
                DefenseCountDown.GetComponent<Text>().text = "GO!";
                await Task.Delay(3000);
            }

            DefenseCountDown.GetComponent<Text>().text = "";
        }

        private decimal RoundPercentage(int round)
        {
            switch (round)
            {
                case 1: return 0.05M;
                case 2: return 0.10M;
                case 3: return 0.175M;
                case 4: return 0.275M;
                case 5: return 0.4M;
                default: return 0M;
            }
        }

        public override async Task StartWithSettings()
        {
            RemainingCurrencyToDefend = -1;
            DefenseStarted = false;
            DefenseCountDownStartPosition = DefenseCountDown.transform.position;
        }

        public async Task SpinCamera()
        {
            await CameraControl.RotateCamera();
        }

        public void StartDefense()
        {
            if (DefenseStarted) return;

            DefenseStarted = true;
            WaveHelpers = GetComponents<DirectionWaveHelper>().ToList();

            Task.Run(() => WaveSpawnerLogic());
        }

        public async Task SetRoundText(string text, int duration = 3)
        {
            await new WaitForMainThread();
            StartText = text;
            await Task.Delay(TimeSpan.FromSeconds(duration));
            StartText = "";
        }

        public async Task WaveSpawnerLogic()
        {
            int maximumEnemies = 0;
            int maxEnemiesPerWave = 0;
            int minEnemiesPerWave = 0;
            CurrentRound = 1;

            // calculator here
            // https://docs.google.com/spreadsheets/d/18r-f_UEVuiyJvxFfTC9xbNht8U9kPWwKMvyOt0Dioh8/edit#gid=0

            DefenseDirection lastDirection = DefenseDirection.East;

            while (CurrentRound <= 5)
            {
                while (WaveHelpers.Sum(f => f.SpawnedEnemies) != 0)
                {
                    await Task.Delay(1000);
                }

                // get the amount of enemies for this current round
                int remainingEnemies = (int)Math.Floor(RoundPercentage(CurrentRound) * RemainingCurrencyToDefend * (decimal)S_EnemyPerCurrencyMultiplier);
                Debug.Log($"NEW ROUND: {CurrentRound}, total enemy count {remainingEnemies}");

                minEnemiesPerWave = (int)Math.Ceiling((S_BaseMultiplier * (S_MinimumEnemiesPerWaveBase + CurrentRound * S_PerWaveIncreaseMin * (PlayerCount / 2.0))));
                maximumEnemies = (int)Math.Ceiling(((PlayerCount * S_MaxEnemiesMultiplier) * (S_MaximumEnemiesPerWaveBase + CurrentRound * S_PerWaveIncreaseMax)));
                maxEnemiesPerWave = (int)Math.Ceiling((Math.Min(PlayerCount, 3) * S_BaseMultiplier * (S_MaximumEnemiesPerWaveBase + CurrentRound * S_PerWaveIncreaseMax)));

                _ = SetRoundText($"Round {CurrentRound}{Environment.NewLine}Enemies: {remainingEnemies}", 5);

                await StartCountdown(5, null, CurrentRound == 1);

                while (remainingEnemies > 0)
                {
                    if (RemainingCurrencyToDefend == 0)
                    {
                        await new WaitForMainThread();
                        _ = SetRoundText("You lost!");
                        _ = DefenseModule.DefenseEnd(false);
                        return;
                    }

                    // wait for a second
                    await Task.Delay(TimeSpan.FromSeconds(S_CooldownBetweenWaves));
                    System.Random random = new System.Random(DateTime.Now.Millisecond);

                    // wait until all wave spawners ethat are not the last direction are done with spawning enemies
                    // and the sum of total enemies on the battlefield is less than minimum
                    // todo: I am not sure if this makes sense, I think the check for total enemies here is unnecessary
                    //       because it is done further down the line for the check, I will disable it for now for testing
                    var directionalWavesExceptLast = WaveHelpers.Where(e => e.AttackVector != lastDirection && !e.SpawningWave);
                    while (!directionalWavesExceptLast.Any(f => f.WaveDone))
                    {
                        await Task.Delay(1000);
                    }

                    // enemies that can still be spawned
                    // (absolute maximum players can defend in total subtracted by the amount currently on the field)
                    // ignoring enemies carrying bananas
                    var allowedEnemies = maximumEnemies - WaveHelpers.Sum(f => f.AttackWaves.Sum(e => e.SpawnedEnemies.Count(x => x.CarriesBanana)));

                    // the maximum amount of enemies per wave, either enemies that can still be spawned or the absolute maximum
                    var allowedMaximumEnemiesPerWave = allowedEnemies > maxEnemiesPerWave ? maxEnemiesPerWave : allowedEnemies;

                    // if maximum enemies per wave is smaller than remaining enemies in round, take remaining enemies
                    int actualMaximumEnemies = allowedMaximumEnemiesPerWave <= remainingEnemies ? allowedMaximumEnemiesPerWave : remainingEnemies;

                    // if minimum enemies per wave is larger than the maximum enemies per wave take that as maximum
                    int actualMinimumEnemies = minEnemiesPerWave > actualMaximumEnemies ? actualMaximumEnemies : minEnemiesPerWave;

                    // so if maximum enemies per wave is smaller than minimum enemies don't spawn anything.
                    // minimum enemies could also be the remaining enemies
                    // this is to prevent tiny wave spawning and wave sizes will be at least minEnemiesPerWave or the remainder of the round in size
                    if (allowedMaximumEnemiesPerWave < (minEnemiesPerWave > remainingEnemies ? remainingEnemies : minEnemiesPerWave))
                    {
                        // if we have less enemies per wave allowed than minimum enemies then wait for some seconds before checking for next wave spawn
                        //yield return new WaitForSeconds(random.Next((int)(S_CooldownBetweenWaves * 2), (int)(S_CooldownBetweenWaves * 3)));
                        await Task.Delay(TimeSpan.FromSeconds(random.Next((int)(S_CooldownBetweenWaves * 2), (int)(S_CooldownBetweenWaves * 3))));
                        continue;
                    }

                    // get a random direction of the wavespawner that is currently been defended
                    // however ignore the last direction the enemies came from
                    // todo: prioritize directions where currently no players are defending towards
                    var waveDirectionHelper =
                        directionalWavesExceptLast.Where(f => f.WaveDone && !f.SpawningWave)
                        .ToArray()[random.Next(directionalWavesExceptLast.Count(w => w.WaveDone && !w.SpawningWave))];

                    // select a random amount of enemies which between REMAINDER/MINENEMIES to MAXPERWAVE/DIFF(MAX-MAXPERWAVE)/REMAINDER
                    // and spawn them, save the last direction 
                    int toSpawn = random.Next(actualMinimumEnemies, actualMaximumEnemies);
                    remainingEnemies -= toSpawn;
                    _ = Task.Run(() => SpawnWave(toSpawn, waveDirectionHelper.AttackVector));
                    Debug.Log($"Spawning {toSpawn}, remaining: {remainingEnemies}");
                    lastDirection = waveDirectionHelper.AttackVector;
                }

                CurrentRound++;
            }

            while (!WaveHelpers.All(f => f.SpawnedEnemies == 0))
            {
                await Task.Delay(500);
            }

            _ = SetRoundText("You win!", 10);
            _ = DefenseModule.DefenseEnd(true);
        }

        public List<EnemyAI> GetEnemies(DefenseDirection defenseDirection)
        {
            return WaveHelpers.SingleOrDefault(f => f.AttackVector == defenseDirection)?
                .AttackWaves.SelectMany(e => e.SpawnedEnemies).Where(e => e.NavMeshAgent != null).ToList();
        }

        public void DebugSpawnWave(DefenseDirection defenseDirection)
        {
            _ = Task.Run(() => SpawnWave(50, defenseDirection));
        }

        public async Task SpawnWave(long amount, DefenseDirection attackVector, TwitchDBUser user = null)
        {
            await new WaitForMainThread();

            var helper = WaveHelpers.Single(e => e.AttackVector == attackVector);

            Transform spawnPoint = helper.SpawnPoint;

            AttackWave wave = new AttackWave();
            wave.User = user;

            List<EnemyAI> enemies = new List<EnemyAI>();

            System.Random r = new System.Random();
            Vector3 randomizer;
            if (attackVector == DefenseDirection.South || attackVector == DefenseDirection.North)
                randomizer = Vector3.forward;
            else
                randomizer = Vector3.right;
            for (int i = 0; i < amount; i++)
            {
                GameObject enemy = Instantiate(EnemyPrefab);
                enemy.name = $"Enemy #{i} {attackVector}";
                enemy.transform.position = spawnPoint.position + Vector3.up * 2 + randomizer * r.Next(-100, 100);
                NavMesh.SamplePosition(enemy.transform.position, out NavMeshHit closestHit, 500, 1);
                enemy.transform.position = closestHit.position + Vector3.up * 4;

                enemy.transform.SetParent(spawnPoint);

                enemy.AddComponent<NavMeshAgent>();
                var agent = enemy.GetComponent<NavMeshAgent>();
                agent.speed = 75;
                agent.angularSpeed = 300;
                agent.acceleration = 500;
                agent.autoBraking = false;
                agent.autoTraverseOffMeshLink = true;
                agent.autoRepath = false;
                var ai = enemy.GetComponent<EnemyAI>();

                ai.StartPosition = closestHit.position;
                ai.CenterTarget = GameObject.Find("CenterBananaBox");
                ai.CenterTarget.transform.position += Vector3.forward * 0.00000000001f;
                ai.Wave = wave;
                ai.DefenseLogic = this;
                ai.NavMeshAgent = agent;
                wave.TotalEnemies++;
                wave.SpawnedEnemies.Add(ai);
            }

            if (user != null)
            {
                _ = Task.Run(() => DefenseModule.AwardUserForWave(wave));
            }

            WaveHelpers.Single(f => f.AttackVector == attackVector).AttackWaves.Add(wave);
        }
    }
}
