﻿using Assets.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers.AI
{
    public class LightsEnabler : MonoBehaviour
    {
        public Light LightComponent;

        public DefenseGameLogic WaveSpawner;

        public bool LightsOn = false;

        public DefenseDirection LightsDirection;

        public bool IsFading;

        public float LightsStartIntensity;

        void Start()
        {
            LightComponent = GetComponent<Light>();
            LightsStartIntensity = LightComponent.intensity;
            LightComponent.intensity = 0;
        }

        void Update()
        {
            if (WaveSpawner.WaveHelpers.Count == 0) return;
            if (WaveSpawner.WaveHelpers.Single(f => f.AttackVector == LightsDirection).AttackWaves.Sum(e => e.SpawnedEnemies.Count()) == 0 && LightsOn)
            {
                StartCoroutine(FadeLight(true));
            }
            else if (WaveSpawner.WaveHelpers.Single(f => f.AttackVector == LightsDirection).SpawningWave && !LightsOn)
            {
                StartCoroutine(FadeLight(false));
            }
        }

        public IEnumerator FadeLight(bool fadeOut)
        {
            if (IsFading) yield break;

            IsFading = true;
            float targetIntensity = fadeOut ? 0 : LightsStartIntensity;

            LightsOn = !fadeOut;

            while ((fadeOut && LightComponent.intensity > targetIntensity) || (!fadeOut && LightComponent.intensity < targetIntensity))
            {
                yield return new WaitForSeconds(0.05f);
                LightComponent.intensity += fadeOut ? -(LightsStartIntensity / 25) : (LightsStartIntensity / 25);
            }
            IsFading = false;
        }
    }
}
