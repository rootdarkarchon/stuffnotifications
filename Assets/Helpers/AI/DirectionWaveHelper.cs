﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Base;
using Assets.Models;
using Asyncoroutine;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Helpers.AI
{
    [Serializable]
    public class AttackWave
    {
        public List<EnemyAI> SpawnedEnemies;
        public bool IsSpawned;
        public int TotalEnemies;
        public TwitchDBUser User;
        public int StolenBananas;

        public AttackWave()
        {
            SpawnedEnemies = new List<EnemyAI>();
        }

        public bool WaveDone => SpawnedEnemies.Count() <= (TotalEnemies / 2)
            || SpawnedEnemies.Count(e => e.CarriesBanana) >= SpawnedEnemies.Count() / 2;

        public bool WaveFullyCompleted => SpawnedEnemies.Count() == 0;

        public void CarriedHome(EnemyAI ai)
        {
            SpawnedEnemies.Remove(ai);
            StolenBananas++;
        }

        public async Task Spawn()
        {
            IsSpawned = true;

            int i = 0;

            await new WaitForMainThread();

            foreach (var enemy in new List<EnemyAI>(SpawnedEnemies.Where(e => !e.enabled)))
            {
                enemy.enabled = true;
                if (i % 20 == 0)
                {
                    await Task.Delay(TimeSpan.FromSeconds(0.5));
                }
                i++;
            }

            // make sure nothing is left
            if (SpawnedEnemies.Any(e => !e.enabled))
            {
                foreach (var enemy in new List<EnemyAI>(SpawnedEnemies.Where(e => !e.enabled)))
                {
                    enemy.enabled = true;
                    if (i % 20 == 0)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(0.5));
                    }
                    i++;
                }
            }
        }
    }

    public class DirectionWaveHelper : MonoBehaviour
    {
        public DirectionWaveHelper()
        {
        }

        public DefenseDirection AttackVector;
        public Color CanvasColor;
        public bool SpawningWave;
        public Text CanvasText;
        public List<AttackWave> AttackWaves;
        public Transform SpawnPoint;

        public int SpawnedEnemies => AttackWaves.Sum(e => e.SpawnedEnemies.Count());

        public bool WaveDone => AttackWaves.All(e => e.WaveDone);

        public void RecalculateTextColor()
        {
            if (AttackWaves.Where(e => !e.IsSpawned).Sum(e => e.SpawnedEnemies.Count()) <= 100)
            {
                CanvasColor = Color.green;
            }
            else if (AttackWaves.Where(e => !e.IsSpawned).Sum(e => e.SpawnedEnemies.Count()) <= 200)
            {
                CanvasColor = Color.yellow;
            }
            else
            {
                CanvasColor = Color.red;
            }
        }

        public void Start()
        {
            AttackWaves = new List<AttackWave>();
        }

        public void Update()
        {
            if (AttackWaves.Any(e => !e.IsSpawned) && !SpawningWave)
            {
                _ = Task.Run(() => SpawnWave(AttackWaves.First(e => !e.IsSpawned)));
            }
        }

        private async Task SpawnWave(AttackWave wave)
        {
            RecalculateTextColor();
            await BlinkArrowAndEnableEnemies(wave);
        }

        private async Task BlinkArrowAndEnableEnemies(AttackWave wave)
        {
            SpawningWave = true;

            _ = Task.Run(() => BlinkCanvasText());

            await Task.Delay(TimeSpan.FromSeconds(5));

            await wave.Spawn();
        }

        public async Task BlinkCanvasText()
        {
            await new WaitForMainThread();

            for (int i = 0; i < 10; i++)
            {
                int enemyCount = AttackWaves.Where(w => !w.IsSpawned).Sum(e => e.TotalEnemies) % 100;
                CanvasText.fontStyle = FontStyle.Normal;
                if (enemyCount < 33)
                {
                    CanvasText.text = "↓";
                }
                else if (enemyCount < 66)
                {
                    CanvasText.text = "↓↓";
                }
                else
                {
                    CanvasText.text = "↓↓↓";
                }

                if (AttackWaves.Where(w => !w.IsSpawned).Sum(e => e.TotalEnemies) > 300)
                {
                    CanvasText.fontStyle = FontStyle.Bold;
                    CanvasText.text = "! ! !";
                }

                CanvasText.color = CanvasColor;
                CanvasText.enabled = true;
                await Task.Delay(500);

                CanvasText.enabled = false;
                await Task.Delay(500);
            }

            SpawningWave = false;
        }
    }
}