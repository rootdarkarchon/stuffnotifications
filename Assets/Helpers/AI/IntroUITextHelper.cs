﻿using Assets.Base;
using Asyncoroutine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Assets.Helpers.AI
{
    public class IntroUITextHelper : MonoBehaviour
    {
        public TextMeshProUGUI Text;
        public TextMeshProUGUI SayingText;
        public Vector3 OriginalPositionText;
        public Vector3 OriginalPositionSaying;

        public DefenseCameraControl CameraControl;

        public bool Displaying;
        public List<GameObject> PlayerListQueue;

        public List<string> Sayings;

        public CancellationTokenSource AbortTokenSource;

        // Start is called before the first frame update
        void Start()
        {
            AbortTokenSource = new CancellationTokenSource();
            Sayings = new List<string> {
                "FUCKING MOVE ALREADY",
                "Kicking cylinders' butt since 1785",
                "The Master of Blaster",
                "ZipZaWoop",
                "Power Overwhelming",
                "PDF! PDF! PDF!",
                "You wouldn't believe the things I've seen",
                "Ironically destructive",
                "What's a team?",
                "Uhh guys, how do I aim this thing?",
                "The Weak Link",
                "All your Potassium are belong to us",
                "Wake up Mr. Nana, wake up and smell the ashes",
                "Actually a Handicap",
                "Doesn't really like lasers",
                "Those are some very ripe bananas",
                "Anyway, I started blastin'",
                "Are we there yet?",
                "Ready for pegging action",
                "Are we the baddies?",
                "I'm here to kick gum and chew ass",
                "This is OUR defense",
                "Where is the raytracing?",
                "Needs more godrays",
                "Good guys finish last",
                "Acktschuahly bananas are fruit",
                "Not as cool as Minty",
                "Lurks harder than Jonathan",
                "The Chin [lite]",
                "Will probably be stationary",
                "I am the Chat",
                "All show, no substenance",
                "Slapping the enemy harder than Davie504 the bass",
                "Designated Driver",
                "Knows their cardinal directions",
                "Pro-Streamer",
                "Here for the cats",
                "NANA phone home",
                "Show me the nanas",
                "I'll be back",
                "I see dead cylinders",
                "Here's Johnny!",
                "!gamble all",
                "!vote 31",
                "*Screams in Wilhelm*",
                "May the farce be with you",
                "More exciting than ArmA",
                "This Game > Stream, don't @ me",
                "Just here for the !communism",
                "Get Pegged",
                "uwu"
            };
            OriginalPositionText = Text.transform.localPosition;
            OriginalPositionSaying = SayingText.transform.localPosition;
            PlayerListQueue = new List<GameObject>();
        }

        public async Task DisplayUserText(GameObject newUser)
        {
            PlayerListQueue.Add(newUser);
            if (Displaying)
            {
                return;
            }

            Displaying = true;

            await new WaitForMainThread();

            var uiTexts = GameObject.Find("PlayerStats").GetComponentsInChildren<TextMeshProUGUI>().ToList();
            uiTexts.ForEach(f => f.enabled = false);
            GameObject.Find("RemainingCurrency").GetComponent<TextMeshProUGUI>().enabled = false;

            var randomizedSayingsListCopy = new List<string>(Sayings);
            randomizedSayingsListCopy = randomizedSayingsListCopy.OrderBy(e => Guid.NewGuid()).ToList();


            while (PlayerListQueue.Any())
            {
                var user = PlayerListQueue.First();
                PlayerListQueue.Remove(user);

                await CameraControl.Animate(user.GetComponent<PlayerAI>().CurrentPosition.DefenseDirection, user);

                Displaying = true;
                Text.text = user.name;

                var saying = randomizedSayingsListCopy.First();
                var color = user.GetComponent<PlayerAI>().Color;
                SayingText.text = saying;
                SayingText.faceColor = new Color32((byte)(color.r * (byte)255), (byte)(color.g * (byte)255), (byte)(color.b * (byte)255), (byte)(color.a * (byte)255));
                Text.faceColor = new Color32((byte)(color.r * (byte)255), (byte)(color.g * (byte)255), (byte)(color.b * (byte)255), (byte)(color.a * (byte)255));
                float delay = 0f;

                while (delay < 4f)
                {
                    if (AbortTokenSource.IsCancellationRequested)
                    {
                        ResetTextAndCamera(true);
                        return;
                    }
                    await Task.Delay(15);
                    Text.transform.position += Vector3.right * 0.002f + Vector3.forward * 0.001f + Vector3.down * 0.0015f;
                    SayingText.transform.position += Vector3.left * 0.0005f + Vector3.back * 0.00125f + Vector3.up * 0.0005f;
                    delay += 0.015f;
                }

                ResetTextAndCamera();

                await Task.Delay(500);

                randomizedSayingsListCopy.Remove(saying);
            }

            uiTexts.ForEach(f => f.enabled = true);
            GameObject.Find("RemainingCurrency").GetComponent<TextMeshProUGUI>().enabled = true;
            Displaying = false;
        }

        public void ResetTextAndCamera(bool fullReset = false)
        {
            CameraControl.ResetCamera();

            AbortTokenSource.Cancel();
            AbortTokenSource.Dispose();
            AbortTokenSource = new CancellationTokenSource();

            Text.text = "";
            SayingText.text = "";

            Text.transform.localPosition = OriginalPositionText;
            SayingText.transform.localPosition = OriginalPositionSaying;

            if (fullReset)
            {
                Displaying = false;
                PlayerListQueue.Clear();
            }
        }
    }
}