﻿using Assets.Base;
using Asyncoroutine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Assets.Helpers.AI
{
    public class PlayerAI : MonoBehaviour
    {
        public NavMeshAgent Agent;
        public Color Color;
        public PlayerMovementHelper CurrentPosition;
        public float LaserIntensity = 2f;
        public Material LaserMaterial;
        public int PlayerNumber;
        public float S_ShotDistance;
        public string SpokenColor;
        public int Stat_BananaSaves;
        public int Stat_TotalMoves;
        public bool T_ShootingDone = true;
        public TextMeshProUGUI UI_KillsText;
        public TextMeshProUGUI UI_NameText;
        public DefenseGameLogic WaveSpawner;
        private bool AfterInitMoving;
        private int S_KillsPerTimeFrame;
        private float S_ShotTimeFrame;
        private int stat_Kills;
        private double TimeSinceLastShot;
        private int LastKills;
        CancellationTokenSource MoveCancelSource;

        public bool PlayerStoppedMoving
        {
            get
            {
                return (Agent.transform.position.y > 5 || Agent.remainingDistance < 1f) && !AfterInitMoving;
            }
        }

        public int Stat_Kills
        {
            get => stat_Kills;
            set
            {
                stat_Kills = value;
                UI_KillsText.text = $"{stat_Kills}";
            }
        }

        public void Spawn(PlayerMovementHelper position, Color color, int playerNumber, int shotDistance, float shotTimeFrame, int killsPerTimeFrame)
        {
            S_ShotTimeFrame = shotTimeFrame;
            S_KillsPerTimeFrame = killsPerTimeFrame;
            CurrentPosition = position;
            CurrentPosition.Occupied = true;
            Color = color;
            S_ShotDistance = shotDistance;
            Agent.Warp(CurrentPosition.transform.position);
            GetComponent<Renderer>().material.color = color;
            GetComponent<Renderer>().material.SetColor("_EmissionColor", color * 2.5f);
            LaserMaterial = Instantiate(LaserMaterial);
            LaserMaterial.color = color;
            LaserIntensity = 10f;
            LaserMaterial.SetColor("_EmissionColor", color * LaserIntensity);
            PlayerNumber = playerNumber;
            UI_NameText = GameObject.Find($"Player{playerNumber}Name").GetComponent<TextMeshProUGUI>();
            UI_NameText.faceColor = color;
            UI_KillsText = GameObject.Find($"Player{playerNumber}Kills").GetComponent<TextMeshProUGUI>();
            UI_KillsText.faceColor = color;
            T_ShootingDone = true;
            Stat_Kills = 0;
            UI_NameText.text = transform.gameObject.name;
        }

        public void Start()
        {
            Agent.radius = 0.05f;
            Agent.avoidancePriority = 0;
            MoveCancelSource = new CancellationTokenSource();
        }

        public void Update()
        {
            TimeSinceLastShot += Time.deltaTime;
            if (T_ShootingDone && PlayerStoppedMoving && (TimeSinceLastShot > 0.1 || LastKills > 0) && WaveSpawner != null)
            {
                TimeSinceLastShot = 0;
                Task.Run(() => FireInDirection(CurrentPosition.DefenseDirection, MoveCancelSource.Token));
            }
            else
            {
                LastKills = 0;
            }
        }

        internal async Task MoveToDefensePoint(PlayerMovementHelper newDefensePoint)
        {
            MoveCancelSource.Cancel();
            MoveCancelSource.Dispose();
            MoveCancelSource = new CancellationTokenSource();

            CurrentPosition.Occupied = false;
            newDefensePoint.Occupied = true;
            CurrentPosition = newDefensePoint;
            Stat_TotalMoves++;
            AfterInitMoving = true;

            transform.SetParent(newDefensePoint.transform);

            Agent.SetDestination(CurrentPosition.transform.position);

            while (!Agent.hasPath)
            {
                await Task.Delay(15);
            }

            _ = Task.Run(async () =>
              {
                  await Task.Delay(1000);
                  AfterInitMoving = false;
                  await newDefensePoint.IndicateMovement(this);
              });
        }

        internal async Task ShotFired(EnemyAI enemyToShootAt, Vector3 shotPosition)
        {
            await new WaitForMainThread();

            GameObject laser = new GameObject();
            var enemyPos = enemyToShootAt.transform.position;
            laser.name = "LaserShot";
            laser.transform.SetParent(gameObject.transform);
            laser.AddComponent<LineRenderer>();
            laser.layer = 11;

            var rend = laser.GetComponent<LineRenderer>();
            rend.material = LaserMaterial;
            rend.SetPositions(new Vector3[] { shotPosition, shotPosition });

            var shotLength = 30f;
            var totalLength = Vector3.Distance(shotPosition, enemyPos);

            for (float i = 0; i < 1; i += 0.1f)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(2 * totalLength / shotLength));
                rend.SetPosition(0, Vector3.Lerp(shotPosition, enemyPos, i));
                rend.SetPosition(1, Vector3.Lerp(shotPosition, enemyPos, i + (shotLength / totalLength)));
            }
            var normalized = (enemyToShootAt.transform.position - shotPosition).normalized;
            _ = Task.Run(() => enemyToShootAt.GetHit(normalized));
            Destroy(laser);
        }

        private async Task FireInDirection(DefenseDirection defenseDirection, CancellationToken token)
        {
            T_ShootingDone = false;
            LastKills = 0;
            List<EnemyAI> enemiesFromDirection;

            if (WaveSpawner == null)
            {
                T_ShootingDone = true;
                return;
            }

            await new WaitForMainThread();

            var height = GetComponent<MeshRenderer>().bounds.size.y / 3f;

            var shotPosition = transform.position + Vector3.up * 3 * height;

            enemiesFromDirection = WaveSpawner.GetEnemies(defenseDirection)?
                .Where(e => Vector3.Distance(shotPosition, e.transform.position) < S_ShotDistance * 1.25)
                .Where(e => Physics.Raycast(shotPosition, (e.transform.position - shotPosition).normalized, out var hit, S_ShotDistance)
                && hit.transform.gameObject == e.transform.gameObject && !e.Killed).ToList();

            if (enemiesFromDirection == null)
            {
                T_ShootingDone = true;
                return;
            }

            // prioritize carriers
            var enemyToShootAt = enemiesFromDirection.Where(e => e.CarriesBanana)
                .OrderBy(e => Vector3.Distance(e.transform.position, e.StartPosition)).Take(S_KillsPerTimeFrame).ToList();

            // if less carries in hit range than allowed to kill in timeframe, add them
            if (enemyToShootAt?.Count() < S_KillsPerTimeFrame)
            {
                enemyToShootAt.AddRange(enemiesFromDirection
                    .OrderBy(e => Vector3.Distance(e.transform.position, transform.position)).Take(S_KillsPerTimeFrame - enemyToShootAt.Count()));

                // if still not a single enemy is available to be shot at, cancel
                // todo: is this ever true?
                // --> probably never the case
                if (enemyToShootAt.Count() == 0)
                {
                    T_ShootingDone = true;
                    return;
                }
            }
            else if (enemyToShootAt == null)
            {
                T_ShootingDone = true;
                return;
            }

            foreach (var enemy in enemyToShootAt)
            {
                if (token.IsCancellationRequested)
                {
                    T_ShootingDone = true;
                    return;
                }

                if (enemy.Killed) continue;
                enemy.Killed = true;
                if (enemy.CarriesBanana) Stat_BananaSaves++;

                var shotposition = transform.position + Vector3.up * 3 * height;
                _ = Task.Run(() => ShotFired(enemy, shotposition));
                Stat_Kills++;
                LastKills++;
                await Task.Delay(TimeSpan.FromSeconds(S_ShotTimeFrame / S_KillsPerTimeFrame));
            }

            T_ShootingDone = true;
        }
    }
}