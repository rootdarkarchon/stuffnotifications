﻿using UnityEngine;
using Assets.Base;
using UnityEngine.UI;
using System.Threading.Tasks;
using Asyncoroutine;

namespace Assets.Helpers.AI
{
    public class PlayerMovementHelper : MonoBehaviour
    {
        public DefenseDirection DefenseDirection;
        public DefenseDirectionNuance DefenseDirectionNuance;
        public bool Occupied;

        public async Task IndicateMovement(PlayerAI player)
        {
            await new WaitForMainThread();

            var text = GetComponentInChildren<Text>();

            await Task.Delay(500);

            text.color = player.Color;

            int blinks = 0;

            while (Occupied && blinks < 5)
            {
                blinks++;
                text.enabled = true;
                await Task.Delay(500);
                text.enabled = false;
                await Task.Delay(500);
            }
        }
    }
}