﻿using Assets.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Asyncoroutine;
using System.Threading.Tasks;
using System.Threading;

public class DefenseCameraControl : MonoBehaviour
{
    public Vector3 StartPosition;
    public Quaternion StartRotation;

    public Transform EastSpawn;
    public Transform NorthSpawn;
    public Transform SouthSpawn;
    public Transform WestSpawn;

    public Transform CenterLookAtTransform;

    public Vector3 EndLookPoint;

    public Vector3 GoToPosition;

    public Vector3 velocity;
    public float angularvelocity;
    public float timeCount;

    CancellationTokenSource CameraMovementTokenSource;


    // Start is called before the first frame update
    void Start()
    {
        CenterLookAtTransform = GameObject.Find("CenterBananaBox").transform;
        StartPosition = transform.position;
        StartRotation = transform.rotation;
        CameraMovementTokenSource = new CancellationTokenSource();
    }

    public async Task ZoomCamera(CancellationToken ct)
    {
        while (true)
        {
            if (ct.IsCancellationRequested) return;
            float moveSpeed = 300f;
            transform.position = Vector3.SmoothDamp(transform.position, GoToPosition, ref velocity, 0.05f, moveSpeed);
            if (velocity.y <= 0.1f && Math.Abs(transform.position.y - GoToPosition.y) < 1f)
            {
                var endrotation = Quaternion.LookRotation(EndLookPoint - transform.position);
                var delta = Quaternion.Angle(transform.rotation, endrotation);
                if (delta > 0.0f)
                {
                    float rotSmoothTime = 0.1f;
                    var t = Mathf.SmoothDampAngle(delta, 0.0f, ref angularvelocity, rotSmoothTime);
                    var timeCount = 1.0f - t / delta;
                    transform.rotation = Quaternion.Slerp(transform.rotation, endrotation, timeCount);
                }
                else
                {
                    break;
                }
            }

            await new WaitForEndOfFrame();
        }
    }

    public async Task RotateCamera(CancellationToken ct, int totalTime)
    {
        float nextRotation = 0.16f;
        var totalRotation = 0.0f;
        double currentTime = 0.0f;

        await new WaitForMainThread();

        System.Random rnd = new System.Random();


        //Vector3 rotationDir = random == 0 ? Vector3.up : Vector3.up * -1;

        while (true)
        {
            if (ct.IsCancellationRequested) return;

            // shake rotation
            var rotationPart = 0.25f;
            var shakeRotation = 0.0f; ;

            for (int i = 1; i <= 12; i++)
            {
                float toRotate = rotationPart * i * ((i % 2 == 0) ? -1 : 1);
                shakeRotation += toRotate;
                transform.RotateAround(CenterLookAtTransform.position, Vector3.up, toRotate);
                await Task.Delay(40);
            }

            transform.RotateAround(CenterLookAtTransform.position, Vector3.up, -shakeRotation);

            int amountToRotate = rnd.Next(3, 6) * 45 * ((rnd.Next(0, 2) % 2 == 0) ? -1 : 1);

            // actual rotation
            do
            {
                transform.RotateAround(CenterLookAtTransform.position, Vector3.up, nextRotation);

                totalRotation += nextRotation;

                nextRotation = amountToRotate / (0.25f * (1 / Time.deltaTime));

                if (Math.Abs(nextRotation + totalRotation) > Math.Abs(amountToRotate))
                {
                    nextRotation = Math.Abs(amountToRotate) - Math.Abs(totalRotation);
                    nextRotation *= totalRotation < 0 ? -1 : 1;
                }

                await new WaitForEndOfFrame();
            } while (Math.Abs(totalRotation) < Math.Abs(amountToRotate));

            totalRotation = 0;
            nextRotation = 0;

            var delayTime = TimeSpan.FromSeconds(rnd.Next(4, 6));
            await Task.Delay(delayTime);
            currentTime += delayTime.TotalSeconds;

            if (currentTime > totalTime)
            {
                ResetCamera();
            }
        }
    }

    public void ResetCamera()
    {
        CameraMovementTokenSource.Cancel();
        CameraMovementTokenSource.Dispose();

        transform.position = StartPosition;
        transform.rotation = StartRotation;
        CameraMovementTokenSource = new CancellationTokenSource();
    }

    public async Task Animate(DefenseDirection direction, GameObject player)
    {
        EndLookPoint = player.transform.position + Vector3.up * 5;

        switch (direction)
        {
            case DefenseDirection.East:
                GoToPosition = EastSpawn.transform.position;
                break;
            case DefenseDirection.West:
                GoToPosition = WestSpawn.transform.position;
                break;
            case DefenseDirection.North:
                GoToPosition = NorthSpawn.transform.position;
                break;
            case DefenseDirection.South:
                GoToPosition = SouthSpawn.transform.position;
                break;
        }

        await ZoomCamera(CameraMovementTokenSource.Token);
    }

    public async Task RotateCamera(int totalTime = 30)
    {
        await RotateCamera(CameraMovementTokenSource.Token, totalTime);
    }
}
