﻿using Assets.Base;
using Asyncoroutine;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Helpers.AI
{
    public class EnemyAI : MonoBehaviour
    {
        public GameObject CenterTarget;
        public Vector3 StartPosition;
        public NavMeshAgent NavMeshAgent;
        public DefenseGameLogic DefenseLogic;
        public AttackWave Wave;
        public bool CarriesBanana;
        public float Ticker;
        public Material CarrierMaterial;
        public Material DeadMaterial;
        public bool Killed = false;
        public float DistanceToStart;
        public Vector3 Destination;

        public void Start()
        {
            NavMeshAgent.destination = CenterTarget.transform.position;
            Destination = CenterTarget.transform.position;
        }

        public void Update()
        {
            if (NavMeshAgent == null)
            {
                return;
            }

            Ticker += Time.deltaTime;

            if (!Killed && !NavMeshAgent.hasPath && ((int)Ticker % 2) == 0)
            {
                NavMeshAgent.destination = Destination;
            }

            if (Ticker > 3 && CarriesBanana)
            {
                NavMeshAgent.avoidancePriority = 50;
            }

            DistanceToStart = Vector3.Distance(transform.position, StartPosition);

            if (CarriesBanana && DistanceToStart < 20)
            {
                Wave.CarriedHome(this);
                Destroy(gameObject);
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.name == "CenterBananaBox" && NavMeshAgent != null && NavMeshAgent.destination != StartPosition)
            {
                CarriesBanana = true;
                Ticker = 0;
                Destination = StartPosition;
                NavMeshAgent.avoidancePriority = 10;
                NavMeshAgent.destination = StartPosition;
                NavMeshAgent.radius = 0.1f;
                NavMeshAgent.speed = 50;
                gameObject.GetComponent<Renderer>().material = CarrierMaterial;
                DefenseLogic.RemainingCurrencyToDefend--;
            }
        }

        internal async Task GetHit(Vector3 normalized)
        {
            await new WaitForMainThread();
            Wave.SpawnedEnemies.Remove(this);
            Destroy(NavMeshAgent);
            if (CarriesBanana)
            {
                DefenseLogic.RemainingCurrencyToDefend++;
                CarriesBanana = false;
            }
            gameObject.GetComponent<Renderer>().material = DeadMaterial;
            gameObject.layer = 12;
            var body = GetComponent<Rigidbody>();
            body.centerOfMass += Vector3.down * (GetComponent<MeshRenderer>().bounds.size.y / 2f);
            body.isKinematic = false;
            body.useGravity = true;
            body.drag = 0.001f;
            body.angularDrag = 0.001f;
            body.mass = 100000;
            body.AddForce(normalized * 10, ForceMode.Acceleration);
            await Task.Delay(TimeSpan.FromSeconds(2));
            body.isKinematic = true;

            float time = 0;
            while (time < 2)
            {
                time += Time.deltaTime;
                try
                {
                    body.transform.position = new Vector3(body.transform.position.x, body.transform.position.y - 0.75f, body.transform.position.z);
                }
                catch { }

                await Task.Delay(TimeSpan.FromSeconds(Time.deltaTime));
            }

            Destroy(gameObject);
        }
    }
}
