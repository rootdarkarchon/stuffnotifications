﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeRandomizer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var trees = GetComponentsInChildren<MeshRenderer>();
        foreach(var tree in trees)
        {
            tree.gameObject.transform.localScale = tree.gameObject.transform.localScale * Random.Range(0.6f, 1.4f);
            tree.gameObject.transform.Rotate(Vector3.forward, Random.Range(0f, 90f));
            tree.gameObject.transform.Rotate(Vector3.up, Random.Range(-10f, 10f));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
