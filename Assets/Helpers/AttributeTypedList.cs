﻿using Assets.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Helpers
{
    public class AttributeTypedList : List<C_BaseAttribute>
    {
        public AttributeTypedList() : base()
        {
        }

        public C_AmountAttribute C_Amount => (C_AmountAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_AmountAttribute));
        public C_UserSelfAttribute C_UserSelf => (C_UserSelfAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_UserSelfAttribute));
        public C_UserOtherAttribute C_UserOther => (C_UserOtherAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_UserOtherAttribute));
        public C_CounterAttribute C_Counter => (C_CounterAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_CounterAttribute));
        public C_SoundEffectAttribute C_SoundEffect => (C_SoundEffectAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_SoundEffectAttribute));
        public C_CommandAttribute C_Command => (C_CommandAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_CommandAttribute));
        public C_SettingAttribute C_Setting => (C_SettingAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_SettingAttribute));
        public C_ModuleAttribute C_Module => (C_ModuleAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_ModuleAttribute));
        public C_SettingValueAttribute C_SettingValue => (C_SettingValueAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_SettingValueAttribute));
        public C_TimeAttribute C_Time => (C_TimeAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_TimeAttribute));
        public C_FreeTextAttribute C_FreeText => (C_FreeTextAttribute)this.SingleOrDefault(f => f.GetType() == typeof(C_FreeTextAttribute));

        public C_BaseAttribute Get(Type t)
        {
            return this.SingleOrDefault(f => f.GetType() == t);
        }
    }
}