﻿using Assets.Base;
using Assets.Managers;
using Assets.Models;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers
{
    public class CoinGenerator : MonoBehaviourSettings
    {
        public GameObject CoinPrefab;
        public GameObject BitsCoinPrefab;

        [Setting("COIN_XY_SIZE", 12.5f)]
        public float S_CoinXYSize;

        [Setting("COIN_Z_SIZE", 1.0f)]
        public float S_CoinZSize = 1.0f;

        public GameObject CreateBitsCoin(BitEvent bitEvent)
        {
            GameObject coin = Instantiate(BitsCoinPrefab);
            coin.name = "BitsCoin" + bitEvent.Emote.Prefix;

            Destroy(coin.GetComponent<CapsuleCollider>());

            coin.AddComponent<Rigidbody>();
            Rigidbody rigidBody = coin.GetComponent<Rigidbody>();
            rigidBody.mass = 100000;
            rigidBody.useGravity = true;
            rigidBody.drag = 0.001f;
            rigidBody.angularDrag = 0.001f;
            rigidBody.collisionDetectionMode = CollisionDetectionMode.Continuous;

            Renderer rend = coin.GetComponent<Renderer>();
            rend.material = Resources.Load<Material>("Material/CoinMat");
            var data = (rend.material.mainTexture as Texture2D).EncodeToPNG();

            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));

            var baseTexture = (Bitmap)tc.ConvertFrom(data);
            var emoteTexture = (Bitmap)tc.ConvertFrom(bitEvent.Image);

            var outputDrawing = System.Drawing.Graphics.FromImage(baseTexture);

            int TexH = baseTexture.Height;
            int TexW = baseTexture.Width;
            int Border = 2;

            outputDrawing.DrawImage(emoteTexture,
                ((TexW/2)/3)/2,
                TexH / 2 + ((TexH / 2) / 3) / 6,
                (TexW / 3) - (2 * Border),
                (TexH / 3) - (2 * Border));
            emoteTexture.RotateFlip(RotateFlipType.RotateNoneFlipX);
            outputDrawing.DrawImage(emoteTexture,
                TexW / 2 + Border + ((TexW / 2) / 3) / 2,
                TexH / 2 + Border + ((TexH / 2) / 3) / 6,
                (TexW / 3) - (2 * Border),
                (TexH / 3) - (2 * Border));

            outputDrawing.Save();

            using (var ms = new MemoryStream())
            {
                var tex = new Texture2D(TexH, TexW);
                baseTexture.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                tex.LoadImage(ms.ToArray());
                rend.material.mainTexture = tex;
            }

            Globals.SetMeshRendererVisibilityForGameObject(coin, true);

            return coin;
        }

        public GameObject CreateCoinWithTexture(TwitchUser follower, float sizeMultiplier = 1.0f)
        {
            var mainTex = new Texture2D(256, 256);
            var emissionTex = new Texture2D(256, 256);

            GameObject userPill = CreateBaseCoin(sizeMultiplier);

            byte[] mainTexture = Globals.ReadAllImageBytes(follower.MainTexture);
            mainTex.LoadImage(mainTexture);

            byte[] emissionTexture = Globals.ReadAllImageBytes(follower.EmissionTexture);
            emissionTex.LoadImage(emissionTexture);

            Renderer rend = userPill.GetComponent<Renderer>();
            rend.material = new Material(Shader.Find("Standard"));
            rend.material.SetTexture("_MainTex", mainTex);

            rend.material.mainTexture.wrapMode = TextureWrapMode.MirrorOnce;
            if (follower.IsSub)
            {
                rend.material.SetTexture("_EmissionMap", emissionTex);
                rend.material.SetColor("_EmissionColor", new UnityEngine.Color(203, 243, 0));
                rend.material.EnableKeyword("_EMISSION");
                DynamicGI.SetEmissive(rend, new UnityEngine.Color(203, 243, 0));
            }
            rend.material.enableInstancing = true;

            Resources.UnloadUnusedAssets();

            return userPill;
        }

        private GameObject CreateBaseCoin(float sizeMultiplier = 1.0f)
        {
            GameObject userPill = Instantiate(CoinPrefab);

            userPill.AddComponent<MeshCollider>();
            userPill.GetComponent<MeshCollider>().convex = true;

            userPill.name = "UserPill";

            Destroy(userPill.GetComponent<CapsuleCollider>());

            userPill.transform.localScale = new Vector3(S_CoinXYSize * sizeMultiplier,
                S_CoinXYSize * sizeMultiplier,
                S_CoinZSize * sizeMultiplier);

            userPill.AddComponent<Rigidbody>();
            Rigidbody rigidBody = userPill.GetComponent<Rigidbody>();
            rigidBody.mass = 1000;
            rigidBody.useGravity = true;
            rigidBody.drag = 1;
            rigidBody.angularDrag = 0.01f;
            rigidBody.collisionDetectionMode = CollisionDetectionMode.Continuous;

            Globals.SetMeshRendererVisibilityForGameObject(userPill, true);

            return userPill;
        }
    }
}