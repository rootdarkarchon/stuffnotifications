﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers
{
    class ColliderTimer : MonoBehaviour
    {
        Vector3 bounds;
        public void Start()
        {
            bounds = transform.GetComponent<MeshCollider>().sharedMesh.bounds.size;
            StartCoroutine(RemoveColliderOnInactivity());
        }

        private IEnumerator RemoveColliderOnInactivity()
        {
            yield return new WaitForSeconds(2);
            while (true)
            {
                yield return new WaitForSeconds(1);

                var topCenter = transform.position + transform.up * bounds.x / 2;
                var botCenter = transform.position - transform.up * bounds.x / 2;
                var rightCenter = transform.position + transform.right * bounds.x / 2;
                var leftCenter = transform.position - transform.right * bounds.x / 2;

                if (Physics.Raycast(transform.position, Vector3.up, out var hit, 0.5f)
                && Physics.Raycast(topCenter, Vector3.up, out var hit1, 0.5f)
                && Physics.Raycast(botCenter, Vector3.up, out var hit2, 0.5f)
                && Physics.Raycast(rightCenter, Vector3.up, out var hit3, 0.5f)
                && Physics.Raycast(leftCenter, Vector3.up, out var hit4, 0.5f)
                )
                {
                    if (hit.collider.gameObject.GetComponent<ColliderTimer>() != null
                        && hit1.collider.gameObject.GetComponent<ColliderTimer>() != null
                        && hit2.collider.gameObject.GetComponent<ColliderTimer>() != null
                        && hit3.collider.gameObject.GetComponent<ColliderTimer>() != null
                        && hit4.collider.gameObject.GetComponent<ColliderTimer>() != null
                        )
                    {
                        Destroy(transform.gameObject);
                        Destroy(this);
                        enabled = false;
                    }
                }
            }
        }
    }
}
