﻿using Assets.Managers;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Helpers
{
    public class UserCoinCollideHelper : MonoBehaviour
    {
        public List<AudioClip> CollideSounds;

        public bool Collided = false;

        private void Start()
        {
            CollideSounds = new List<AudioClip>();
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/chipsCollide1"));
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/chipsCollide2"));
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/chipsCollide3"));
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/chipsCollide4"));
        }

        private void OnCollisionEnter(Collision collision)
        {
            Rigidbody body;
            if (TryGetComponent(out body))
            {
                if (body.velocity.magnitude > 0.3)
                {
                    Collided = true;
                    GetComponent<AudioSource>().PlayOneShot(CollideSounds[new System.Random().Next(CollideSounds.Count)]);
                }
            }
        }
    }
}