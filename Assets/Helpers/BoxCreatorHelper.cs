﻿using Assets.Base;
using Assets.Managers;
using Assets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers
{
    public class BoxCreatorHelper : MonoBehaviourSettings
    {
        public TwitchManager TwitchManager;
        public CoinGenerator CoinGenerator;
        public TableCreatorHelper TableCreatorHelper;

        private List<GameObject> objectsInBox;
        private float start = 0.0f;

        private bool createdAllObjects;
        private bool fixedCoins;

        public override async Task StartWithSettings()
        {
            createdAllObjects = false;
            objectsInBox = new List<GameObject>();

            await TwitchManager.WaitForInit();

            while (!TwitchManager.FinishedDownloadingFollowers)
            {
                await Task.Delay(100);
            }

            GenerateFollowerCoins();
        }

        public Transform GetParentTransform => transform;

        private void FixedUpdate()
        {
            if (fixedCoins)
            {
                return;
            }

            if (createdAllObjects && !fixedCoins)
            {
                start += Time.deltaTime;
            }
            if (start > 5 && !fixedCoins)
            {
                if (objectsInBox.Count == 0)
                {
                    fixedCoins = true;
                    return;
                }

                float highestCoin = GetHighestCoin();

                foreach (GameObject coinObject in GetObjectsInBoxClone())
                {
                    if (coinObject.TryGetComponent(out Rigidbody body))
                    {
                        if (coinObject.transform.position.y > highestCoin - 0.15)
                        {
                            body.drag = 1000;
                            body.angularDrag = 1000;
                            body.velocity = Vector3.zero;
                            body.angularVelocity = Vector3.zero;
                        }
                        else
                        {
                            Destroy(body);
                        }

                        if (coinObject.transform.position.y < -0.2)
                        {
                            RemoveFromBox(coinObject);
                        }
                    }
                }

                fixedCoins = true;
            }
        }

        public void SetVisibilityForBoxObjects(bool isVisible)
        {
            GetComponent<MeshRenderer>().enabled = isVisible;
            foreach (GameObject coinObject in objectsInBox)
            {
                coinObject.GetComponent<MeshRenderer>().enabled = isVisible;
            }
        }

        public void GenerateFollowerCoins()
        {
            Vector3 boxPos = transform.position;
            float curX = boxPos.x - 0.7f;
            float curZ = boxPos.z + 0.6f;
            float curY = 1.5f;
            float curIncX = 0.0f;
            float curIncZ = 0.0f;
            float incTot = 0.3f;

            // get list of non subscribed followers
            var nonSubscribedFollowers = TwitchManager.Followers.Where(f => !f.IsSub).ToList();
            nonSubscribedFollowers.Reverse();

            var nonSubscribedFirstFollowers = nonSubscribedFollowers.Take(nonSubscribedFollowers.Count - TwitchManager.Followers.Count(f => f.IsSub)).ToList();
            var nonSubscribedLastFollowers = nonSubscribedFollowers.Where(c => nonSubscribedFirstFollowers.All(f => f.DisplayName != c.DisplayName)).ToList();

            Log("Sub count: " + TwitchManager.Followers.Count(f => f.IsSub) + ", NonSubFirstFollow: " + nonSubscribedFirstFollowers.Count() + ", NonSubLastFollow: " + nonSubscribedLastFollowers.Count());

            //Master.GuiInitText = "Spawning followers";

            // spawn the first non subscribed followers as the bottom layer
            foreach (TwitchUser follower in nonSubscribedFirstFollowers)
            {
                try
                {
                    follower.CoinObject = CreateBoxFollowerCoin(curX, curZ, ref curY, ref curIncX, ref curIncZ, incTot, follower);
                    objectsInBox.Add(follower.CoinObject);
                }
                catch
                {
                    TwitchManager.Followers.Remove(follower);
                    TwitchManager.AllUsers.Remove(follower);
                }
            }

            // randomize order of last followers and subs
            IEnumerable<TwitchUser> lastFollowersAndSubs = Globals.InterleaveEnumerationsOfEqualLength(
                TwitchManager.Followers.Where(f => f.IsSub).OrderBy(o => Guid.NewGuid()),
                nonSubscribedLastFollowers.OrderBy(o => Guid.NewGuid())
                );

            // spawn last followers and subs on top of the stack
            foreach (TwitchUser follower in lastFollowersAndSubs)
            {
                //Debug.Log("Creating coin for " + follower.DebugString());
                follower.CoinObject = CreateBoxFollowerCoin(curX, curZ, ref curY, ref curIncX, ref curIncZ, incTot, follower);
                objectsInBox.Add(follower.CoinObject);
            }

            createdAllObjects = true;
        }

        private GameObject CreateBoxFollowerCoin(float curX, float curZ, ref float curY, ref float curIncX, ref float curIncZ, float incTot, TwitchUser follower)
        {
            GameObject userPill = CoinGenerator.CreateCoinWithTexture(follower);
            userPill.name = follower.DebugString();

            userPill.transform.position = new Vector3((float)(curX + curIncX), curY, curZ + curIncZ);
            curIncX += incTot;
            if (curIncX > 1.2f)
            {
                curIncX = 0.0f;
                curIncZ -= incTot;
            }

            if (curIncZ < -1.2f)
            {
                curIncZ = 0.0f;
                curY += incTot;
            }

            Rigidbody rigidBody = userPill.GetComponent<Rigidbody>();
            rigidBody.angularVelocity = new Vector3(0.05f, 0.01f, 0.05f);
            rigidBody.velocity = new Vector3(0.00f, -0.05f, 0.00f);

            userPill.transform.rotation = new Quaternion(
                UnityEngine.Random.Range(0f, 90f), UnityEngine.Random.Range(0f, 90f),
                UnityEngine.Random.Range(0f, 90f), UnityEngine.Random.Range(0f, 90f));
            return userPill;
        }

        internal void RemoveFromBox(GameObject coinObject)
        {
            objectsInBox.Remove(coinObject);
            Destroy(coinObject);
        }

        internal float GetHighestCoin()
        {
            return objectsInBox.Max(c => c.transform.position.y);
        }

        public List<GameObject> GetObjectsInBoxClone()
        {
            return new List<GameObject>(objectsInBox);
        }
    }
}