﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Helpers
{
    public class BitCoinCollideHelper : MonoBehaviour
    {
        public List<AudioClip> CollideSounds;

        public GameObject BananaBox;
        public GameObject RubleBox;
        public GameObject CollidedWith;

        public float BoxY;

        public bool Collided = false;
        private float start;
        private float playedSoundFloat;
        private float collisionCooldown;
        public bool PlayedSound;
        System.Random random = new System.Random(DateTime.Now.Millisecond);
        private void Start()
        {
            CollideSounds = new List<AudioClip>();
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/coin1"));
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/coin2"));
            CollideSounds.Add(Resources.Load<AudioClip>("Sounds/coin3"));
        }

        public IEnumerator EnableCollision()
        {
            yield return new WaitForSeconds(0.5f);

            var body = GetComponent<Rigidbody>();

            foreach (var c in body.gameObject.GetComponentsInChildren<MeshCollider>())
            {
                c.enabled = true;
            }

            yield return new WaitForSeconds(0.5f);

            body.collisionDetectionMode = CollisionDetectionMode.Discrete;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (Collided) return;
            if (collision.gameObject.Equals(BananaBox))
            {
                BananaBox.GetComponent<BoxCounter>().Count++;
                Collided = true;
                CollidedWith = BananaBox;
            }

            if (collision.gameObject.Equals(RubleBox))
            {
                RubleBox.GetComponent<BoxCounter>().Count++;
                Collided = true;
                CollidedWith = RubleBox;
            }

            if (collision.transform.parent != null)
            {
                if (collision.transform.parent.gameObject.Equals(RubleBox))
                {
                    RubleBox.GetComponent<BoxCounter>().Count++;
                    Collided = true;
                    CollidedWith = RubleBox;
                }

                if (collision.transform.parent.gameObject.Equals(BananaBox))
                {
                    BananaBox.GetComponent<BoxCounter>().Count++;
                    Collided = true;
                    CollidedWith = BananaBox;
                }
            }

            if (!collision.gameObject.TryGetComponent<BitCoinCollideHelper>(out var collidehelper))
            {
                if (collision.relativeVelocity.magnitude > 7 && !PlayedSound)
                {
                    GetComponent<AudioSource>().PlayOneShot(CollideSounds[random.Next(CollideSounds.Count)], 0.15f);
                    PlayedSound = true;
                }
            }
            else
            {
                if (collidehelper.CollidedWith == null) return;

                CollidedWith = collidehelper.CollidedWith;
                CollidedWith.GetComponent<BoxCounter>().Count++;
                Collided = true;
            }
        }

        public void Update()
        {
            start += Time.deltaTime;
            playedSoundFloat += Time.deltaTime;

            if (playedSoundFloat > 5)
            {
                PlayedSound = false;
                playedSoundFloat = 0;
            }

            if (Collided)
            {
                collisionCooldown += Time.deltaTime;
            }

            var body = GetComponent<Rigidbody>();

            // fell under the box
            if (body.transform.position.y < (BoxY) && start > 5)
            {
                Collided = true;
                Destroy(body.transform.gameObject);
            }

            // object is stuck
            if(body.velocity.magnitude < 1 && start > 15)
            {
                Destroy(body);
                Destroy(GetComponent<AudioSource>());
                enabled = false;
            }

            // collided and had enough time to slide around
            if (Collided && collisionCooldown > 3)
            {
                foreach (var c in body.gameObject.GetComponentsInChildren<MeshCollider>())
                {
                    Destroy(c.material);
                }
            }

            // collided, had enough time to slide around and stopped sliding
            if (Collided && collisionCooldown > 3 && body.velocity.magnitude < 1)
            {
                // slided under the actual box position
                if (body.transform.position.y < (BoxY))
                {
                    Destroy(body.transform.gameObject);
                }
                else
                {
                    // add collidertimer to remove dynamic collider when covered
                    // also attach rigidbody to the box
                    foreach (var c in body.gameObject.GetComponentsInChildren<MeshCollider>())
                    {
                        c.transform.gameObject.name = c.transform.parent.gameObject.name;
                        c.transform.gameObject.AddComponent<ColliderTimer>();
                        c.transform.SetParent(CollidedWith.transform);
                    }

                    // get rid of the rigidbody
                    Destroy(body);
                    Destroy(GetComponent<AudioSource>());
                    enabled = false;
                }
            }
        }
    }
}