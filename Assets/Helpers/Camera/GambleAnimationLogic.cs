﻿using Assets.Helpers;
using Assets.Managers;
using Assets.Models;
using Asyncoroutine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class GambleAnimationLogic : MonoBehaviour
{
    public Vector3 CameraStartPosition;
    private Vector3 Die1StartPosition;
    private Vector3 Die2StartPosition;
    private Vector3 CoinStartPosition;
    private Quaternion CameraStartRotation;
    public List<Gamble> QueuedGambles;
    public Camera GambleCamera;
    public TwitchManager TwitchManager;
    public Globals Globals;

    Task GambleTask;

    public TextMeshProUGUI GambleText;

    public GameObject Die1;
    public GameObject Die2;
    public GameObject Coin;

    public void Start()
    {
        CameraStartPosition = GambleCamera.transform.localPosition;
        CameraStartRotation = GambleCamera.transform.rotation;

        Die1StartPosition = Die1.transform.position;
        Die2StartPosition = Die2.transform.position;
        CoinStartPosition = Coin.transform.position;

        QueuedGambles = new List<Gamble>();

        GambleTask = Task.Run(() => Task.Delay(0));
    }

    public void Update()
    {
        if (QueuedGambles.Any() && GambleTask.IsCompleted)
        {
            var gamble = QueuedGambles.First();
            QueuedGambles.Remove(gamble);
            if (gamble.GambleType == GambleType.Dice)
            {
                GambleTask = DiceGambleAnimation(gamble);
            }
            else if (gamble.GambleType == GambleType.Coin)
            {
                GambleTask = CoinGambleAnimation(gamble);
            }
            Task.Run(() => GambleTask);
        }
    }

    public async Task CoinGambleAnimation(Gamble gamble)
    {
        var userAmount = gamble.User.Currency1;

        await new WaitForMainThread();

        GambleCamera.transform.LookAt(Coin.transform.position + Vector3.down * 0.25f);

        GambleCamera.enabled = true;

        Coin.transform.position = CoinStartPosition;
        Coin.transform.Rotate(UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360), UnityEngine.Random.Range(0, 360));

        var coinbody = Coin.GetComponent<Rigidbody>();
        coinbody.AddForce(Vector3.up * UnityEngine.Random.Range(4, 8), ForceMode.Impulse);
        var MinForce = 500;
        var MaxForce = 1250;
        System.Random rnd = new System.Random();
        float multiplierRight = 1;
        if (rnd.Next(0, 2) == 1) multiplierRight = -1;
        coinbody.AddTorque(Coin.transform.right * multiplierRight * UnityEngine.Random.Range(MinForce, MaxForce), ForceMode.Acceleration);

        Vector3 curSpeed = Vector3.zero;
        while (!coinbody.IsSleeping())
        {
            var newPos = Coin.transform.localPosition + Vector3.up * 0.5f + Vector3.right * 0.5f;
            GambleCamera.transform.localPosition = Vector3.SmoothDamp(GambleCamera.transform.localPosition,
                newPos, ref curSpeed, 0.1f);
            GambleCamera.transform.LookAt(Coin.transform.position + Vector3.down * 0.25f);
            await new WaitForNextFrame();
        }

        long amountWon = GetCoinWin(Coin.transform) ? gamble.Amount : -gamble.Amount;

        if (gamble.Amount == userAmount && amountWon > 0) amountWon = (long)(amountWon * 1.5);

        await ShowText(gamble.User, amountWon);

        await Task.Delay(TimeSpan.FromSeconds(2));

        gamble.User.Currency1 += amountWon;

        SendTwitchChatText(gamble, "gambled", userAmount, amountWon);

        GambleCamera.enabled = false;

        GambleText.text = $"";

        GambleCamera.transform.localPosition = CameraStartPosition;
        GambleCamera.transform.rotation = CameraStartRotation;
    }

    public async Task DiceGambleAnimation(Gamble gamble)
    {
        var userAmount = gamble.User.Currency1;

        await new WaitForMainThread();

        GambleCamera.enabled = true;

        Die1.transform.position = Die1StartPosition;
        Die2.transform.position = Die2StartPosition;
        var MinForce = 0;
        var Force = 5;
        Die1.GetComponent<Rigidbody>().AddTorque(UnityEngine.Random.Range(MinForce, Force), UnityEngine.Random.Range(MinForce, Force), UnityEngine.Random.Range(MinForce, Force));
        Die2.GetComponent<Rigidbody>().AddTorque(UnityEngine.Random.Range(MinForce, Force), UnityEngine.Random.Range(MinForce, Force), UnityEngine.Random.Range(MinForce, Force));

        await Task.Delay(TimeSpan.FromSeconds(0.25));

        float down = 0.75f * 2f;

        var newPos = GambleCamera.transform.localPosition + Vector3.down * down + Vector3.right * 0.1f * 2f;
        Vector3 curSpeed = Vector3.zero;
        while (Math.Abs(GambleCamera.transform.localPosition.y - (CameraStartPosition.y - down)) > 0.01)
        {
            GambleCamera.transform.localPosition = Vector3.SmoothDamp(GambleCamera.transform.localPosition, newPos, ref curSpeed, 0.75f);
            await new WaitForNextFrame();
        }

        Rigidbody die1body = Die1.GetComponent<Rigidbody>();
        Rigidbody die2body = Die2.GetComponent<Rigidbody>();

        while (die1body.velocity.magnitude > 0.05
            || die2body.velocity.magnitude > 0.05)
        {
            await Task.Delay(250);
        }

        var die1 = GetDiceNumber(Die1.transform);
        var die2 = GetDiceNumber(Die2.transform);

        long amountWon = die1 == die2 ? gamble.Amount * 3 : -gamble.Amount;

        if (gamble.Amount == userAmount && amountWon > 0) amountWon = (long)(amountWon * 1.5);

        await ShowText(gamble.User, amountWon);

        await Task.Delay(TimeSpan.FromSeconds(2));

        gamble.User.Currency1 += amountWon;

        SendTwitchChatText(gamble, "rolled the dice", userAmount, amountWon);

        GambleCamera.enabled = false;

        GambleText.text = $"";

        GambleCamera.transform.localPosition = CameraStartPosition;
    }

    private void SendTwitchChatText(Gamble gamble, string action, long userAmount, long amountWon)
    {
        string text = $"{gamble.User} {action} ";
        if (gamble.Amount == userAmount) text += $"for ALL of their {Globals.S_Currency1Name} ";
        else text += $"for {gamble.Amount.AsCur1()}, ";
        if (amountWon > 0) text += $"and WON {amountWon.AsCur1()} rootdaToot now has {gamble.User.Currency1.AsCur1()}";
        else if (amountWon < 0 && gamble.Amount == userAmount) text += "and LOST EVERYTHING rootdaArgh";
        else text += $"and LOST {Math.Abs(amountWon).AsCur1()} rootdaArgh now has {gamble.User.Currency1.AsCur1()}";

        TwitchManager.SendChannelMessage($"{text}");
    }

    public async Task ShowText(TwitchDBUser user, long amountWon)
    {
        long previousCurrency = user.Currency1;

        GambleText.text = $"{user}{Environment.NewLine}{amountWon.ToString("+0;-#")}";

        await Task.Delay(TimeSpan.FromSeconds(1));

        int step = 10;
        if (Math.Abs(amountWon) > 1000)
        {
            step = 100;
        }
        if (Math.Abs(amountWon) > 10000)
        {
            step = 1000;
        }
        if (Math.Abs(amountWon) > 100000)
        {
            step = 10000;
        }
        if (Math.Abs(amountWon) > 1000000)
        {
            step = 100000;
        }
        if (Math.Abs(amountWon) > 10000000)
        {
            step = 1000000;
        }

        while (Math.Abs(amountWon) > 0)
        {
            var amountWonStep = Math.Abs(amountWon) > step ? (amountWon > 0 ? step : -step) : amountWon;
            previousCurrency += amountWonStep;
            amountWon -= amountWonStep;
            await Task.Delay(10);
            GambleText.text = $"{user}{Environment.NewLine}{previousCurrency}";
        }
    }

    public bool GetCoinWin(Transform coinTransform)
    {
        var dotWin = Vector3.Dot(Vector3.up, coinTransform.forward);
        var dotFail = Vector3.Dot(Vector3.up, -coinTransform.forward);
        return dotWin >= dotFail;
    }

    public int GetDiceNumber(Transform diceTransform)
    {
        List<Vector3> transformList = new List<Vector3>() { diceTransform.up, -diceTransform.up, diceTransform.right, -diceTransform.right, diceTransform.forward, -diceTransform.forward };
        var upVector = transformList.OrderByDescending(f => Vector3.Dot(Vector3.up, f)).First();

        if (upVector == diceTransform.up) return 4;
        if (upVector == -diceTransform.up) return 3;
        if (upVector == -diceTransform.right) return 2;
        if (upVector == diceTransform.right) return 5;
        if (upVector == -diceTransform.forward) return 6;
        if (upVector == diceTransform.forward) return 1;

        return -1;
    }
}

public enum GambleType
{
    Dice,
    Coin
}


public class Gamble
{
    public TwitchDBUser User;
    public long Amount;
    public GambleType GambleType;
}