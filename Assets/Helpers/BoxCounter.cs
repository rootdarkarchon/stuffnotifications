﻿using Assets.Base;
using Assets.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Helpers
{
    public class BoxCounter : MonoBehaviourSettings
    {
        public int UnityCount;
        public int Currency;
        public Text UIText;
        public MainCamera MainCamera;

        public int Count
        {
            get
            {
                return UnityCount;
            }
            set
            {
                UnityCount = value;

                try
                {
                    if (Currency == 1)
                    {
                        UIText.text = $"{(UnityCount * 10).AsCur1()}";
                    }
                    else
                    {
                        UIText.text = $"{UnityCount.AsCur2()}";
                    }
                }
                catch
                {
                    UIText.text = "";
                }
            }
        }


        public override async Task StartWithSettings()
        {
            //Count = 0;

            UIText = transform.GetComponentInChildren<RectTransform>().GetComponent<Text>();

            UIText.text = "";
        }

        public void WriteText(string text)
        {
            UIText.text = text;
        }
    }
}
