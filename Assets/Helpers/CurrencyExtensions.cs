﻿using Assets.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers
{
    public static class CurrencyExtensions
    {
        public static Globals Globals;

        public static string AsCur1(this long amount)
        {
            return $"{amount} {Globals.S_Currency1Name}";
        }

        public static string AsCur2(this long amount)
        {
            return $"{amount} {Globals.S_Currency2Name}";
        }

        public static string AsCur1(this int amount)
        {
            return $"{amount} {Globals.S_Currency1Name}";
        }

        public static string AsCur2(this int amount)
        {
            return $"{amount} {Globals.S_Currency2Name}";
        }
    }
}
