﻿using Assets.Base;
using Assets.Managers;
using Assets.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Helpers
{
    public class TableCreatorHelper : MonoBehaviourSettings
    {
        public TwitchManager TwitchManager;
        public CoinGenerator CoinGenerator;
        private SubTableHelper SubTableHelper;
        private List<GameObject> coinsOnTable;

        public async override Task StartWithSettings()
        {
            SubTableHelper = new SubTableHelper(gameObject);
            coinsOnTable = new List<GameObject>();

            await TwitchManager.WaitForInit();

            foreach (IGrouping<string, TwitchUser> subGroup in TwitchManager.Subscribers.GroupBy(s => s.SubPlan).OrderByDescending(s => s.Key))
            {
                foreach (TwitchUser sub in subGroup.OrderBy(s => s.CreatedAtDateTime))
                {
                    CreateSubscriberCoin(sub, subGroup.Key);
                }
            }
        }

        public void CreateSubscriberCoin(TwitchUser sub, string subGroup)
        {
            sub.CoinObject = CreateTableSubscriberCoin(sub, subGroup);

            sub.CoinObject.transform.position = SubTableHelper.GetNextPosition(subGroup);

            sub.CoinObject.transform.Rotate(Vector3.left * 2f);
        }

        private GameObject CreateTableSubscriberCoin(TwitchUser sub, string subPlan)
        {
            float sizeMultiplier = GetSizeMultiplier(subPlan);

            GameObject userCoin = CoinGenerator.CreateCoinWithTexture(sub, sizeMultiplier);
            coinsOnTable.Add(userCoin);
            userCoin.name = sub.DebugString();
            return userCoin;
        }

        public float GetSizeMultiplier(string subLevel)
        {
            float sizeMultiplier;
            switch (subLevel)
            {
                case "3000":
                    sizeMultiplier = 9.0f;
                    break;

                case "2000":
                    sizeMultiplier = 6.0f;
                    break;

                case "1000":
                    sizeMultiplier = 3.0f;
                    break;

                default:
                    sizeMultiplier = 3.0f;
                    break;
            }

            return sizeMultiplier;
        }

        public void SetVisibilityForTableObjects(bool isVisible)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = isVisible;
            foreach (GameObject coin in coinsOnTable)
            {
                coin.GetComponent<MeshRenderer>().enabled = isVisible;
            }
        }
    }
}