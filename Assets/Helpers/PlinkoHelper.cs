﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Helpers
{
    public class PlinkoHelper : MonoBehaviour
    {
        public Canvas FrontCanvas;
        public Canvas BackCanvas;

        public int TotalBits;
        public int CurrentBits;

        public Color MinColor;
        public Color MidColor;
        public Color MaxColor;

        public void WriteText(string text)
        {
            FrontCanvas.GetComponent<Text>().text = text;
            BackCanvas.GetComponent<Text>().text = text;
        }

        public void EnableCanvas(bool enable)
        {
            FrontCanvas.enabled = enable;
            BackCanvas.enabled = enable;
        }

        public void Start()
        {
            FrontCanvas.GetComponent<Text>().text = "";
            BackCanvas.GetComponent<Text>().text = "";
        }

        public void Update()
        {
            if (TotalBits == 0) return;
            Color c;
            if (CurrentBits < TotalBits / 2)
            {
                c = Color.Lerp(MinColor, MidColor, ((float)CurrentBits / TotalBits) * 2);

            }
            else
            {
                c = Color.Lerp(MidColor, MaxColor, (((float)CurrentBits / TotalBits) - 0.5f) * 2);
            }

            FrontCanvas.GetComponent<Text>().color = c;
            BackCanvas.GetComponent<Text>().color = c;
        }
    }
}
