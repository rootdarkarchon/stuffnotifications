﻿using Assets.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class SoundEffect
    {
        public AudioClip AudioClip;
        public string Command;
        public long CostCurrency1;
        public long CostCurrency2;
        public string FileName;
        public long GlobalCooldownTime;
        public bool IsOnCooldown;
        public string Message;
        public string Name;
        public long UserCooldownTime;
        public string UserName;
        public float Volume;
        private bool isOnGlobalCooldown;
        private List<TwitchDBUser> userCooldowns;

        public SoundEffect()
        {
            userCooldowns = new List<TwitchDBUser>();
        }

        public bool IsOnGlobalCooldown()
        {
            return isOnGlobalCooldown;
        }

        public bool IsOnUserCooldown(TwitchDBUser user)
        {
            return userCooldowns.Any(entry => entry == user);
        }

        public void LoadSoundClip(Globals globals)
        {
            AudioClip = globals.CreateSoundClipFromFile(Path.Combine(globals.S_SoundEffectsFolder, FileName));
        }

        public async Task SetCooldownForGlobal()
        {
            isOnGlobalCooldown = true;
            await Task.Delay(TimeSpan.FromSeconds(GlobalCooldownTime));
            isOnGlobalCooldown = false;
        }

        public async Task SetCooldownForUser(TwitchDBUser user)
        {
            userCooldowns.Add(user);
            await Task.Delay(TimeSpan.FromSeconds(UserCooldownTime));
            userCooldowns.Remove(user);
        }
    }
}