﻿using Assets.Base;
using Assets.Helpers.AI;
using Assets.Managers;
using System;
using UnityEngine;

namespace Assets.Models
{
    public class TwitchDBUser
    {
        public TwitchDBUser()
        {
        }

        public TwitchDBUser(string username, DatabaseLayer database, TwitchStream activeStream)
        {
            Database = database;
            ActiveStream = activeStream;
            Username = username;
            lastSeenStreamId = Database.GetUserLastSeenStreamId(Username);
            LastLeaveEvent = Database.GetHistoryLastUserEvent(ActiveStream, Username, StreamEvent.LEAVE);
            LastNonLeaveEvent = Database.GetHistoryLastNonLeaveEvent(ActiveStream, Username);
        }

        public void UpdateActiveStream(TwitchStream activeStream)
        {
            ActiveStream = activeStream;
            LastLeaveEvent = Database.GetHistoryLastUserEvent(ActiveStream, Username, StreamEvent.LEAVE);
            LastNonLeaveEvent = Database.GetHistoryLastNonLeaveEvent(ActiveStream, Username);
        }

        public DatabaseLayer Database;

        public TwitchUser TwitchUser;

        public TwitchStream ActiveStream
        {
            get
            {
                return activeStream;
            }
            set
            {
                activeStream = value;
                currency1 = long.MinValue;
                currency2 = long.MinValue;
                sessionCurrency1 = long.MinValue;
                sessionCurrency2 = long.MinValue;
                moderatorLevel = long.MinValue;
                lastSeenStreamId = long.MinValue;
                interest = double.MinValue;
            }
        }

        public string Username;

        [SerializeField]
        private TwitchStream activeStream;

        public PlayerAI PlayerAI;

        private long currency1 = long.MinValue;
        private long currency2 = long.MinValue;
        private long sessionCurrency1 = long.MinValue;
        private long sessionCurrency2 = long.MinValue;
        private long moderatorLevel = long.MinValue;
        private long lastSeenStreamId = long.MinValue;
        private double interest = double.MinValue;

        public override string ToString() => Username;

        public long GetCurrency(CurrencyType currencyType)
        {
            return currencyType == CurrencyType.Currency1 ? Currency1 : Currency2;
        }

        public double Interest
        {
            get
            {
                if (interest == double.MinValue)
                {
                    interest = Database.GetUserInterest(Username);
                }
                return interest;
            }
            set
            {
                interest = value;

                Database.AdjustUserInterest(Username, interest);
            }
        }

        public DateTime? LastLeaveEvent { get; set; }
        public DateTime? LastNonLeaveEvent { get; set; }

        public bool IsOnline
        {
            get
            {
                //DateTime? LastLeaveEvent = Database.GetHistoryLastUserEvent(ActiveStream, Username, StreamEvent.LEAVE);
                //DateTime? lastNonLeaveEvent = Database.GetHistoryLastNonLeaveEvent(ActiveStream, Username);
                if (LastLeaveEvent == null && LastNonLeaveEvent == null)
                {
                    return false;
                }
                else if (LastLeaveEvent == null && LastNonLeaveEvent != null)
                {
                    return true;
                }
                else
                {
                    if (LastLeaveEvent.Value > LastNonLeaveEvent.Value 
                        && (DateTime.Now - LastLeaveEvent.Value).TotalMinutes > 5)
                    {
                        return false;
                    }

                    return true;
                }
            }
        }

        public long Currency1
        {
            get
            {
                if (currency1 == long.MinValue)
                {
                    currency1 = Database.GetUserCurrency(Username, CurrencyType.Currency1);
                }

                return currency1;
            }
            set
            {
                var difference = value - currency1;

                if (value > 0)
                {
                    currency1 = value;
                    SessionCurrency1 += difference;
                }
                else
                {
                    SessionCurrency1 -= currency1;
                    currency1 = 0;
                }

                Database.SetUserCurrency(Username, currency1, CurrencyType.Currency1);
            }
        }

        public long Currency2
        {
            get
            {
                if (currency2 == long.MinValue)
                {
                    currency2 = Database.GetUserCurrency(Username, CurrencyType.Currency2);
                }

                return currency2;
            }
            set
            {
                var difference = value - currency2;

                if (value > 0)
                {
                    currency2 = value;
                    SessionCurrency2 += difference;
                }
                else
                {
                    SessionCurrency2 -= currency2;
                    currency2 = 0;
                }

                Database.SetUserCurrency(Username, currency2, CurrencyType.Currency2);
            }
        }

        public long SessionCurrency1
        {
            get
            {
                if (sessionCurrency1 == long.MinValue)
                {
                    sessionCurrency1 = Database.GetSessionCurrency(Username, CurrencyType.Currency1, ActiveStream.Id);
                }
                return sessionCurrency1;
            }
            set
            {
                sessionCurrency1 = value;
                Database.AdjustSessionCurrency(Username, sessionCurrency1, CurrencyType.Currency1, ActiveStream.Id);
            }
        }

        public long SessionCurrency2
        {
            get
            {
                if (sessionCurrency2 == long.MinValue)
                {
                    sessionCurrency2 = Database.GetSessionCurrency(Username, CurrencyType.Currency2, ActiveStream.Id);
                }
                return sessionCurrency2;
            }
            set
            {
                sessionCurrency2 = value;
                Database.AdjustSessionCurrency(Username, sessionCurrency2, CurrencyType.Currency2, ActiveStream.Id);
            }
        }

        public long ModeratorLevel
        {
            get
            {
                if (moderatorLevel == long.MinValue)
                {
                    moderatorLevel = Database.GetModeratorLevel(Username);
                }
                return moderatorLevel;
            }
            set
            {
                moderatorLevel = value;
                Database.SetUserModeratorLevel(Username, moderatorLevel);
            }
        }

        public long LastSeenStreamId
        {
            get
            {
                if (lastSeenStreamId == long.MinValue)
                {
                    lastSeenStreamId = Database.GetUserLastSeenStreamId(Username);
                }

                return lastSeenStreamId;
            }
            set
            {
                lastSeenStreamId = value;
            }
        }
    }
}