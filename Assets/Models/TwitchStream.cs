﻿using System;

namespace Assets.Models
{
    [Serializable]
    public class TwitchStream
    {
        public long Id;
        public string StreamEndDate;
        public string StreamStartDate;
        public string StreamTitle;

        public TwitchStream()
        {
        }

        public DateTime EndStreamDateTime
        {
            get
            {
                DateTime dateTime;
                DateTime.TryParse(StreamEndDate, out dateTime);
                return dateTime;
            }
        }

        public DateTime StreamStartDateTime
        {
            get
            {
                DateTime dateTime;
                DateTime.TryParse(StreamStartDate, out dateTime);
                return dateTime;
            }
        }

        public TimeSpan Uptime
        {
            get
            {
                return DateTime.Now.Subtract(StreamStartDateTime);
            }
        }
    }
}