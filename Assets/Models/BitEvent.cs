﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Models
{
    [Serializable]
    public class BitEvent
    {
        public TwitchUser User;
        public TwitchLib.Api.V5.Models.Bits.Action Emote;
        public long Amount;
        public long TotalAmount;
        public byte[] Image;

        public int AmountNormalized
        {
            get
            {
                if (Amount < 100)
                    return 1;
                else if (Amount < 1000)
                    return 100;
                else if (Amount < 5000)
                    return 1000;
                else if (Amount < 10000)
                    return 5000;
                else
                    return 10000;
            }
        }

        public string EmoteUrl
        {
            get
            {
                if (Amount < 100)
                    return Emote.Tiers[0].Images.Light.Static.Two;
                else if (Amount < 1000)
                    return Emote.Tiers[1].Images.Light.Static.Two;
                else if (Amount < 5000)
                    return Emote.Tiers[2].Images.Light.Static.Two;
                else if (Amount < 10000)
                    return Emote.Tiers[3].Images.Light.Static.Two;
                else
                    return Emote.Tiers[4].Images.Light.Static.Two;
            }
        }
    }
}
