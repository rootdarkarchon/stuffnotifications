﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using TwitchLib.Api.Core.Interfaces;
using TwitchLib.Api.V5.Models.Subscriptions;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class TwitchUser
    {
        public TwitchUser()
        {
        }

        public TwitchUser(IUser user, DateTime followTime, Subscription sub, bool isFollowing)
        {
            FollowTime = followTime;
            DisplayName = user.DisplayName;
            Logo = user.Logo;
            IsFollowing = isFollowing;

            if (sub != null)
            {
                UpdatedAt = user.UpdatedAt;

                CreatedAt = sub.CreatedAt.ToString(CultureInfo.InvariantCulture);
                SubPlan = sub.SubPlan;
                IsSub = true;
            }
        }

        public TwitchDBUser TwitchDBUser;

        public DateTime FollowTime { get; set; }

        public DateTime UpdatedAt { get; set; }

        public string CreatedAt { get; set; }

        public string SubPlan { get; set; }

        public DateTime CreatedAtDateTime => DateTime.Parse(CreatedAt, CultureInfo.InvariantCulture);

        public GameObject CoinObject { get; set; }

        public string DisplayName { get; set; }

        public bool IsOnline => TwitchDBUser.IsOnline;

        public bool IsFollowing { get; set; } = false;

        public string Logo { get; set; }

        private const int Border = 2;
        private const int TexH = 256;
        private const int TexW = 256;
        public string EmissionTexture => DisplayName + "_e.png";
        public bool IsSub { get; set; } = false;
        public string MainTexture => DisplayName + ".png";

        public string DebugString()
        {
            return DisplayName + " " + ((!IsSub) ? "(Sub: no)" : "(Sub: yes " + SubPlan + " " + CreatedAt + ")") + " " + ((CoinObject == null) ? "(Coin: no)" : "(Coin: yes)");
        }

        public override string ToString()
        {
            return DisplayName + " " + ((!IsSub) ? "(Sub: no)" : "(Sub: yes)");
        }

        public GameObject CloneCoinObject => UnityEngine.Object.Instantiate(CoinObject);

        public byte[] GenerateEmissionTextureForCoin()
        {
            Image output = new Bitmap(TexW, TexH);
            var outputDrawing = System.Drawing.Graphics.FromImage(output);

            outputDrawing.Clear(System.Drawing.Color.Black);

            if (IsSub)
            {
                outputDrawing.FillRectangle(new SolidBrush(System.Drawing.Color.White), 0, 0, TexW, TexH / 2);
                outputDrawing.FillEllipse(new SolidBrush(System.Drawing.Color.Gray),
                    Border,
                    (int)(TexH / 2) + Border,
                    (TexW / 2) - (2 * Border),
                    (TexH / 2) - (2 * Border));
                outputDrawing.FillEllipse(new SolidBrush(System.Drawing.Color.Black),
                Border + 2 * Border,
                (int)(TexH / 2) + Border + 2 * Border,
                (TexW / 2) - (2 * Border) - 4 * Border,
                (TexH / 2) - (2 * Border) - 4 * Border);
                outputDrawing.FillEllipse(new SolidBrush(System.Drawing.Color.Gray),
                    (int)(TexW / 2) + Border,
                    (int)(TexH / 2) + Border,
                    (TexW / 2) - (2 * Border),
                    (TexH / 2) - (2 * Border));
                outputDrawing.FillEllipse(new SolidBrush(System.Drawing.Color.Black),
                    (int)(TexW / 2) + Border + 2 * Border,
                (int)(TexH / 2) + Border + 2 * Border,
                (TexW / 2) - (2 * Border) - 4 * Border,
                (TexH / 2) - (2 * Border) - 4 * Border);
            }

            outputDrawing.Save();

            using (var ms = new MemoryStream())
            {
                output.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }

        public byte[] GenerateUVTextureForCoin(byte[] data)
        {
            Bitmap sourceImage;
            Image output = new Bitmap(TexW, TexH);
            var outputDrawing = System.Drawing.Graphics.FromImage(output);

            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));

            sourceImage = (Bitmap)tc.ConvertFrom(data);

            outputDrawing.Clear(System.Drawing.Color.Transparent);

            if (!IsSub)
            {
                outputDrawing.FillRectangle(new SolidBrush(System.Drawing.Color.DarkGray), 0, 0, TexW, TexH / 2);
            }
            else
            {
                outputDrawing.FillRectangle(new SolidBrush(System.Drawing.Color.FromArgb(203, 243, 0)), 0, 0, TexW, TexH / 2);
            }

            outputDrawing.DrawImage(sourceImage,
                Border,
                (int)(TexH / 2) + Border,
                (TexW / 2) - (2 * Border),
                (TexH / 2) - (2 * Border));
            sourceImage.RotateFlip(RotateFlipType.RotateNoneFlipX);
            outputDrawing.DrawImage(sourceImage,
                (int)(TexW / 2) + Border,
                (int)(TexH / 2) + Border,
                (TexW / 2) - (2 * Border),
                (TexH / 2) - (2 * Border));

            outputDrawing.Save();
            //}

            using (var ms = new MemoryStream())
            {
                output.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }
    }
}