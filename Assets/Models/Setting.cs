﻿using System;
using System.ComponentModel;

namespace Assets.Models
{
    [Serializable]
    public class Setting
    {
        public string RawSettingKey;
        public string RawValue;

        private string settingsKey;
        private string settingsModule;

        public override string ToString()
        {
            return settingsModule + "." + settingsKey;
        }

        public string Key()
        {
            if (settingsKey != null)
            {
                return settingsKey;
            }

            string module = RawSettingKey.Split(new[] { "." }, StringSplitOptions.None)[0];
            settingsKey = RawSettingKey.Replace($"{module}.", "");

            return settingsKey;
        }

        public string Module()
        {
            if (settingsModule != null)
            {
                return settingsModule;
            }

            settingsModule = RawSettingKey.Split(new[] { "." }, StringSplitOptions.None)[0];

            return settingsModule;
        }

        public T Value<T>()
        {
            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

                if (converter != null)
                {
                    return (T)converter.ConvertFromString(RawValue);
                }
                return default;
            }
            catch (NotSupportedException)
            {
                return default;
            }
        }
    }
}