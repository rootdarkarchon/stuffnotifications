﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Models
{
    [Serializable]
    public class WelcomeMessage
    {
        public WelcomeMessage()
        {
        }

        public WelcomeMessage(TwitchUser user, AudioClip clip, bool firstTime)
        {
            User = user;
            AudioClip = clip;
            FirstTime = firstTime;
        }

        public List<string> Questions = new List<string>
    {
        "How are you doing today?",
        "Anything exciting happened lately?",
        "What's up my dude[tte]?",
        "How is it going?",
        "WASSSSSSSSSUUUUUUUUUUPPPPPPPPPPPPPPPPPP",
    };

        public string WelcomeMessageTwitchText
        {
            get
            {
                if (!FirstTime)
                {
                    return $"Welcome back {User.DisplayName}! rootdaToot " + WelcomeBackMessageQuestion;
                }
                else
                {
                    return $"Welcome to the stream, {User.DisplayName}! rootdaToot Glad to have you here! Stay a while and listen rootdaAllEars";
                }
            }
        }

        public string WelcomeMessageGuiText
        {
            get
            {
                if (!FirstTime)
                {
                    return $"Welcome back{Environment.NewLine}{User.DisplayName}!";
                }
                else
                {
                    return $"Welcome,{Environment.NewLine}{User.DisplayName}!";
                }
            }
        }

        public string WelcomeBackMessageQuestion
        {
            get
            {
                return Questions[new System.Random().Next(Questions.Count)];
            }
        }

        public TwitchUser User { get; }
        public AudioClip AudioClip { get; set; }
        public bool FirstTime { get; }
    }
}