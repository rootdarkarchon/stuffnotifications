﻿using System;
using System.Threading.Tasks;

namespace Assets.Models
{
    [Serializable]
    public class Counter
    {
        public string Name;
        public string Command;
        public long CounterNumber;
        public string Message;
        public long GlobalCooldown;
        public string ReplacedMessage => Message.Replace("{counter}", CounterNumber.ToString());

        private bool isOnGlobalCooldown;

        public bool IsOnGlobalCooldown() => isOnGlobalCooldown;

        public async Task SetCooldownForGlobal()
        {
            isOnGlobalCooldown = true;
            await Task.Delay(TimeSpan.FromSeconds(GlobalCooldown));
            isOnGlobalCooldown = false;
        }
    }
}